<?php
namespace Payever\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Payever\CommonBundle\Impl\Serialization\AbstractSerializableObject;
use Payever\CommonBundle\Interfaces\Model\Merchant\MerchantAddressInterface;

/**
 * MerchantAddress
 *
 * @ORM\Table(name="merchant_address")
 * @ORM\Entity(
 *      repositoryClass="Payever\CommonBundle\Impl\Repository\MerchantAddressRepository"
 * )
 */
class MerchantAddress extends AbstractSerializableObject implements MerchantAddressInterface
{
    /**
     * @return string
     */
    public static function getClass()
    {
        return get_class();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Foreign key to MerchantDetails
     *
     * @var MerchantDetails
     *
     * @ORM\ManyToOne(targetEntity="MerchantDetails", inversedBy="addresses", cascade="all")
     * @ORM\JoinColumn(name="merchant_details_id", referencedColumnName="id", onDelete="cascade")
     */
    private $merchantDetails;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * True if this is the primary address
     *
     * @var boolean
     *
     * @ORM\Column(name="is_primary", type="boolean", nullable=false)
     */
    private $isPrimary = false;

    /**
     * "Anrede"
     *
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title = "";

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=false)
     */
    private $firstName = "";

    /**
     * @var string
     *
     * @ORM\Column(name="middle_name", type="string", length=255, nullable=false)
     */
    private $middleName = "";

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=false)
     */
    private $lastName = "";

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=false)
     */
    private $country = "";

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    private $city = "";

    /**
     * @var string
     *
     * @ORM\Column(name="zip_code", type="string", length=32, nullable=false)
     */
    private $zipCode = "";

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=false)
     */
    private $street = "";

    /**
     * "Hausnummer"
     *
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=32, nullable=false)
     */
    private $number = "";

    /**
     * @var string
     *
     * @ORM\Column(name="email_address", type="string", length=255, nullable=false)
     */
    private $emailAddress = "";

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=64, nullable=false)
     */
    private $phoneNumber = "";

    /**
     * @var string
     *
     * @ORM\Column(name="mobile_phone_number", type="string", length=64, nullable=false)
     */
    private $mobilePhoneNumber = "";

    /**
     * @var string
     *
     * @ORM\Column(name="fax_number", type="string", length=64, nullable=false)
     */
    private $faxNumber = "";

    /**
     * C'tor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('NOW');
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return MerchantDetails
     */
    public function getMerchantDetails()
    {
        return $this->merchantDetails;
    }

    /**
     * @param MerchantDetails $merchantDetails
     */
    public function setMerchantDetails(MerchantDetails $merchantDetails)
    {
        $this->merchantDetails = $merchantDetails;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return boolean
     */
    public function getIsPrimary()
    {
        return $this->isPrimary;
    }

    /**
     * @param boolean $isPrimary
     */
    public function setIsPrimary($isPrimary)
    {
        $this->isPrimary = $isPrimary ? true : false;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * @param string $emailAddress
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getMobilePhoneNumber()
    {
        return $this->mobilePhoneNumber;
    }

    /**
     * @param string $mobilePhoneNumber
     */
    public function setMobilePhoneNumber($mobilePhoneNumber)
    {
        $this->mobilePhoneNumber = $mobilePhoneNumber;
    }

    /**
     * @return string
     */
    public function getFaxNumber()
    {
        return $this->faxNumber;
    }

    /**
     * @param string $faxNumber
     */
    public function setFaxNumber($faxNumber)
    {
        $this->faxNumber = $faxNumber;
    }
}
