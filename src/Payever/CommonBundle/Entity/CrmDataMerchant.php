<?php
namespace Payever\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Payever\CommonBundle\Impl\Serialization\AbstractSerializableObject;
use Payever\CommonBundle\Interfaces\Model\Crm\CrmDataMerchantInterface;

/**
 * CrmDataMerchant holds administrative data about the merchant
 *
 * @ORM\Table(name="crm_data_merchant")
 * @ORM\Entity(
 *      repositoryClass="Payever\CommonBundle\Impl\Repository\CrmDataMerchantRepository"
 * )
 */
class CrmDataMerchant extends AbstractSerializableObject implements CrmDataMerchantInterface
{
    /** low status, when the merchant was never ever approved */
    const STATUS_LOW        = 0;
    /** medium status, when the merchant was approved but changed some data */
    const STATUS_MEDIUM     = 0.5;
    /** highest status, when merchant data is fully approved */
    const STATUS_HIGH       = 1;

    /**
     * @return string
     */
    public static function getClass()
    {
        return get_class();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var MerchantDetails
     *
     * @ORM\OneToOne(targetEntity="MerchantDetails", inversedBy="crmData", cascade="all")
     * @ORM\JoinColumn(name="merchant_details_id", referencedColumnName="id", onDelete="cascade", nullable=false)
     */
    private $merchantDetails;

    /**
     * @var int
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * The value defines the merchants trustability.
     *
     * @var float
     *
     * @ORM\Column(name="trustability", type="decimal", precision=2, scale=2, nullable=false)
     */
    private $trustability = self::STATUS_MEDIUM;

    /**
     * Has the merchants post ident been validated and approved.
     *
     * @var boolean
     *
     * @ORM\Column(name="post_ident_approved", type="boolean", nullable=false)
     */
    private $postIdentApproved = false;

    /**
     * Has the trade register excerpt been validated and approved.
     *
     * @var boolean
     *
     * @ORM\Column(name="trade_register_excerpt_approved", type="boolean", nullable=false)
     */
    private $tradeRegisterExcerptApproved = false;

    /**
     * Has the merchants post ident been validated and approved.
     *
     * @var boolean
     *
     * @ORM\Column(name="button_use_approved", type="boolean", nullable=false)
     */
    private $buttonUseApproved = false;

    /**
     * Is the merchant able to provide 12 months installments payment
     *
     * @var boolean
     *
     * @ORM\Column(name="provides_12_months_installments", type="boolean", nullable=false)
     */
    private $provides12MonthsInstallments = false;

    /**
     * C'tor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('NOW');
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return MerchantDetails
     */
    public function getMerchantDetails()
    {
        return $this->merchantDetails;
    }

    /**
     * @param MerchantDetails $merchantDetails
     */
    public function setMerchantDetails(MerchantDetails $merchantDetails)
    {
        $this->merchantDetails = $merchantDetails;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return float
     */
    public function getTrustability()
    {
        return $this->trustability;
    }

    /**
     * @param float $trustability
     */
    public function setTrustability($trustability)
    {
        $this->trustability = $trustability;
    }

    /**
     * @return boolean
     */
    public function getPostIdentApproved()
    {
        return $this->postIdentApproved;
    }

    /**
     * @param boolean $postIdentApproved
     */
    public function setPostIdentApproved($postIdentApproved)
    {
        $this->postIdentApproved = $postIdentApproved;
    }

    /**
     * @return boolean
     */
    public function getTradeRegisterExcerptApproved()
    {
        return $this->tradeRegisterExcerptApproved;
    }

    /**
     * @param boolean $tradeRegisterExcerptApproved
     */
    public function setTradeRegisterExcerptApproved($tradeRegisterExcerptApproved)
    {
        $this->tradeRegisterExcerptApproved = $tradeRegisterExcerptApproved;
    }

    /**
     * @return boolean
     */
    public function getButtonUseApproved()
    {
        return $this->buttonUseApproved;
    }

    /**
     * @param boolean $buttonUseApproved
     */
    public function setButtonUseApproved($buttonUseApproved)
    {
        $this->buttonUseApproved = $buttonUseApproved;
    }

    /**
     * @return boolean
     */
    public function getProvides12MonthsInstallments()
    {
        return $this->provides12MonthsInstallments;
    }

    /**
     * @param boolean $provides12MonthsInstallments
     */
    public function setProvides12MonthsInstallments($provides12MonthsInstallments)
    {
        $this->provides12MonthsInstallments = $provides12MonthsInstallments;
    }
}
