<?php
namespace Payever\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Payever\CommonBundle\Impl\Serialization\AbstractSerializableObject;
use Payever\CommonBundle\Interfaces\Model\Customer\CustomerBankAccountInterface;

/**
 * CustomerBankAccount
 *
 * @ORM\Table(name="customer_bank_account")
 * @ORM\Entity(
 *      repositoryClass="Payever\CommonBundle\Impl\Repository\CustomerBankAccountRepository"
 * )
 */
class CustomerBankAccount extends AbstractSerializableObject implements CustomerBankAccountInterface
{
    /**
     * @return string
     */
    public static function getClass()
    {
        return get_class();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Foreign key to CustomerDetails
     *
     * @var CustomerDetails
     *
     * @ORM\ManyToOne(targetEntity="CustomerDetails", inversedBy="bankAccounts", cascade="all")
     * @ORM\JoinColumn(name="customer_details_id", referencedColumnName="id", onDelete="cascade")
     */
    private $customerDetails;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_primary", type="boolean", nullable=false)
     */
    private $isPrimary = false;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_name", type="string", length=255, nullable=false)
     */
    private $bankName = "";

    /**
     * @var string
     *
     * @ORM\Column(name="bank_code", type="string", length=64, nullable=false)
     */
    private $bankCode = "";

    /**
     * @var string
     *
     * @ORM\Column(name="account_number", type="string", length=64, nullable=false)
     */
    private $accountNumber = "";

    /**
     * C'tor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('NOW');
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return CustomerDetails
     */
    public function getCustomerDetails()
    {
        return $this->customerDetails;
    }

    /**
     * @param CustomerDetails $customerDetails
     */
    public function setCustomerDetails(CustomerDetails $customerDetails)
    {
        $this->customerDetails = $customerDetails;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return boolean
     */
    public function getIsPrimary()
    {
        return $this->isPrimary;
    }

    /**
     * @param boolean $isPrimary
     */
    public function setIsPrimary($isPrimary)
    {
        $this->isPrimary = $isPrimary ? true : false;
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param string $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * @return string
     */
    public function getBankCode()
    {
        return $this->bankCode;
    }

    /**
     * @param string $bankCode
     */
    public function setBankCode($bankCode)
    {
        $this->bankCode = $bankCode;
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @param string $accountNumber
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;
    }
}
