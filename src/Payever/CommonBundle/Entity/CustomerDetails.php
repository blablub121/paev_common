<?php
namespace Payever\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Payever\CommonBundle\Impl\Serialization\AbstractSerializableObject;
use Payever\CommonBundle\Interfaces\Model\User\UserAccountInterface;
use Payever\CommonBundle\Interfaces\Model\Customer\CustomerBankAccountInterface;
use Payever\CommonBundle\Interfaces\Model\Customer\CustomerDetailsInterface;

/**
 * CustomerDetails
 *
 * @ORM\Table(
 *      name="customer_details",
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="uniq_idx_customer_user_account_id", columns={"user_account_id"})
 *      }
 * )
 * @ORM\Entity(
 *      repositoryClass="Payever\CommonBundle\Impl\Repository\CustomerDetailsRepository"
 * )
 */
class CustomerDetails extends AbstractSerializableObject implements CustomerDetailsInterface
{
    /**
     * @return string
     */
    public static function getClass()
    {
        return get_class();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var UserAccountInterface
     *
     * @ORM\OneToOne(targetEntity="UserAccount", cascade="all")
     * @ORM\JoinColumn(name="user_account_id", referencedColumnName="id")
     */
    private $userAccount;

    /**
     * @var Payment\BasePayment[]
     *
     * @ORM\OneToMany(targetEntity="\Payever\CommonBundle\Entity\Payment\BasePayment", mappedBy="customerDetails", cascade="all")
     */
    private $payments;

    /**
     * @var CustomerBankAccount[]
     *
     * @ORM\OneToMany(targetEntity="CustomerBankAccount", mappedBy="customerDetails", cascade="all")
     */
    private $bankAccounts;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt = null;

    /**
     * @var string
     *
     * @ORM\Column(name="salutation", type="string", length=255, nullable=false)
     */
    private $salutation = "";

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=false)
     */
    private $firstName = "";

    /**
     * @var string
     *
     * @ORM\Column(name="middle_name", type="string", length=255, nullable=false)
     */
    private $middleName = "";

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=false)
     */
    private $lastName = "";

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     */
    private $birthday = null;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=false)
     */
    private $country = "";

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    private $city = "";

    /**
     * @var string
     *
     * @ORM\Column(name="zip_code", type="string", length=255, nullable=false)
     */
    private $zipCode = "";

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=false)
     */
    private $street = "";

    /**
     * @var string
     *
     * @ORM\Column(name="street_number", type="string", length=255, nullable=false)
     */
    private $streetNumber = "";

    /**
     * "wohnhaft seit"
     *
     * @var \DateTime
     *
     * @ORM\Column(name="resident_since", type="datetime", nullable=true)
     */
    private $residentSince = null;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=255, nullable=false)
     */
    private $phoneNumber = "";

    /**
     * @var string
     *
     * @ORM\Column(name="mobile_phone_number", type="string", length=255, nullable=false)
     */
    private $mobilePhoneNumber = "";

    /**
     * @var string
     *
     * @ORM\Column(name="birth_place", type="string", length=255, nullable=false)
     */
    private $birthPlace = "";

    /**
     * @var string
     *
     * @ORM\Column(name="nationality", type="string", length=255, nullable=false)
     */
    private $nationality = "";

    /**
     * @var int
     *
     * @ORM\Column(name="marital_status", type="integer", nullable=false)
     */
    private $maritalStatus = self::MARITAL_STATUS_SINGLE;

    /**
     * @var int
     *
     * @ORM\Column(name="num_children", type="integer", nullable=false)
     */
    private $numChildren = 0;

    /**
     * "Berufsgruppe"
     *
     * @var string
     *
     * @ORM\Column(name="occupation_group", type="string", length=255, nullable=false)
     */
    private $occupationGroup = "";

    /**
     * "Branche"
     *
     * @var string
     *
     * @ORM\Column(name="industry", type="string", length=255, nullable=false)
     */
    private $industry = "";

    /**
     * "Ausgeübter Beruf"
     *
     * @var string
     *
     * @ORM\Column(name="profession", type="string", length=255, nullable=false)
     */
    private $profession = "";

    /**
     * "Arbeitgeber"
     *
     * @var string
     *
     * @ORM\Column(name="employer", type="string", length=255, nullable=false)
     */
    private $employer = "";

    /**
     * "Arbeitgeber Telefon"
     *
     * @var string
     *
     * @ORM\Column(name="employer_phone_number", type="string", length=255, nullable=false)
     */
    private $employerPhoneNumber = "";

    /**
     * Land des "Arbeitgeber"
     *
     * @var string
     *
     * @ORM\Column(name="employer_country", type="string", length=255, nullable=false)
     */
    private $employerCountry = "";

    /**
     * Stadt des "Arbeitgeber"
     *
     * @var string
     *
     * @ORM\Column(name="employer_city", type="string", length=255, nullable=false)
     */
    private $employerCity = "";

    /**
     * PLZ des "Arbeitgeber"
     *
     * @var string
     *
     * @ORM\Column(name="employer_zip_code", type="string", length=255, nullable=false)
     */
    private $employerZipCode = "";

    /**
     * "angestellt seit"
     *
     * @var \DateTime
     *
     * @ORM\Column(name="employed_since", type="datetime", nullable=true)
     */
    private $employedSince = null;

    /**
     * "befristet angestellt"
     *
     * @var bool
     *
     * @ORM\Column(name="employed_temporary", type="boolean", nullable=false)
     */
    private $employedTemporary = false;

    /**
     * "Monatliches Nettoeinkommen"
     *
     * @var float
     *
     * @ORM\Column(name="monthly_income_net", type="decimal", nullable=false)
     */
    private $monthlyIncomeNet = 0;

    /**
     * "Monatliche Nettorente"
     *
     * @var float
     *
     * @ORM\Column(name="monthly_pension_net", type="decimal", nullable=false)
     */
    private $monthlyPensionNet = 0;

    /**
     * "Monatliche Ausgaben Miete"
     *
     * @var float
     *
     * @ORM\Column(name="monthly_expenses_rent", type="decimal", nullable=false)
     */
    private $monthlyExpensesRent = 0;

    /**
     * "Monatliche Ausgaben Auto"
     *
     * @var float
     *
     * @ORM\Column(name="monthly_expenses_car", type="decimal", nullable=false)
     */
    private $monthlyExpensesCar = 0;

    /**
     * "Monatliche sonstige Ausgaben"
     *
     * @var float
     *
     * @ORM\Column(name="monthly_expenses_misc", type="decimal", nullable=false)
     */
    private $monthlyExpensesMisc = 0;

    /**
     * C'tor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('NOW');
        $this->bankAccounts = new ArrayCollection();
        $this->payments = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return UserAccountInterface
     */
    public function getUserAccount()
    {
        return $this->userAccount;
    }

    /**
     * @param UserAccountInterface $userAccount
     */
    public function setUserAccount(UserAccountInterface $userAccount)
    {
        $this->userAccount = $userAccount;
    }

    /**
     * @return CustomerBankAccount[]
     */
    public function getBankAccounts()
    {
        return $this->bankAccounts;
    }

    /**
     * @return CustomerBankAccountInterface
     */
    public function getPrimaryBankAccount()
    {
        // if there is no address at all we create one
        if (count($this->bankAccounts) == 0) {
            $newPrimary = new CustomerBankAccount();
            $newPrimary->setIsPrimary(true);
            $this->bankAccounts[] = $newPrimary;
        }
        // search an address that is set to primary
        foreach ($this->bankAccounts as $account) {
            if ($account->getIsPrimary()) {
                return $account;
            }
        }
        // we have not found a primary address so we take the first one as primary
        $this->bankAccounts[0]->setIsPrimary(true);
        return $this->bankAccounts[0];
    }

    /**
     * @param CustomerBankAccountInterface $bankAccount
     */
    public function setPrimaryBankAccount(CustomerBankAccountInterface $bankAccount)
    {
        // if there is no address at all we create one
        if (count($this->bankAccounts) == 0) {
            $newPrimary = new CustomerBankAccount();
            $newPrimary->setIsPrimary(true);
            $this->bankAccounts[] = $newPrimary;
        }
        foreach ($this->bankAccounts as $k => $v) {
            if ($v->getIsPrimary()) {
                $this->bankAccounts[$k] = $bankAccount;
                return;
            }
        }
        // we have not found a primary address so we take the first one
        $this->bankAccounts[0] = $bankAccount;
    }

    /**
     * @param CustomerBankAccountInterface $bankAccount
     */
    public function addBankAccount(CustomerBankAccountInterface $bankAccount)
    {
        $this->bankAccounts->add($bankAccount);
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * @param string $salutation
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param string $middleName
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param string $zipCode
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getStreetNumber()
    {
        return $this->streetNumber;
    }

    /**
     * @param string $streetNumber
     */
    public function setStreetNumber($streetNumber)
    {
        $this->streetNumber = $streetNumber;
    }

    /**
     * @return \DateTime
     */
    public function getResidentSince()
    {
        return $this->residentSince;
    }

    /**
     * @param \DateTime $residentSince
     */
    public function setResidentSince($residentSince)
    {
        $this->residentSince = $residentSince;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getMobilePhoneNumber()
    {
        return $this->mobilePhoneNumber;
    }

    /**
     * @param string $mobilePhoneNumber
     */
    public function setMobilePhoneNumber($mobilePhoneNumber)
    {
        $this->mobilePhoneNumber = $mobilePhoneNumber;
    }

    /**
     * @return string
     */
    public function getBirthPlace()
    {
        return $this->birthPlace;
    }

    /**
     * @param string $birthPlace
     */
    public function setBirthPlace($birthPlace)
    {
        $this->birthPlace = $birthPlace;
    }

    /**
     * @return string
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param string $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

    /**
     * @return int
     */
    public function getMaritalStatus()
    {
        return $this->maritalStatus;
    }

    /**
     * @param int $maritalStatus
     */
    public function setMaritalStatus($maritalStatus)
    {
        $this->maritalStatus = $maritalStatus;
    }

    /**
     * @return int
     */
    public function getNumChildren()
    {
        return $this->numChildren;
    }

    /**
     * @param int $numChildren
     */
    public function setNumChildren($numChildren)
    {
        $this->numChildren = $numChildren;
    }

    /**
     * @return string
     */
    public function getOccupationGroup()
    {
        return $this->occupationGroup;
    }

    /**
     * @param string $occupationGroup
     */
    public function setOccupationGroup($occupationGroup)
    {
        $this->occupationGroup = $occupationGroup;
    }

    /**
     * @return string
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * @param string $industry
     */
    public function setIndustry($industry)
    {
        $this->industry = $industry;
    }

    /**
     * @return string
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * @param string $profession
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;
    }

    /**
     * @return string
     */
    public function getEmployer()
    {
        return $this->employer;
    }

    /**
     * @param string $employer
     */
    public function setEmployer($employer)
    {
        $this->employer = $employer;
    }

    /**
     * @return string
     */
    public function getEmployerPhoneNumber()
    {
        return $this->employerPhoneNumber;
    }

    /**
     * @param string $employerPhoneNumber
     */
    public function setEmployerPhoneNumber($employerPhoneNumber)
    {
        $this->employerPhoneNumber = $employerPhoneNumber;
    }

    /**
     * @return string
     */
    public function getEmployerCountry()
    {
        return $this->employerCountry;
    }

    /**
     * @param string $employerCountry
     */
    public function setEmployerCountry($employerCountry)
    {
        $this->employerCountry = $employerCountry;
    }

    /**
     * @return string
     */
    public function getEmployerCity()
    {
        return $this->employerCity;
    }

    /**
     * @param string $employerCity
     */
    public function setEmployerCity($employerCity)
    {
        $this->employerCity = $employerCity;
    }

    /**
     * @return string
     */
    public function getEmployerZipCode()
    {
        return $this->employerZipCode;
    }

    /**
     * @param string $employerZipCode
     */
    public function setEmployerZipCode($employerZipCode)
    {
        $this->employerZipCode = $employerZipCode;
    }

    /**
     * @return \DateTime
     */
    public function getEmployedSince()
    {
        return $this->employedSince;
    }

    /**
     * @param \DateTime $employedSince
     */
    public function setEmployedSince($employedSince)
    {
        $this->employedSince = $employedSince;
    }

    /**
     * @return boolean
     */
    public function getEmployedTemporary()
    {
        return $this->employedTemporary;
    }

    /**
     * @param boolean $employedTemporary
     */
    public function setEmployedTemporary($employedTemporary)
    {
        $this->employedTemporary = $employedTemporary;
    }

    /**
     * @return float
     */
    public function getMonthlyIncomeNet()
    {
        return $this->monthlyIncomeNet;
    }

    /**
     * @param float $monthlyIncomeNet
     */
    public function setMonthlyIncomeNet($monthlyIncomeNet)
    {
        $this->monthlyIncomeNet = $monthlyIncomeNet;
    }

    /**
     * @return float
     */
    public function getMonthlyPensionNet()
    {
        return $this->monthlyPensionNet;
    }

    /**
     * @param float $monthlyPensionNet
     */
    public function setMonthlyPensionNet($monthlyPensionNet)
    {
        $this->monthlyPensionNet = $monthlyPensionNet;
    }

    /**
     * @return float
     */
    public function getMonthlyExpensesRent()
    {
        return $this->monthlyExpensesRent;
    }

    /**
     * @param float $monthlyExpensesRent
     */
    public function setMonthlyExpensesRent($monthlyExpensesRent)
    {
        $this->monthlyExpensesRent = $monthlyExpensesRent;
    }

    /**
     * @return float
     */
    public function getMonthlyExpensesCar()
    {
        return $this->monthlyExpensesCar;
    }

    /**
     * @param float $monthlyExpensesCar
     */
    public function setMonthlyExpensesCar($monthlyExpensesCar)
    {
        $this->monthlyExpensesCar = $monthlyExpensesCar;
    }

    /**
     * @return float
     */
    public function getMonthlyExpensesMisc()
    {
        return $this->monthlyExpensesMisc;
    }

    /**
     * @param float $monthlyExpensesMisc
     */
    public function setMonthlyExpensesMisc($monthlyExpensesMisc)
    {
        $this->monthlyExpensesMisc = $monthlyExpensesMisc;
    }

    /**
     * @return \Payever\CommonBundle\Entity\Payment\BasePayment[]
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * @param \Payever\CommonBundle\Entity\Payment\BasePayment[] $payments
     */
    public function setPayments($payments)
    {
        $this->payments = $payments;
    }
}
