<?php
namespace Payever\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Payever\CommonBundle\Impl\Serialization\AbstractSerializableObject;

/**
 * CrmDataMerchant holds administrative data about the merchant
 *
 * @ORM\Table(
 *      name="unique_identifier",
 *      uniqueConstraints = {
 *          @ORM\UniqueConstraint(name="idx_unique_identifier_hash", columns={"hash"})
 *      }
 * )
 * @ORM\Entity(
 *      repositoryClass="Payever\CommonBundle\Impl\Repository\UniqueIdentifierRepository"
 * )
 */
class UniqueIdentifier extends AbstractSerializableObject
{

    /**
     * @return string
     */
    public static function getClass()
    {
        return get_class();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=32, nullable=false)
     */
    private $hash;

    /**
     * @var int
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * C'tor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('NOW');
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
