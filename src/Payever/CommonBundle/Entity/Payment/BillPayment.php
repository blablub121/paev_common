<?php
namespace Payever\CommonBundle\Entity\Payment;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class BillPayment
 *
 * @ORM\Entity
 *
 * @package Payever\CommonBundle\Entity\Payment
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class BillPayment extends BasePayment
{
    /**
     * @return string
     */
    public function getType()
    {
        return 'billPayment';
    }
}
