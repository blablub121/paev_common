<?php
namespace Payever\CommonBundle\Entity\Payment;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class InstallmentsPayment
 *
 * @ORM\Entity
 *
 * @package Payever\CommonBundle\Entity\Payment
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class InstallmentsPayment extends BasePayment
{
    /**
     * @return string
     */
    public static function getClass()
    {
        return get_class();
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'installmentsPayment';
    }

    /**
     * @var string
     *
     * @ORM\Column(name="credit_duration_in_months", type="string", length=64, nullable=true)
     */
    private $creditDurationInMonths;

    /**
     * @var string
     *
     * @ORM\Column(name="credit_due_date", type="string", length=64, nullable=true)
     */
    private $creditDueDate;


    /**
     * @var string
     *
     * @ORM\Column(name="credit_reason", type="string", length=255, nullable=true)
     */
    private $creditReason;

    /**
     * If the selection of credit reason is "misc" the user needs to enter a value here.
     *
     * @var string
     *
     * @ORM\Column(name="credit_reason_details", type="string", length=255, nullable=true)
     */
    private $creditReasonDetails;

    /**
     * @var bool
     *
     * @ORM\Column(name="credit_accepts_req_tca", type="boolean", nullable=true)
     */
    private $creditAcceptsRequestsToCreditAgencies;

    /**
     * @var bool
     *
     * @ORM\Column(name="credit_conf_self_initiative", type="boolean", nullable=true)
     */
    private $creditConfirmsSelfInitiative;

    /**
     * @var string
     *
     * @ORM\Column(name="personal_salutation", type="string", length=255, nullable=true)
     */
    private $personalSalutation;

    /**
     * @var string
     *
     * @ORM\Column(name="personal_first_name", type="string", length=255, nullable=true)
     */
    private $personalFirstName;

    /**
     * @var string
     *
     * @ORM\Column(name="personal_last_name", type="string", length=255, nullable=true)
     */
    private $personalLastName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="personal_birthday", type="datetime", nullable=true)
     */
    private $personalBirthday;

    /**
     * @var string
     *
     * @ORM\Column(name="personal_marital_status", type="string", length=64, nullable=true)
     */
    private $personalMaritalStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="personal_nationality", type="string", length=255, nullable=true)
     */
    private $personalNationality;

    /**
     * @var string
     *
     * @ORM\Column(name="personal_work_permit", type="string", length=64, nullable=true)
     */
    private $personalWorkPermit;

    /**
     * @var string
     *
     * @ORM\Column(name="personal_residence_permit", type="string", length=64, nullable=true)
     */
    private $personalResidencePermit;

    /**
     * @var string
     *
     * @ORM\Column(name="address_city", type="string", length=255, nullable=true)
     */
    private $addressCity;

    /**
     * @var string
     *
     * @ORM\Column(name="address_zip_code", type="string", length=255, nullable=true)
     */
    private $addressZipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="personal_street", type="string", length=255, nullable=true)
     */
    private $addressStreet;

    /**
     * @var string
     *
     * @ORM\Column(name="address_street_number", type="string", length=255, nullable=true)
     */
    private $addressStreetNumber;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="address_since", type="datetime", nullable=true)
     */
    private $addressSince;

    /**
     * @var string
     *
     * @ORM\Column(name="address_residence_terms", type="string", length=64, nullable=true)
     */
    private $addressResidenceTerms;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_phone_private", type="string", length=64, nullable=true)
     */
    private $contactPhonePrivate;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_phone_mobile", type="string", length=64, nullable=true)
     */
    private $contactPhoneMobile;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_phone_business", type="string", length=64, nullable=true)
     */
    private $contactPhoneBusiness;

    /**
     * @var string
     *
     * @ORM\Column(name="employment_type", type="string", length=64, nullable=true)
     */
    private $employmentType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="employment_since", type="datetime", nullable=true)
     */
    private $employmentSince;

    /**
     * @var string
     *
     * @ORM\Column(name="employment_contract_type", type="string", length=64, nullable=true)
     */
    private $employmentContractType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="employment_until", type="datetime", nullable=true)
     */
    private $employmentUntil;

    /**
     * @var string
     *
     * @ORM\Column(name="employment_branch", type="string", length=64, nullable=true)
     */
    private $employmentBranch;

    /**
     * @var string
     *
     * @ORM\Column(name="employer_company", type="string", length=255, nullable=true)
     */
    private $employerCompany;

    /**
     * @var string
     *
     * @ORM\Column(name="employer_city", type="string", length=255, nullable=true)
     */
    private $employerCity;

    /**
     * @var string
     *
     * @ORM\Column(name="employer_zip_code", type="string", length=255, nullable=true)
     */
    private $employerZipCode;

    /**
     * @var string
     *
     * @ORM\Column(name="employer_street", type="string", length=255, nullable=true)
     */
    private $employerStreet;

    /**
     * @var string
     *
     * @ORM\Column(name="employer_street_number", type="string", length=255, nullable=true)
     */
    private $employerStreetNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="income_monthly", type="string", length=64, nullable=true)
     */
    private $incomeMonthly;

    /**
     * @var string
     *
     * @ORM\Column(name="expenses_monthly_rent", type="string", length=64, nullable=true)
     */
    private $expensesMonthlyRent;

    /**
     * @var string
     *
     * @ORM\Column(name="expenses_monthly_mortgages", type="string", length=64, nullable=true)
     */
    private $expensesMonthlyMortgages;

    /**
     * @var string
     *
     * @ORM\Column(name="expenses_monthly_aliment", type="string", length=64, nullable=true)
     */
    private $expensesMonthlyAliment;

    /**
     * @var string
     *
     * @ORM\Column(name="expenses_monthly_misc", type="string", length=64, nullable=true)
     */
    private $expensesMonthlyMisc;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_account_owner", type="string", length=255, nullable=true)
     */
    private $bankAccountOwner;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_account_number", type="string", length=255, nullable=true)
     */
    private $bankAccountNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_account_bank_code", type="string", length=255, nullable=true)
     */
    private $bankAccountBankCode;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_account_bank_name", type="string", length=255, nullable=true)
     */
    private $bankAccountBankName;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_account_bank_city", type="string", length=255, nullable=true)
     */
    private $bankAccountBankCity;

    /**
     * @return string
     */
    public function getCreditDurationInMonths()
    {
        return $this->creditDurationInMonths;
    }

    /**
     * @param string $creditDurationInMonths
     */
    public function setCreditDurationInMonths($creditDurationInMonths)
    {
        $this->creditDurationInMonths = $creditDurationInMonths;
    }

    /**
     * @return string
     */
    public function getCreditDueDate()
    {
        return $this->creditDueDate;
    }

    /**
     * @param string $creditDueDate
     */
    public function setCreditDueDate($creditDueDate)
    {
        $this->creditDueDate = $creditDueDate;
    }

    /**
     * @return string
     */
    public function getCreditReason()
    {
        return $this->creditReason;
    }

    /**
     * @param string $creditReason
     */
    public function setCreditReason($creditReason)
    {
        $this->creditReason = $creditReason;
    }

    /**
     * @return string
     */
    public function getCreditReasonDetails()
    {
        return $this->creditReasonDetails;
    }

    /**
     * @param string $creditReasonDetails
     */
    public function setCreditReasonDetails($creditReasonDetails)
    {
        $this->creditReasonDetails = $creditReasonDetails;
    }

    /**
     * @return boolean
     */
    public function getCreditAcceptsRequestsToCreditAgencies()
    {
        return $this->creditAcceptsRequestsToCreditAgencies;
    }

    /**
     * @param boolean $creditAcceptsRequestsToCreditAgencies
     */
    public function setCreditAcceptsRequestsToCreditAgencies($creditAcceptsRequestsToCreditAgencies)
    {
        $this->creditAcceptsRequestsToCreditAgencies = $creditAcceptsRequestsToCreditAgencies;
    }

    /**
     * @return boolean
     */
    public function getCreditConfirmsSelfInitiative()
    {
        return $this->creditConfirmsSelfInitiative;
    }

    /**
     * @param boolean $creditConfirmsSelfInitiative
     */
    public function setCreditConfirmsSelfInitiative($creditConfirmsSelfInitiative)
    {
        $this->creditConfirmsSelfInitiative = $creditConfirmsSelfInitiative;
    }

    /**
     * @return string
     */
    public function getPersonalSalutation()
    {
        return $this->personalSalutation;
    }

    /**
     * @param string $personalSalutation
     */
    public function setPersonalSalutation($personalSalutation)
    {
        $this->personalSalutation = $personalSalutation;
    }

    /**
     * @return string
     */
    public function getPersonalFirstName()
    {
        return $this->personalFirstName;
    }

    /**
     * @param string $personalFirstName
     */
    public function setPersonalFirstName($personalFirstName)
    {
        $this->personalFirstName = $personalFirstName;
    }

    /**
     * @return string
     */
    public function getPersonalLastName()
    {
        return $this->personalLastName;
    }

    /**
     * @param string $personalLastName
     */
    public function setPersonalLastName($personalLastName)
    {
        $this->personalLastName = $personalLastName;
    }

    /**
     * @return \DateTime
     */
    public function getPersonalBirthday()
    {
        return $this->personalBirthday;
    }

    /**
     * @param \DateTime $personalBirthday
     */
    public function setPersonalBirthday($personalBirthday)
    {
        $this->personalBirthday = $personalBirthday;
    }

    /**
     * @return string
     */
    public function getPersonalMaritalStatus()
    {
        return $this->personalMaritalStatus;
    }

    /**
     * @param string $personalMaritalStatus
     */
    public function setPersonalMaritalStatus($personalMaritalStatus)
    {
        $this->personalMaritalStatus = $personalMaritalStatus;
    }

    /**
     * @return string
     */
    public function getPersonalNationality()
    {
        return $this->personalNationality;
    }

    /**
     * @param string $personalNationality
     */
    public function setPersonalNationality($personalNationality)
    {
        $this->personalNationality = $personalNationality;
    }

    /**
     * @return string
     */
    public function getPersonalWorkPermit()
    {
        return $this->personalWorkPermit;
    }

    /**
     * @param string $personalWorkPermit
     */
    public function setPersonalWorkPermit($personalWorkPermit)
    {
        $this->personalWorkPermit = $personalWorkPermit;
    }

    /**
     * @return string
     */
    public function getPersonalResidencePermit()
    {
        return $this->personalResidencePermit;
    }

    /**
     * @param string $personalResidencePermit
     */
    public function setPersonalResidencePermit($personalResidencePermit)
    {
        $this->personalResidencePermit = $personalResidencePermit;
    }

    /**
     * @return string
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }

    /**
     * @param string $addressCity
     */
    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;
    }

    /**
     * @return string
     */
    public function getAddressZipCode()
    {
        return $this->addressZipCode;
    }

    /**
     * @param string $addressZipCode
     */
    public function setAddressZipCode($addressZipCode)
    {
        $this->addressZipCode = $addressZipCode;
    }

    /**
     * @return string
     */
    public function getAddressStreet()
    {
        return $this->addressStreet;
    }

    /**
     * @param string $addressStreet
     */
    public function setAddressStreet($addressStreet)
    {
        $this->addressStreet = $addressStreet;
    }

    /**
     * @return string
     */
    public function getAddressStreetNumber()
    {
        return $this->addressStreetNumber;
    }

    /**
     * @param string $addressStreetNumber
     */
    public function setAddressStreetNumber($addressStreetNumber)
    {
        $this->addressStreetNumber = $addressStreetNumber;
    }

    /**
     * @return \DateTime
     */
    public function getAddressSince()
    {
        return $this->addressSince;
    }

    /**
     * @param \DateTime $addressSince
     */
    public function setAddressSince($addressSince)
    {
        $this->addressSince = $addressSince;
    }

    /**
     * @return string
     */
    public function getAddressResidenceTerms()
    {
        return $this->addressResidenceTerms;
    }

    /**
     * @param string $addressResidenceTerms
     */
    public function setAddressResidenceTerms($addressResidenceTerms)
    {
        $this->addressResidenceTerms = $addressResidenceTerms;
    }

    /**
     * @return string
     */
    public function getContactPhonePrivate()
    {
        return $this->contactPhonePrivate;
    }

    /**
     * @param string $contactPhonePrivate
     */
    public function setContactPhonePrivate($contactPhonePrivate)
    {
        $this->contactPhonePrivate = $contactPhonePrivate;
    }

    /**
     * @return string
     */
    public function getContactPhoneMobile()
    {
        return $this->contactPhoneMobile;
    }

    /**
     * @param string $contactPhoneMobile
     */
    public function setContactPhoneMobile($contactPhoneMobile)
    {
        $this->contactPhoneMobile = $contactPhoneMobile;
    }

    /**
     * @return string
     */
    public function getContactPhoneBusiness()
    {
        return $this->contactPhoneBusiness;
    }

    /**
     * @param string $contactPhoneBusiness
     */
    public function setContactPhoneBusiness($contactPhoneBusiness)
    {
        $this->contactPhoneBusiness = $contactPhoneBusiness;
    }

    /**
     * @return string
     */
    public function getEmploymentType()
    {
        return $this->employmentType;
    }

    /**
     * @param string $employmentType
     */
    public function setEmploymentType($employmentType)
    {
        $this->employmentType = $employmentType;
    }

    /**
     * @return \DateTime
     */
    public function getEmploymentSince()
    {
        return $this->employmentSince;
    }

    /**
     * @param \DateTime $employmentSince
     */
    public function setEmploymentSince($employmentSince)
    {
        $this->employmentSince = $employmentSince;
    }

    /**
     * @return string
     */
    public function getEmploymentContractType()
    {
        return $this->employmentContractType;
    }

    /**
     * @param string $employmentContractType
     */
    public function setEmploymentContractType($employmentContractType)
    {
        $this->employmentContractType = $employmentContractType;
    }

    /**
     * @return \DateTime
     */
    public function getEmploymentUntil()
    {
        return $this->employmentUntil;
    }

    /**
     * @param \DateTime $employmentUntil
     */
    public function setEmploymentUntil($employmentUntil)
    {
        $this->employmentUntil = $employmentUntil;
    }

    /**
     * @return string
     */
    public function getEmploymentBranch()
    {
        return $this->employmentBranch;
    }

    /**
     * @param string $employmentBranch
     */
    public function setEmploymentBranch($employmentBranch)
    {
        $this->employmentBranch = $employmentBranch;
    }

    /**
     * @return string
     */
    public function getEmployerCompany()
    {
        return $this->employerCompany;
    }

    /**
     * @param string $employerCompany
     */
    public function setEmployerCompany($employerCompany)
    {
        $this->employerCompany = $employerCompany;
    }

    /**
     * @return string
     */
    public function getEmployerCity()
    {
        return $this->employerCity;
    }

    /**
     * @param string $employerCity
     */
    public function setEmployerCity($employerCity)
    {
        $this->employerCity = $employerCity;
    }

    /**
     * @return string
     */
    public function getEmployerZipCode()
    {
        return $this->employerZipCode;
    }

    /**
     * @param string $employerZipCode
     */
    public function setEmployerZipCode($employerZipCode)
    {
        $this->employerZipCode = $employerZipCode;
    }

    /**
     * @return string
     */
    public function getEmployerStreet()
    {
        return $this->employerStreet;
    }

    /**
     * @param string $employerStreet
     */
    public function setEmployerStreet($employerStreet)
    {
        $this->employerStreet = $employerStreet;
    }

    /**
     * @return string
     */
    public function getEmployerStreetNumber()
    {
        return $this->employerStreetNumber;
    }

    /**
     * @param string $employerStreetNumber
     */
    public function setEmployerStreetNumber($employerStreetNumber)
    {
        $this->employerStreetNumber = $employerStreetNumber;
    }

    /**
     * @return string
     */
    public function getIncomeMonthly()
    {
        return $this->incomeMonthly;
    }

    /**
     * @param string $incomeMonthly
     */
    public function setIncomeMonthly($incomeMonthly)
    {
        $this->incomeMonthly = $incomeMonthly;
    }

    /**
     * @return string
     */
    public function getExpensesMonthlyRent()
    {
        return $this->expensesMonthlyRent;
    }

    /**
     * @param string $expensesMonthlyRent
     */
    public function setExpensesMonthlyRent($expensesMonthlyRent)
    {
        $this->expensesMonthlyRent = $expensesMonthlyRent;
    }

    /**
     * @return string
     */
    public function getExpensesMonthlyMortgages()
    {
        return $this->expensesMonthlyMortgages;
    }

    /**
     * @param string $expensesMonthlyMortgages
     */
    public function setExpensesMonthlyMortgages($expensesMonthlyMortgages)
    {
        $this->expensesMonthlyMortgages = $expensesMonthlyMortgages;
    }

    /**
     * @return string
     */
    public function getExpensesMonthlyAliment()
    {
        return $this->expensesMonthlyAliment;
    }

    /**
     * @param string $expensesMonthlyAliment
     */
    public function setExpensesMonthlyAliment($expensesMonthlyAliment)
    {
        $this->expensesMonthlyAliment = $expensesMonthlyAliment;
    }

    /**
     * @return string
     */
    public function getExpensesMonthlyMisc()
    {
        return $this->expensesMonthlyMisc;
    }

    /**
     * @param string $expensesMonthlyMisc
     */
    public function setExpensesMonthlyMisc($expensesMonthlyMisc)
    {
        $this->expensesMonthlyMisc = $expensesMonthlyMisc;
    }

    /**
     * @return string
     */
    public function getBankAccountOwner()
    {
        return $this->bankAccountOwner;
    }

    /**
     * @param string $bankAccountOwner
     */
    public function setBankAccountOwner($bankAccountOwner)
    {
        $this->bankAccountOwner = $bankAccountOwner;
    }

    /**
     * @return string
     */
    public function getBankAccountNumber()
    {
        return $this->bankAccountNumber;
    }

    /**
     * @param string $bankAccountNumber
     */
    public function setBankAccountNumber($bankAccountNumber)
    {
        $this->bankAccountNumber = $bankAccountNumber;
    }

    /**
     * @return string
     */
    public function getBankAccountBankCode()
    {
        return $this->bankAccountBankCode;
    }

    /**
     * @param string $bankAccountBankCode
     */
    public function setBankAccountBankCode($bankAccountBankCode)
    {
        $this->bankAccountBankCode = $bankAccountBankCode;
    }

    /**
     * @return string
     */
    public function getBankAccountBankName()
    {
        return $this->bankAccountBankName;
    }

    /**
     * @param string $bankAccountBankName
     */
    public function setBankAccountBankName($bankAccountBankName)
    {
        $this->bankAccountBankName = $bankAccountBankName;
    }

    /**
     * @return string
     */
    public function getBankAccountBankCity()
    {
        return $this->bankAccountBankCity;
    }

    /**
     * @param string $bankAccountBankCity
     */
    public function setBankAccountBankCity($bankAccountBankCity)
    {
        $this->bankAccountBankCity = $bankAccountBankCity;
    }
}
