<?php
namespace Payever\CommonBundle\Entity\Payment;

use Doctrine\ORM\Mapping as ORM;
use Payever\CommonBundle\Entity\CustomerDetails;
use Payever\CommonBundle\Entity\MerchantDetails;

/**
 * @ORM\Entity(
 *      repositoryClass="Payever\CommonBundle\Impl\Repository\PaymentRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="payment_type", type="string")
 * @ORM\DiscriminatorMap({
 *      "installments" = "InstallmentsPayment",
 *      "bill_payment" = "BillPayment"
 * })
 */
abstract class BasePayment
{
    /**
     * @return string
     */
    public static function getClass()
    {
        return get_class();
    }

    /**
     * @return string
     */
    abstract public function getType();

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=64, nullable=false)
     */
    protected $status;

    /**
     * Foreign key to MerchantOnlineShop
     *
     * @var MerchantDetails
     *
     * @ORM\ManyToOne(targetEntity="Payever\CommonBundle\Entity\MerchantOnlineShop", inversedBy="payments", cascade="all")
     * @ORM\JoinColumn(name="merchant_details_id", referencedColumnName="id", onDelete="cascade")
     */
    private $onlineShop;

    /**
     * Foreign key to CustomerDetails
     *
     * @var CustomerDetails
     *
     * @ORM\ManyToOne(targetEntity="Payever\CommonBundle\Entity\CustomerDetails", inversedBy="payments", cascade="all")
     * @ORM\JoinColumn(name="customer_details_id", referencedColumnName="id", onDelete="cascade")
     */
    private $customerDetails;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="credit_reference", type="string", length=255, nullable=true)
     */
    protected $creditReference;

    /**
     * @var string
     *
     * @ORM\Column(name="credit_amount", type="string", length=64, nullable=true)
     */
    protected $creditAmount;

    /**
     *
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return \Payever\CommonBundle\Entity\MerchantOnlineShop
     */
    public function getOnlineShop()
    {
        return $this->onlineShop;
    }

    /**
     * @param \Payever\CommonBundle\Entity\MerchantOnlineShop $onlineShop
     */
    public function setOnlineShop($onlineShop)
    {
        $this->onlineShop = $onlineShop;
    }

    /**
     * @return \Payever\CommonBundle\Entity\CustomerDetails
     */
    public function getCustomerDetails()
    {
        return $this->customerDetails;
    }

    /**
     * @param \Payever\CommonBundle\Entity\CustomerDetails $customerDetails
     */
    public function setCustomerDetails($customerDetails)
    {
        $this->customerDetails = $customerDetails;
    }

    /**
     * @return string
     */
    public function getCreditReference()
    {
        return $this->creditReference;
    }

    /**
     * @param string $creditReference
     */
    public function setCreditReference($creditReference)
    {
        $this->creditReference = $creditReference;
    }

    /**
     * @return string
     */
    public function getCreditAmount()
    {
        return $this->creditAmount;
    }

    /**
     * @param string $creditAmount
     */
    public function setCreditAmount($creditAmount)
    {
        $this->creditAmount = $creditAmount;
    }

    /**
     * @return string
     */
    public function getPaymentType()
    {
        return $this->payment_type;
    }

    /**
     * @param string $payment_type
     */
    public function setPaymentType($payment_type)
    {
        $this->payment_type = $payment_type;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }
}
