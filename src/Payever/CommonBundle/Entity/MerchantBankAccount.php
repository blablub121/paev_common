<?php
namespace Payever\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Payever\CommonBundle\Impl\Serialization\AbstractSerializableObject;
use Payever\CommonBundle\Interfaces\Model\Merchant\MerchantBankAccountInterface;

/**
 * MerchantBankAccount
 *
 * @ORM\Table(name="merchant_bank_account")
 * @ORM\Entity(
 *      repositoryClass="Payever\CommonBundle\Impl\Repository\MerchantBankAccountRepository"
 * )
 */
class MerchantBankAccount extends AbstractSerializableObject implements MerchantBankAccountInterface
{
    /**
     * @return string
     */
    public static function getClass()
    {
        return get_class();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var MerchantDetails
     *
     * Foreign key to MerchantDetails
     *
     * @ORM\ManyToOne(targetEntity="MerchantDetails", inversedBy="bankAccounts", cascade="all")
     * @ORM\JoinColumn(name="merchant_details_id", referencedColumnName="id", onDelete="cascade")
     */
    private $merchantDetails;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_primary", type="boolean", nullable=false)
     */
    private $isPrimary = false;

    /**
     * @var string
     *
     * @ORM\Column(name="bank_name", type="string", length=255, nullable=false)
     */
    private $bankName = "";

    /**
     * @var string
     *
     * @ORM\Column(name="bank_code", type="string", length=64, nullable=false)
     */
    private $bankCode = "";

    /**
     * @var string
     *
     * @ORM\Column(name="owner", type="string", length=255, nullable=false)
     */
    private $owner = "";

    /**
     * @var string
     *
     * @ORM\Column(name="account_number", type="string", length=64, nullable=false)
     */
    private $accountNumber = "";

    /**
     * @var string
     *
     * @ORM\Column(name="iban", type="string", length=64, nullable=false)
     */
    private $iban = "";

    /**
     * @var string
     *
     * @ORM\Column(name="bic", type="string", length=64, nullable=false)
     */
    private $bic = "";

    /**
     * C'tor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('NOW');
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return MerchantDetails
     */
    public function getMerchantDetails()
    {
        return $this->merchantDetails;
    }

    /**
     * @param MerchantDetails $merchantDetails
     */
    public function setMerchantDetails(MerchantDetails $merchantDetails)
    {
        $this->merchantDetails = $merchantDetails;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return boolean
     */
    public function getIsPrimary()
    {
        return $this->isPrimary;
    }

    /**
     * @param boolean $isPrimary
     */
    public function setIsPrimary($isPrimary)
    {
        $this->isPrimary = $isPrimary ? true : false;
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param string $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * @return string
     */
    public function getBankCode()
    {
        return $this->bankCode;
    }

    /**
     * @param string $bankCode
     */
    public function setBankCode($bankCode)
    {
        $this->bankCode = $bankCode;
    }

    /**
     * @return string
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param string $owner
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @param string $accountNumber
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;
    }

    /**
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * @param string $iban
     */
    public function setIban($iban)
    {
        $this->iban = $iban;
    }

    /**
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * @param string $bic
     */
    public function setBic($bic)
    {
        $this->bic = $bic;
    }
}
