<?php
namespace Payever\CommonBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Payever\CommonBundle\Impl\Serialization\AbstractSerializableObject;
use Payever\CommonBundle\Interfaces\Model\Merchant\MerchantOnlineShopInterface;

/**
 * MerchantOnlineShop
 *
 * @ORM\Table(name="merchant_online_shop")
 * @ORM\Entity(
 *      repositoryClass="Payever\CommonBundle\Impl\Repository\MerchantOnlineShopRepository"
 * )
 */
class MerchantOnlineShop extends AbstractSerializableObject implements MerchantOnlineShopInterface
{
    /**
     * @return string
     */
    public static function getClass()
    {
        return get_class();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Foreign key to MerchantDetails
     *
     * @var MerchantDetails
     *
     * @ORM\ManyToOne(targetEntity="MerchantDetails", inversedBy="onlineShops", cascade="all")
     * @ORM\JoinColumn(name="merchant_details_id", referencedColumnName="id", onDelete="cascade")
     */
    private $merchantDetails;

    /**
     * @var Payment\BasePayment[]
     *
     * @ORM\OneToMany(targetEntity="Payever\CommonBundle\Entity\Payment\BasePayment", mappedBy="onlineShop", cascade="all")
     */
    private $payments;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name = "";

    /**
     * @var string
     *
     * @ORM\Column(name="logo_filename", type="string", length=255, nullable=false)
     */
    private $logoFilename = "";

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=64, nullable=false)
     */
    private $phoneNumber = "";

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url = "";

    /**
     * "Branche"
     *
     * @var string
     *
     * @ORM\Column(name="sector", type="string", length=255, nullable=false)
     */
    private $sector = "";

    /**
     * @var string
     *
     * @ORM\Column(name="products", type="text", nullable=false)
     */
    private $products = "";

    /**
     * @var float
     *
     * @ORM\Column(name="avg_order_value", type="decimal", nullable=false)
     */
    private $avgOrderValue = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="annual_sales_value", type="decimal", nullable=false)
     */
    private $annualSalesValue = 0;

    /**
     * Is "Ratenzahlung" accepted for this shop?
     *
     * @var boolean
     *
     * @ORM\Column(name="accepts_installment_payment", type="boolean", nullable=false)
     */
    private $acceptsInstallmentPayment = false;

    /**
     * True, when shop owner pays the fees of "Ratenzahlung"
     *
     * @var boolean
     *
     * @ORM\Column(name="pays_installment_payment_fees", type="boolean", nullable=false)
     */
    private $paysInstallmentPaymentFees = false;

    /**
     * Is "Rechnungszahlen" accepted for this shop?
     *
     * @var boolean
     *
     * @ORM\Column(name="accepts_bill_payment", type="boolean", nullable=false)
     */
    private $acceptsBillPayment = false;

    /**
     * True, when shop owner pays the fees of "Rechnungszahlung"
     *
     * @var boolean
     *
     * @ORM\Column(name="pays_bill_payment_fees", type="boolean", nullable=false)
     */
    private $paysBillPaymentFees = false;

    /**
     * C'tor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('NOW');
        $this->payments = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return MerchantDetails
     */
    public function getMerchantDetails()
    {
        return $this->merchantDetails;
    }

    /**
     * @param MerchantDetails $merchantDetails
     */
    public function setMerchantDetails(MerchantDetails $merchantDetails)
    {
        $this->merchantDetails = $merchantDetails;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLogoFilename()
    {
        return $this->logoFilename;
    }

    /**
     * @param string $logoFilename
     */
    public function setLogoFilename($logoFilename)
    {
        $this->logoFilename = $logoFilename;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getSector()
    {
        return $this->sector;
    }

    /**
     * @param string $sector
     */
    public function setSector($sector)
    {
        $this->sector = $sector;
    }

    /**
     * @return string
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param string $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return float
     */
    public function getAvgOrderValue()
    {
        return $this->avgOrderValue;
    }

    /**
     * @param float $avgOrderValue
     */
    public function setAvgOrderValue($avgOrderValue)
    {
        $this->avgOrderValue = $avgOrderValue;
    }

    /**
     * @return float
     */
    public function getAnnualSalesValue()
    {
        return $this->annualSalesValue;
    }

    /**
     * @param float $annualSalesValue
     */
    public function setAnnualSalesValue($annualSalesValue)
    {
        $this->annualSalesValue = $annualSalesValue;
    }

    /**
     * @return boolean
     */
    public function getAcceptsInstallmentPayment()
    {
        return $this->acceptsInstallmentPayment;
    }

    /**
     * @param boolean $acceptsInstallmentPayment
     */
    public function setAcceptsInstallmentPayment($acceptsInstallmentPayment)
    {
        $this->acceptsInstallmentPayment = $acceptsInstallmentPayment ? true : false;
    }

    /**
     * @return boolean
     */
    public function getAcceptsBillPayment()
    {
        return $this->acceptsBillPayment;
    }

    /**
     * @param boolean $acceptsBillPayment
     */
    public function setAcceptsBillPayment($acceptsBillPayment)
    {
        $this->acceptsBillPayment = $acceptsBillPayment ? true : false;
    }

    /**
     * @return boolean
     */
    public function getPaysInstallmentPaymentFees()
    {
        return $this->paysInstallmentPaymentFees;
    }

    /**
     * @param boolean $paysInstallmentPaymentFees
     */
    public function setPaysInstallmentPaymentFees($paysInstallmentPaymentFees)
    {
        $this->paysInstallmentPaymentFees = $paysInstallmentPaymentFees ? true : false;
    }

    /**
     * @return boolean
     */
    public function getPaysBillPaymentFees()
    {
        return $this->paysBillPaymentFees;
    }

    /**
     * @param boolean $paysBillPaymentFees
     */
    public function setPaysBillPaymentFees($paysBillPaymentFees)
    {
        $this->paysBillPaymentFees = $paysBillPaymentFees ? true : false;
    }

    /**
     * @return \Payever\CommonBundle\Entity\Payment\BasePayment[]
     */
    public function getPayments()
    {
        return $this->payments;
    }

    /**
     * @param \Payever\CommonBundle\Entity\Payment\BasePayment[] $payments
     */
    public function setPayments($payments)
    {
        $this->payments = $payments;
    }
}
