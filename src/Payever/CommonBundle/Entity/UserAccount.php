<?php
namespace Payever\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Payever\CommonBundle\Impl\Serialization\AbstractSerializableObject;
use Payever\CommonBundle\Interfaces\Model\User\UserAccountInterface;

/**
 * UserAccount
 *
 * @ORM\Table(
 *      name="user_account",
 *      uniqueConstraints = {
 *          @ORM\UniqueConstraint(name="idx_user_account_hash", columns={"hash"})
 *      }
 * )
 * @ORM\Entity(
 *      repositoryClass="Payever\CommonBundle\Impl\Repository\UserAccountRepository"
 * )
 */
class UserAccount extends AbstractSerializableObject implements UserAccountInterface
{
    /**
     * @return string
     */
    public static function getClass()
    {
        return get_class();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=32, nullable=false)
     */
    private $hash;

    /**
     * @var int
     *
     * @ORM\Column(name="blocked", type="smallint", nullable=false)
     */
    private $blocked = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint", nullable=false)
     */
    private $status = self::STATUS_AWAITING_ACTIVATION;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=64, nullable=false)
     */
    private $type = self::TYPE_DEFAULT;

    /**
     * @var string
     *
     * @ORM\Column(name="salutation", type="string", length=255, nullable=true)
     */
    private $salutation;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="middle_name", type="string", length=255, nullable=true)
     */
    private $middleName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password_salt", type="string", length=255, nullable=true)
     */
    private $passwordSalt;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     */
    private $birthday;

    /**
     * @var bool
     *
     * @ORM\Column(name="accepts_terms_and_conditions", type="boolean", nullable=true)
     */
    private $acceptsTermsAndConditions = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="accepts_privacy_policy", type="boolean", nullable=true)
     */
    private $acceptsPrivacyPolicy = false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * Set blocked
     *
     * @param int $blocked
     * @return $this
     */
    public function setBlocked($blocked)
    {
        $this->blocked = $blocked ? 1 : 0;

        return $this;
    }

    /**
     * Get blocked
     *
     * @return int
     */
    public function getBlocked()
    {
        return $this->blocked;
    }

    /**
     * Set status
     *
     * @param int $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set salutation
     *
     * @param string $salutation
     * @return $this
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;

        return $this;
    }

    /**
     * Get salutation
     *
     * @return string
     */
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return $this
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set middleName
     *
     * @param string $middleName
     * @return $this
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;

        return $this;
    }

    /**
     * Get middleName
     *
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return $this
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password salt
     *
     * @param string $passwordSalt
     * @return $this
     */
    public function setPasswordSalt($passwordSalt)
    {
        $this->passwordSalt = $passwordSalt;

        return $this;
    }

    /**
     * Get password salt
     *
     * @return string
     */
    public function getPasswordSalt()
    {
        return $this->passwordSalt;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return $this
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @return boolean
     */
    public function getAcceptsTermsAndConditions()
    {
        return $this->acceptsTermsAndConditions;
    }

    /**
     * @param boolean $acceptsTermsAndConditions
     */
    public function setAcceptsTermsAndConditions($acceptsTermsAndConditions)
    {
        $this->acceptsTermsAndConditions = $acceptsTermsAndConditions;
    }

    /**
     * @return boolean
     */
    public function getAcceptsPrivacyPolicy()
    {
        return $this->acceptsPrivacyPolicy;
    }

    /**
     * @param boolean $acceptsPrivacyPolicy
     */
    public function setAcceptsPrivacyPolicy($acceptsPrivacyPolicy)
    {
        $this->acceptsPrivacyPolicy = $acceptsPrivacyPolicy;
    }

    /**
     * @return bool
     */
    public function isActivated()
    {
        return $this->status === UserAccountInterface::STATUS_ACTIVE;
    }
}
