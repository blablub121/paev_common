<?php
namespace Payever\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Payever\CommonBundle\Impl\Serialization\AbstractSerializableObject;
use Payever\CommonBundle\Interfaces\Model\User\UserAccountInterface;
use Payever\CommonBundle\Interfaces\Model\Crm\CrmDataMerchantInterface;
use Payever\CommonBundle\Interfaces\Model\Merchant\MerchantAddressInterface;
use Payever\CommonBundle\Interfaces\Model\Merchant\MerchantBankAccountInterface;
use Payever\CommonBundle\Interfaces\Model\Merchant\MerchantDetailsInterface;
use Payever\CommonBundle\Interfaces\Model\Merchant\MerchantOnlineShopInterface;

/**
 * MerchantDetails
 *
 * @ORM\Table(
 *      name="merchant_details",
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(name="uniq_idx_user_account_id", columns={"user_account_id"})
 *      }
 * )
 * @ORM\Entity(
 *      repositoryClass="Payever\CommonBundle\Impl\Repository\MerchantDetailsRepository"
 * )
 */
class MerchantDetails extends AbstractSerializableObject implements MerchantDetailsInterface
{
    /**
     * @return string
     */
    public static function getClass()
    {
        return get_class();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var UserAccountInterface
     *
     * @ORM\OneToOne(targetEntity="UserAccount", cascade="all")
     * @ORM\JoinColumn(name="user_account_id", referencedColumnName="id")
     */
    private $userAccount;

    /**
     * @var CrmDataMerchant
     *
     * @ORM\OneToOne(targetEntity="CrmDataMerchant", mappedBy="merchantDetails", cascade="all")
     */
    private $crmData;

    /**
     * @var MerchantAddress[]
     *
     * @ORM\OneToMany(targetEntity="MerchantAddress", mappedBy="merchantDetails", cascade="all")
     */
    private $addresses;

    /**
     * @var MerchantBankAccount[]
     *
     * @ORM\OneToMany(targetEntity="MerchantBankAccount", mappedBy="merchantDetails", cascade="all")
     */
    private $bankAccounts;

    /**
     * @var MerchantOnlineShop[]
     *
     * @ORM\OneToMany(targetEntity="MerchantOnlineShop", mappedBy="merchantDetails", cascade="all")
     */
    private $onlineShops;

    /**
     * @var int
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=255, nullable=false)
     */
    private $companyName = "";

    /**
     * @var string
     *
     * @ORM\Column(name="legal_form", type="string", length=255, nullable=false)
     */
    private $legalForm = "";

    /**
     * @var string
     *
     * @ORM\Column(name="chief_title", type="string", length=255, nullable=false)
     */
    private $chiefTitle = "";

    /**
     * @var string
     *
     * @ORM\Column(name="chief_first_name", type="string", length=255, nullable=false)
     */
    private $chiefFirstName = "";

    /**
     * @var string
     *
     * @ORM\Column(name="chief_middle_name", type="string", length=255, nullable=false)
     */
    private $chiefMiddleName = "";

    /**
     * @var string
     *
     * @ORM\Column(name="chief_last_name", type="string", length=255, nullable=false)
     */
    private $chiefLastName = "";

    /**
     * "Handelsregisternummer"
     *
     * @var string
     *
     * @ORM\Column(name="commercial_register_number", type="string", length=255, nullable=false)
     */
    private $commercialRegisterNumber = "";

    /**
     * "Umsatzsteuernummer"
     *
     * @var string
     *
     * @ORM\Column(name="trade_id", type="string", length=255, nullable=false)
     */
    private $tradeId = "";

    /**
     * @var boolean
     *
     * @ORM\Column(name="sells_b2b", type="boolean", nullable=false)
     */
    private $sellsB2B = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sells_b2c", type="boolean", nullable=false)
     */
    private $sellsB2C = false;

    /**
     * C'tor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime('NOW');
        $this->addresses = new ArrayCollection();
        $this->bankAccounts = new ArrayCollection();
        $this->onlineShops = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return UserAccountInterface
     */
    public function getUserAccount()
    {
        return $this->userAccount;
    }

    /**
     * @param UserAccountInterface $userAccount
     */
    public function setUserAccount(UserAccountInterface $userAccount)
    {
        $this->userAccount = $userAccount;
    }

    /**
     * @return CrmDataMerchant
     */
    public function getCrmData()
    {
        return $this->crmData;
    }

    /**
     * @param CrmDataMerchantInterface $crmDataMerchant
     */
    public function setCrmData(CrmDataMerchantInterface $crmDataMerchant)
    {
        $this->crmData = $crmDataMerchant;
    }

    /**
     * @return MerchantAddress[]
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * @param MerchantAddressInterface $address
     */
    public function addAddress(MerchantAddressInterface $address)
    {
        $this->addresses->add($address);
    }

    /**
     * @return MerchantAddressInterface|null
     */
    public function getPrimaryAddress()
    {
        foreach ($this->addresses as $address) {
            if ($address->getIsPrimary()) {
                return $address;
            }
        }

        return null;
    }

    /**
     * @return MerchantBankAccountInterface[]
     */
    public function getBankAccounts()
    {
        return $this->bankAccounts;
    }

    /**
     * @param MerchantBankAccountInterface $bankAccount
     */
    public function addBankAccount(MerchantBankAccountInterface $bankAccount)
    {
        $this->bankAccounts->add($bankAccount);
    }

    /**
     * @return MerchantBankAccountInterface|null
     */
    public function getPrimaryBankAccount()
    {
        foreach ($this->bankAccounts as $bankAccount) {
            if ($bankAccount->getIsPrimary()) {
                return $bankAccount;
            }
        }

        return null;
    }

    /**
     * @return MerchantOnlineShop[]
     */
    public function getOnlineShops()
    {
        return $this->onlineShops;
    }

    /**
     * @param MerchantOnlineShopInterface $onlineShop
     */
    public function addOnlineShop(MerchantOnlineShopInterface $onlineShop)
    {
        $this->onlineShops->add($onlineShop);
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return string
     */
    public function getLegalForm()
    {
        return $this->legalForm;
    }

    /**
     * @param string $legalForm
     */
    public function setLegalForm($legalForm)
    {
        $this->legalForm = $legalForm;
    }

    /**
     * @return string
     */
    public function getChiefTitle()
    {
        return $this->chiefTitle;
    }

    /**
     * @param string $chiefTitle
     */
    public function setChiefTitle($chiefTitle)
    {
        $this->chiefTitle = $chiefTitle;
    }

    /**
     * @return string
     */
    public function getChiefFirstName()
    {
        return $this->chiefFirstName;
    }

    /**
     * @param string $chiefFirstName
     */
    public function setChiefFirstName($chiefFirstName)
    {
        $this->chiefFirstName = $chiefFirstName;
    }

    /**
     * @return string
     */
    public function getChiefMiddleName()
    {
        return $this->chiefMiddleName;
    }

    /**
     * @param string $chiefMiddleName
     */
    public function setChiefMiddleName($chiefMiddleName)
    {
        $this->chiefMiddleName = $chiefMiddleName;
    }

    /**
     * @return string
     */
    public function getChiefLastName()
    {
        return $this->chiefLastName;
    }

    /**
     * @param string $chiefLastName
     */
    public function setChiefLastName($chiefLastName)
    {
        $this->chiefLastName = $chiefLastName;
    }

    /**
     * @return string
     */
    public function getCommercialRegisterNumber()
    {
        return $this->commercialRegisterNumber;
    }

    /**
     * @param string $commercialRegisterNumber
     */
    public function setCommercialRegisterNumber($commercialRegisterNumber)
    {
        $this->commercialRegisterNumber = $commercialRegisterNumber;
    }

    /**
     * @return string
     */
    public function getTradeId()
    {
        return $this->tradeId;
    }

    /**
     * @param string $tradeId
     */
    public function setTradeId($tradeId)
    {
        $this->tradeId = $tradeId;
    }

    /**
     * @return boolean
     */
    public function getSellsB2B()
    {
        return $this->sellsB2B;
    }

    /**
     * @param boolean $sellsB2B
     */
    public function setSellsB2B($sellsB2B)
    {
        $this->sellsB2B = $sellsB2B ? true : false;
    }

    /**
     * @return boolean
     */
    public function getSellsB2C()
    {
        return $this->sellsB2C;
    }

    /**
     * @param boolean $sellsB2C
     */
    public function setSellsB2C($sellsB2C)
    {
        $this->sellsB2C = $sellsB2C ? true : false;
    }
}
