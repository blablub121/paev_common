<?php
namespace Payever\CommonBundle\Interfaces\Services;

/**
 * Class TokenGeneratorInterface
 *
 * @package Payever\CommonBundle\Interfaces
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface TokenGeneratorInterface
{
    /**
     * Generates an Account Activation token
     *
     * @return string
     */
    public function generateAccountActivationToken();

    /**
     * Generates a Reset Password token
     *
     * @return string
     */
    public function generateResetPasswordToken();
}
