<?php
namespace Payever\CommonBundle\Interfaces\Services;

/**
 * Class AuthenticationServiceInterface
 *
 * @package Payever\CommonBundle\Interfaces\Services
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface AuthenticationServiceInterface
{
    /**
     * @param string    $username
     * @param string    $password
     * @param string    $firewall
     *
     * @return bool
     */
    public function loginUser($username, $password, $firewall = 'main');
}
