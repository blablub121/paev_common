<?php
namespace Payever\CommonBundle\Interfaces\Services;

use Payever\CommonBundle\Interfaces\Model\User\UserAccountInterface;
use Payever\CommonBundle\Interfaces\Model\User\UserDetailsInterface;

/**
 * Class UserLoaderInterface
 *
 * @package Payever\CommonBundle\Interfaces\Services
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface UserLoaderInterface
{
    /**
     * @param $email
     *
     * @return UserAccountInterface|null
     */
    public function loadUserByEmail($email);

    /**
     * @param string $uid
     *
     * @return UserDetailsInterface
     */
    public function loadUserDetailsByEmail($uid);

    /**
     * @param string $uid
     *
     * @return UserDetailsInterface
     */
    public function loadUserDetailsByUid($uid);

    /**
     * @param UserAccountInterface $user
     *
     * @return UserDetailsInterface
     */
    public function loadUserDetailsByUserAccount(UserAccountInterface $user);
}
