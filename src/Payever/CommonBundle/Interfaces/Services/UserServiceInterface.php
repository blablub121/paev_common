<?php
namespace Payever\CommonBundle\Interfaces\Services;

use Payever\CommonBundle\Impl\Exceptions\ServiceException;
use Payever\CommonBundle\Interfaces\Model\User\UserAccountActivationTokenInterface;
use Payever\CommonBundle\Interfaces\Model\User\UserAccountInterface;
use Payever\CommonBundle\Interfaces\Model\User\UserAccountPasswordTokenInterface;
use Payever\ApplicationFrontendBundle\Impl\Forms\Account\Objects\RegisterUserData;

/**
 * Interface UserServiceInterface
 *
 * @package Payever\ApplicationFrontendBundle\Interfaces\Services
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface UserServiceInterface
{

    /**
     * @param RegisterUserData $data
     * @param int              $userStatus
     *
     * @return UserAccountInterface
     * @throws ServiceException
     */
    public function registerUserAndSendActivationMail(RegisterUserData $data, $userStatus = UserAccountInterface::STATUS_AWAITING_ACTIVATION);

    /**
     * @param RegisterUserData $data
     * @param int              $userStatus
     *
     * @return UserAccountInterface
     * @throws ServiceException
     */
    public function registerUser(RegisterUserData $data, $userStatus = UserAccountInterface::STATUS_AWAITING_ACTIVATION);

    /**
     * @param $email
     *
     * @return UserAccountActivationTokenInterface
     * @throws ServiceException
     */
    public function getActivationToken($email);

    /**
     * @param string    $token
     *
     * @return bool
     * @throws ServiceException
     */
    public function applyActivationToken($token);

    /**
     * @param string    $email
     *
     * @return UserAccountPasswordTokenInterface
     * @throws ServiceException
     */
    public function getForgotPasswordToken($email);

    /**
     * @param string    $token
     * @param string    $newPassword
     *
     * @return bool
     * @throws ServiceException
     */
    public function applyForgotPasswordToken($token, $newPassword);

    /**
     * @param string $token
     *
     * @return bool
     * @throws ServiceException
     */
    public function denyForgotPasswordToken($token);
}
