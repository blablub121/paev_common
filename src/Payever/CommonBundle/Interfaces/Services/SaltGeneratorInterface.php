<?php
namespace Payever\CommonBundle\Interfaces\Services;

/**
 * Class SaltGeneratorInterface
 *
 * @package Payever\CommonBundle\Interfaces\Services
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface SaltGeneratorInterface
{
    /**
     * @return string
     */
    public function generateSalt();
}
