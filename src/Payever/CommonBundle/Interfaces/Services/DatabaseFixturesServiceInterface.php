<?php
namespace Payever\CommonBundle\Interfaces\Services;

use Payever\CommonBundle\Impl\Exceptions\DatabaseFixtureServiceException;

/**
 * Class DatabaseFixturesServiceInterface
 *
 * @package Payever\CommonBundle\Interfaces\Services
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface DatabaseFixturesServiceInterface
{

    /**
     * @param string    $fixtureName
     *
     * @throws DatabaseFixtureServiceException
     */
    public function applyFixture($fixtureName);

    /**
     * @param string $fixtureName
     *
     * @throws DatabaseFixtureServiceException
     *
     * @return string
     */
    public function getFixtureFilePath($fixtureName);

    /**
     * @return string
     */
    public function getDatabaseName();
}
