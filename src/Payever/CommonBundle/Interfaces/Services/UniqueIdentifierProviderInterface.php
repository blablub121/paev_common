<?php
namespace Payever\CommonBundle\Interfaces\Services;

/**
 * Class UniqueIdentifierProviderInterface
 *
 * @package Payever\CommonBundle\Interfaces\Services
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface UniqueIdentifierProviderInterface
{
    /**
     * @return string   A system wide unique hash
     */
    public function getNextHash();
}
