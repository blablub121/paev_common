<?php
namespace Payever\CommonBundle\Interfaces\Services;

/**
 * Class LocatorServiceInterface
 *
 * @package Payever\CommonBundle\Interfaces\Services
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface LocatorServiceInterface
{
    /**
     * @param string    $resourceName   The name of a resource
     *
     * @return string   File path of the resource
     */
    public function locateResource($resourceName);
}
