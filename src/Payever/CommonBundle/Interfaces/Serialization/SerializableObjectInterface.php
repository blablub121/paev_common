<?php
namespace Payever\CommonBundle\Interfaces\Serialization;

/**
 * Class SerializableObjectInterface
 *
 * @package Payever\CommonBundle\Interfaces\Serialization
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface SerializableObjectInterface
{
    /**
     * Walks through all private properties and puts them into a \stdClass instance keeping the properties names
     *
     * @return \stdClass
     */
    public function toStdClass();
}
