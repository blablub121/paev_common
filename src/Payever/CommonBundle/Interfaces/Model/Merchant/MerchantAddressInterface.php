<?php
namespace Payever\CommonBundle\Interfaces\Model\Merchant;

use Payever\CommonBundle\Interfaces\Reflector\ReflectiveClassInterface;

/**
 * Class MerchantAddressInterface
 *
 * @package Payever\CommonBundle\Interfaces\Model\Merchant
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface MerchantAddressInterface extends ReflectiveClassInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return int
     */
    public function getCreatedAt();

    /**
     * @return boolean
     */
    public function getIsPrimary();

    /**
     * @param boolean $isPrimary
     */
    public function setIsPrimary($isPrimary);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     */
    public function setTitle($title);

    /**
     * @return string
     */
    public function getFirstName();

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName);

    /**
     * @return string
     */
    public function getMiddleName();

    /**
     * @param string $middleName
     */
    public function setMiddleName($middleName);

    /**
     * @return string
     */
    public function getLastName();

    /**
     * @param string $lastName
     */
    public function setLastName($lastName);

    /**
     * @return string
     */
    public function getCountry();

    /**
     * @param string $country
     */
    public function setCountry($country);

    /**
     * @return string
     */
    public function getCity();

    /**
     * @param string $city
     */
    public function setCity($city);

    /**
     * @return string
     */
    public function getZipCode();

    /**
     * @param string $zipCode
     */
    public function setZipCode($zipCode);

    /**
     * @return string
     */
    public function getStreet();

    /**
     * @param string $street
     */
    public function setStreet($street);

    /**
     * @return string
     */
    public function getNumber();

    /**
     * @param string $number
     */
    public function setNumber($number);

    /**
     * @return string
     */
    public function getEmailAddress();

    /**
     * @param string $emailAddress
     */
    public function setEmailAddress($emailAddress);

    /**
     * @return string
     */
    public function getPhoneNumber();

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber);

    /**
     * @return string
     */
    public function getMobilePhoneNumber();

    /**
     * @param string $mobilePhoneNumber
     */
    public function setMobilePhoneNumber($mobilePhoneNumber);

    /**
     * @return string
     */
    public function getFaxNumber();

    /**
     * @param string $faxNumber
     */
    public function setFaxNumber($faxNumber);
}
