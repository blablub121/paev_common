<?php
namespace Payever\CommonBundle\Interfaces\Model\Merchant;

use Payever\CommonBundle\Interfaces\Reflector\ReflectiveClassInterface;

/**
 * Class MerchantBankAccountInterface
 *
 * @package Payever\CommonBundle\Interfaces\Model\Merchant
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface MerchantBankAccountInterface extends ReflectiveClassInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return int
     */
    public function getCreatedAt();

    /**
     * @return boolean
     */
    public function getIsPrimary();

    /**
     * @param boolean $isPrimary
     */
    public function setIsPrimary($isPrimary);

    /**
     * @return string
     */
    public function getBankName();

    /**
     * @param string $bankName
     */
    public function setBankName($bankName);

    /**
     * @return string
     */
    public function getBankCode();

    /**
     * @param string $bankCode
     */
    public function setBankCode($bankCode);

    /**
     * @return string
     */
    public function getOwner();

    /**
     * @param string $owner
     */
    public function setOwner($owner);

    /**
     * @return string
     */
    public function getAccountNumber();

    /**
     * @param string $accountNumber
     */
    public function setAccountNumber($accountNumber);

    /**
     * @return string
     */
    public function getIban();

    /**
     * @param string $iban
     */
    public function setIban($iban);

    /**
     * @return string
     */
    public function getBic();

    /**
     * @param string $bic
     */
    public function setBic($bic);
}
