<?php
namespace Payever\CommonBundle\Interfaces\Model\Merchant;

use Payever\CommonBundle\Interfaces\Model\User\UserDetailsInterface;
use Payever\CommonBundle\Interfaces\Reflector\ReflectiveClassInterface;
use Payever\CommonBundle\Interfaces\Model\Crm\CrmDataMerchantInterface;
use Payever\CommonBundle\Interfaces\Model\User\UserAccountInterface;

/**
 * Class MerchantDetailsInterface
 *
 * @package Payever\CommonBundle\Interfaces\Model\Merchant
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface MerchantDetailsInterface extends UserDetailsInterface, ReflectiveClassInterface
{
    /**
     * @return UserAccountInterface
     */
    public function getUserAccount();

    /**
     * @param UserAccountInterface $userAccount
     */
    public function setUserAccount(UserAccountInterface $userAccount);

    /**
     * @return CrmDataMerchantInterface
     */
    public function getCrmData();

    /**
     * @param CrmDataMerchantInterface $crmData
     */
    public function setCrmData(CrmDataMerchantInterface $crmData);

    /**
     * @return MerchantAddressInterface[]
     */
    public function getAddresses();

    /**
     * @param MerchantAddressInterface $address
     */
    public function addAddress(MerchantAddressInterface $address);

//    /**
//     * @param MerchantAddressInterface[] $addresses
//     */
//    public function setAddresses($addresses);
//
    /**
     * @return MerchantAddressInterface|null
     */
    public function getPrimaryAddress();
//
//    /**
//     * @param MerchantAddressInterface $address
//     */
//    public function setPrimaryAddress(MerchantAddressInterface $address);

    /**
     * @return MerchantBankAccountInterface[]
     */
    public function getBankAccounts();

    /**
     * @param MerchantBankAccountInterface $bankAccount
     */
    public function addBankAccount(MerchantBankAccountInterface $bankAccount);

//    /**
//     * @param MerchantBankAccountInterface[] $bankAccounts
//     */
//    public function setBankAccounts($bankAccounts);
//
    /**
     * @return MerchantBankAccountInterface|null
     */
    public function getPrimaryBankAccount();
//
//    /**
//     * @param MerchantBankAccountInterface $bankAccount
//     */
//    public function setPrimaryBankAccount(MerchantBankAccountInterface $bankAccount);

    /**
     * @return MerchantOnlineShopInterface[]
     */
    public function getOnlineShops();

    /**
     * @param MerchantOnlineShopInterface $onlineShop
     */
    public function addOnlineShop(MerchantOnlineShopInterface $onlineShop);

//    /**
//     * @param MerchantOnlineShopInterface[] $onlineShops
//     */
//    public function setOnlineShops($onlineShops);

    /**
     * @return int
     */
    public function getCreatedAt();

    /**
     * @return string
     */
    public function getCompanyName();

    /**
     * @param string $companyName
     */
    public function setCompanyName($companyName);

    /**
     * @return string
     */
    public function getLegalForm();

    /**
     * @param string $legalForm
     */
    public function setLegalForm($legalForm);

    /**
     * @return string
     */
    public function getChiefTitle();

    /**
     * @param string $chiefTitle
     */
    public function setChiefTitle($chiefTitle);

    /**
     * @return string
     */
    public function getChiefFirstName();

    /**
     * @param string $chiefFirstName
     */
    public function setChiefFirstName($chiefFirstName);

    /**
     * @return string
     */
    public function getChiefMiddleName();

    /**
     * @param string $chiefMiddleName
     */
    public function setChiefMiddleName($chiefMiddleName);

    /**
     * @return string
     */
    public function getChiefLastName();

    /**
     * @param string $chiefLastName
     */
    public function setChiefLastName($chiefLastName);

    /**
     * @return string
     */
    public function getCommercialRegisterNumber();

    /**
     * @param string $commercialRegisterNumber
     */
    public function setCommercialRegisterNumber($commercialRegisterNumber);

    /**
     * @return string
     */
    public function getTradeId();

    /**
     * @param string $tradeId
     */
    public function setTradeId($tradeId);

    /**
     * @return boolean
     */
    public function getSellsB2B();

    /**
     * @param boolean $sellsB2B
     */
    public function setSellsB2B($sellsB2B);

    /**
     * @return boolean
     */
    public function getSellsB2C();

    /**
     * @param boolean $sellsB2C
     */
    public function setSellsB2C($sellsB2C);
}
