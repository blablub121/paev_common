<?php
namespace Payever\CommonBundle\Interfaces\Model\Merchant;

use Payever\CommonBundle\Interfaces\Reflector\ReflectiveClassInterface;

/**
 * Class MerchantOnlineShopInterface
 *
 * @package Payever\CommonBundle\Interfaces\Model\Merchant
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface MerchantOnlineShopInterface extends ReflectiveClassInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return int
     */
    public function getCreatedAt();

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getLogoFilename();

    /**
     * @param string $logoFilename
     */
    public function setLogoFilename($logoFilename);

    /**
     * @return string
     */
    public function getPhoneNumber();

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber);

    /**
     * @return string
     */
    public function getUrl();

    /**
     * @param string $url
     */
    public function setUrl($url);

    /**
     * @return string
     */
    public function getSector();

    /**
     * @param string $sector
     */
    public function setSector($sector);

    /**
     * @return string
     */
    public function getProducts();

    /**
     * @param string $products
     */
    public function setProducts($products);

    /**
     * @return float
     */
    public function getAvgOrderValue();

    /**
     * @param float $avgOrderValue
     */
    public function setAvgOrderValue($avgOrderValue);

    /**
     * @return float
     */
    public function getAnnualSalesValue();

    /**
     * @param float $annualSalesValue
     */
    public function setAnnualSalesValue($annualSalesValue);

    /**
     * @return boolean
     */
    public function getAcceptsInstallmentPayment();

    /**
     * @param boolean $acceptsInstallmentPayment
     */
    public function setAcceptsInstallmentPayment($acceptsInstallmentPayment);

    /**
     * @return boolean
     */
    public function getAcceptsBillPayment();

    /**
     * @param boolean $acceptsBillPayment
     */
    public function setAcceptsBillPayment($acceptsBillPayment);

    /**
     * @return boolean
     */
    public function getPaysInstallmentPaymentFees();

    /**
     * @param boolean $paysInstallmentPaymentFees
     */
    public function setPaysInstallmentPaymentFees($paysInstallmentPaymentFees);

    /**
     * @return boolean
     */
    public function getPaysBillPaymentFees();

    /**
     * @param boolean $paysBillPaymentFees
     */
    public function setPaysBillPaymentFees($paysBillPaymentFees);
}
