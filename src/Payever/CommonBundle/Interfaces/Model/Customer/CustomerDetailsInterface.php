<?php
namespace Payever\CommonBundle\Interfaces\Model\Customer;

use Payever\CommonBundle\Interfaces\Model\User\UserDetailsInterface;
use Payever\CommonBundle\Interfaces\Reflector\ReflectiveClassInterface;
use Payever\CommonBundle\Interfaces\Model\User\UserAccountInterface;

/**
 * Class CustomerDetailsInterface
 *
 * @package Payever\CommonBundle\Interfaces\Model\Customer
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface CustomerDetailsInterface extends UserDetailsInterface, ReflectiveClassInterface
{
    const MARITAL_STATUS_SINGLE     = 1;
    const MARITAL_STATUS_MARRIED    = 2;
    const MARITAL_STATUS_DIVORCED   = 4;

    /**
     * @return UserAccountInterface
     */
    public function getUserAccount();

    /**
     * @param UserAccountInterface $userAccount
     */
    public function setUserAccount(UserAccountInterface $userAccount);

    /**
     * @return CustomerBankAccountInterface[]
     */
    public function getBankAccounts();

    /**
     * @param CustomerBankAccountInterface $bankAccount
     */
    public function addBankAccount(CustomerBankAccountInterface $bankAccount);

    /**
     * @return CustomerBankAccountInterface
     */
    public function getPrimaryBankAccount();

    /**
     * @param CustomerBankAccountInterface $bankAccount
     */
    public function setPrimaryBankAccount(CustomerBankAccountInterface $bankAccount);

    /**
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * @return string
     */
    public function getSalutation();

    /**
     * @param string $salutation
     */
    public function setSalutation($salutation);

    /**
     * @return string
     */
    public function getFirstName();

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName);

    /**
     * @return string
     */
    public function getMiddleName();

    /**
     * @param string $middleName
     */
    public function setMiddleName($middleName);

    /**
     * @return string
     */
    public function getLastName();

    /**
     * @param string $lastName
     */
    public function setLastName($lastName);

    /**
     * @return \DateTime
     */
    public function getBirthday();

    /**
     * @param \DateTime $birthday
     */
    public function setBirthday($birthday);

    /**
     * @return string
     */
    public function getCountry();

    /**
     * @param string $country
     */
    public function setCountry($country);

    /**
     * @return string
     */
    public function getCity();

    /**
     * @param string $city
     */
    public function setCity($city);

    /**
     * @return string
     */
    public function getZipCode();

    /**
     * @param string $zipCode
     */
    public function setZipCode($zipCode);

    /**
     * @return string
     */
    public function getStreet();

    /**
     * @param string $street
     */
    public function setStreet($street);

    /**
     * @return string
     */
    public function getStreetNumber();

    /**
     * @param string $streetNumber
     */
    public function setStreetNumber($streetNumber);

    /**
     * @return \DateTime
     */
    public function getResidentSince();

    /**
     * @param \DateTime $residentSince
     */
    public function setResidentSince($residentSince);

    /**
     * @return string
     */
    public function getPhoneNumber();

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber);

    /**
     * @return string
     */
    public function getMobilePhoneNumber();

    /**
     * @param string $mobilePhoneNumber
     */
    public function setMobilePhoneNumber($mobilePhoneNumber);

    /**
     * @return string
     */
    public function getBirthPlace();

    /**
     * @param string $birthPlace
     */
    public function setBirthPlace($birthPlace);

    /**
     * @return string
     */
    public function getNationality();

    /**
     * @param string $nationality
     */
    public function setNationality($nationality);

    /**
     * @return int
     */
    public function getMaritalStatus();

    /**
     * @param int $maritalStatus
     */
    public function setMaritalStatus($maritalStatus);

    /**
     * @return int
     */
    public function getNumChildren();

    /**
     * @param int $numChildren
     */
    public function setNumChildren($numChildren);

    /**
     * @return string
     */
    public function getOccupationGroup();

    /**
     * @param string $occupationGroup
     */
    public function setOccupationGroup($occupationGroup);

    /**
     * @return string
     */
    public function getIndustry();

    /**
     * @param string $industry
     */
    public function setIndustry($industry);

    /**
     * @return string
     */
    public function getProfession();

    /**
     * @param string $profession
     */
    public function setProfession($profession);

    /**
     * @return string
     */
    public function getEmployer();

    /**
     * @param string $employer
     */
    public function setEmployer($employer);

    /**
     * @return string
     */
    public function getEmployerPhoneNumber();

    /**
     * @param string $employerPhoneNumber
     */
    public function setEmployerPhoneNumber($employerPhoneNumber);

    /**
     * @return string
     */
    public function getEmployerCountry();

    /**
     * @param string $employerCountry
     */
    public function setEmployerCountry($employerCountry);

    /**
     * @return string
     */
    public function getEmployerCity();

    /**
     * @param string $employerCity
     */
    public function setEmployerCity($employerCity);

    /**
     * @return string
     */
    public function getEmployerZipCode();

    /**
     * @param string $employerZipCode
     */
    public function setEmployerZipCode($employerZipCode);

    /**
     * @return \DateTime
     */
    public function getEmployedSince();

    /**
     * @param \DateTime $employedSince
     */
    public function setEmployedSince($employedSince);

    /**
     * @return boolean
     */
    public function getEmployedTemporary();

    /**
     * @param boolean $employedTemporary
     */
    public function setEmployedTemporary($employedTemporary);

    /**
     * @return float
     */
    public function getMonthlyIncomeNet();

    /**
     * @param float $monthlyIncomeNet
     */
    public function setMonthlyIncomeNet($monthlyIncomeNet);

    /**
     * @return float
     */
    public function getMonthlyPensionNet();

    /**
     * @param float $monthlyPensionNet
     */
    public function setMonthlyPensionNet($monthlyPensionNet);

    /**
     * @return float
     */
    public function getMonthlyExpensesRent();

    /**
     * @param float $monthlyExpensesRent
     */
    public function setMonthlyExpensesRent($monthlyExpensesRent);

    /**
     * @return float
     */
    public function getMonthlyExpensesCar();

    /**
     * @param float $monthlyExpensesCar
     */
    public function setMonthlyExpensesCar($monthlyExpensesCar);

    /**
     * @return float
     */
    public function getMonthlyExpensesMisc();

    /**
     * @param float $monthlyExpensesMisc
     */
    public function setMonthlyExpensesMisc($monthlyExpensesMisc);
}
