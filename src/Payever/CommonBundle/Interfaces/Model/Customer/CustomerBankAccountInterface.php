<?php
namespace Payever\CommonBundle\Interfaces\Model\Customer;

use Payever\CommonBundle\Interfaces\Reflector\ReflectiveClassInterface;

/**
 * Class CustomerBankAccountInterface
 *
 * @package Payever\CommonBundle\Interfaces\Model\Customer
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface CustomerBankAccountInterface extends ReflectiveClassInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return int
     */
    public function getCreatedAt();

    /**
     * @return boolean
     */
    public function getIsPrimary();

    /**
     * @param boolean $isPrimary
     */
    public function setIsPrimary($isPrimary);

    /**
     * @return string
     */
    public function getBankName();

    /**
     * @param string $bankName
     */
    public function setBankName($bankName);

    /**
     * @return string
     */
    public function getBankCode();

    /**
     * @param string $bankCode
     */
    public function setBankCode($bankCode);

    /**
     * @return string
     */
    public function getAccountNumber();

    /**
     * @param string $accountNumber
     */
    public function setAccountNumber($accountNumber);
}
