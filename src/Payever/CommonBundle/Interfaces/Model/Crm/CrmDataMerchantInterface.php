<?php
namespace Payever\CommonBundle\Interfaces\Model\Crm;

use Payever\CommonBundle\Interfaces\Reflector\ReflectiveClassInterface;

/**
 * Class CrmDataMerchantInterface
 *
 * @package Payever\CommonBundle\Interfaces\Model
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface CrmDataMerchantInterface extends ReflectiveClassInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @return int
     */
    public function getCreatedAt();

    /**
     * @return float
     */
    public function getTrustability();

    /**
     * @param float $trustability
     */
    public function setTrustability($trustability);

    /**
     * @return boolean
     */
    public function getPostIdentApproved();

    /**
     * @param boolean $postIdentApproved
     */
    public function setPostIdentApproved($postIdentApproved);

    /**
     * @return boolean
     */
    public function getTradeRegisterExcerptApproved();

    /**
     * @param boolean $tradeRegisterExcerptApproved
     */
    public function setTradeRegisterExcerptApproved($tradeRegisterExcerptApproved);

    /**
     * @return boolean
     */
    public function getButtonUseApproved();

    /**
     * @param boolean $buttonUseApproved
     */
    public function setButtonUseApproved($buttonUseApproved);

    /**
     * @return mixed
     */
    public function getProvides12MonthsInstallments();

    /**
     * @param mixed $provides12MonthsInstallments
     */
    public function setProvides12MonthsInstallments($provides12MonthsInstallments);
}
