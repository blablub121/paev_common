<?php
namespace Payever\CommonBundle\Interfaces\Model\Payment;

/**
 * Class PaymentStatusInterface
 *
 * @package Payever\CommonBundle\Interfaces\Model\Payment
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface PaymentStatusInterface
{
    const STATUS_NEW        = 'STATUS_NEW';
    const STATUS_IN_PROCESS = 'STATUS_IN_PROCESS';
    const STATUS_ACCEPTED   = 'STATUS_ACCEPTED';
    const STATUS_DECLINED   = 'STATUS_DECLINED';
}
