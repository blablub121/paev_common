<?php
namespace Payever\CommonBundle\Interfaces\Model\User;

use Payever\CommonBundle\Interfaces\Reflector\ReflectiveClassInterface;

/**
 * Class UserAccountInterface
 *
 * @package Payever\CommonBundle\Interfaces\Model\User
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface UserAccountInterface extends ReflectiveClassInterface
{
    const TYPE_DEFAULT                  = "default";
    const TYPE_MERCHANT                 = 'merchant';
    const TYPE_CUSTOMER                 = 'customer';

    const STATUS_DISABLED               = 0;
    const STATUS_ACTIVE                 = 1;
    const STATUS_AWAITING_ACTIVATION    = 2;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * @return string
     */
    public function getHash();

    /**
     * @param string $hash
     */
    public function setHash($hash);

    /**
     * Set status
     *
     * @param int $status
     */
    public function setStatus($status);

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus();

    /**
     * Set blocked status
     *
     * @param int $blocked
     */
    public function setBlocked($blocked);

    /**
     * Get blocked
     *
     * @return int
     */
    public function getBlocked();

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type);

    /**
     * Get type
     *
     * @return string
     */
    public function getType();

    /**
     * Set salutation
     *
     * @param string $salutation
     */
    public function setSalutation($salutation);

    /**
     * Get salutation
     *
     * @return string
     */
    public function getSalutation();

    /**
     * Set firstName
     *
     * @param string $firstName
     */
    public function setFirstName($firstName);

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName();

    /**
     * Set middleName
     *
     * @param string $middleName
     */
    public function setMiddleName($middleName);

    /**
     * Get middleName
     *
     * @return string
     */
    public function getMiddleName();

    /**
     * Set lastName
     *
     * @param string $lastName
     */
    public function setLastName($lastName);

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName();

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email);

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail();

    /**
     * Set password salt
     *
     * @param string $passwordSalt
     */
    public function setPasswordSalt($passwordSalt);

    /**
     * Get password salt
     *
     * @return string
     */
    public function getPasswordSalt();

    /**
     * Set password
     *
     * @param string $password
     */
    public function setPassword($password);

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword();

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     */
    public function setBirthday($birthday);

    /**
     * Get birthday
     *
     * @return \DateTime
     */
    public function getBirthday();

    /**
     * @return boolean
     */
    public function getAcceptsTermsAndConditions();

    /**
     * @param boolean $acceptsTermsAndConditions
     */
    public function setAcceptsTermsAndConditions($acceptsTermsAndConditions);

    /**
     * @return boolean
     */
    public function getAcceptsPrivacyPolicy();

    /**
     * @param boolean $acceptsPrivacyPolicy
     */
    public function setAcceptsPrivacyPolicy($acceptsPrivacyPolicy);

    /**
     * @return bool
     */
    public function isActivated();
}
