<?php
namespace Payever\CommonBundle\Interfaces\Model\User;

/**
 * Class UserDetailsInterface
 *
 * @package Payever\CommonBundle\Interfaces\Model\User
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface UserDetailsInterface
{
    /**
     * @return UserAccountInterface
     */
    public function getUserAccount();
}
