<?php
namespace Payever\CommonBundle\Interfaces\Model\User;

use Payever\CommonBundle\Interfaces\Reflector\ReflectiveClassInterface;

/**
 * Class UserAccountActivationTokenInterface
 *
 * @package Payever\CommonBundle\Interfaces\Model\User
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface UserAccountActivationTokenInterface extends ReflectiveClassInterface
{
    /**
     * Set email
     *
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email);

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail();

    /**
     * Set token
     *
     * @param int $token
     * @return $this
     */
    public function setToken($token);

    /**
     * Get token
     *
     * @return int
     */
    public function getToken();
}
