<?php
namespace Payever\CommonBundle\Interfaces\Mapping;

/**
 * Class DataMapperInterface
 *
 * @package Payever\CommonBundle\Interfaces\Mapping
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface DataMapperInterface
{
    /**
     * @param mixed     $source                 The source data (can be anything)
     * @param array     $fieldNames             An array with field names defining which data to take from the source and map to the target
     * @param boolean   $ignoreMissingFields    True to ignore if fields are missing on the target entity
     */
    public function mapToTarget($source, array $fieldNames, $ignoreMissingFields = false);
}
