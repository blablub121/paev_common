<?php
namespace Payever\CommonBundle\Interfaces\Forms\MultiStep;

use Payever\CommonBundle\Impl\Forms\MultiStep\Data\ProcessingResult;
use Payever\CommonBundle\Impl\Forms\MultiStep\MultiStepFormException;
use Payever\CommonBundle\Impl\Forms\MultiStep\Data\MultiStepFormStep;

/**
 * Class ProcessingStrategyInterface
 *
 * @package Payever\CommonBundle\Interfaces\Forms\MultiStep
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface ProcessingStrategyInterface
{
    /**
     * @param string    $formName
     *
     * @return ProcessingResult
     */
    public function processStep($formName);

    /**
     * Clean up, f.e. if there is something stored inside of the session
     */
    public function cleanUp();

    /**
     * @return MultiStepFormStep
     * @throws MultiStepFormException
     */
    public function getNextStep();

    /**
     * @param ProcessingResult  $data
     * @param MultiStepFormStep $step
     *
     * @return boolean
     */
    public function isStepReachable(ProcessingResult $data, MultiStepFormStep $step);
}
