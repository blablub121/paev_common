<?php
namespace Payever\CommonBundle\Interfaces\Awareness;

use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class TranslatorAwareInterface
 *
 * @package Payever\CommonBundle\Interfaces\Awareness
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface TranslatorAwareInterface extends FormTypeInterface
{
    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator);

    /**
     * @return TranslatorInterface
     */
    public function getTranslator();
}
