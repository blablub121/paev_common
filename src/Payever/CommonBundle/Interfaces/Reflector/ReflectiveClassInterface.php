<?php
namespace Payever\CommonBundle\Interfaces\Reflector;

/**
 * Class ReflectiveClassInterface
 *
 * @package Payever\CommonBundle\Interfaces\Serialization
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface ReflectiveClassInterface
{
    /**
     * @return string
     */
    public static function getClass();
}
