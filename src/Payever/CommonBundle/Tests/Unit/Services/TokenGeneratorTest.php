<?php
namespace Payever\CommonBundle\Tests\Unit\Services;

use Payever\CommonBundle\Impl\Services\TokenGenerator;


/**
 * Class TokenGeneratorTest
 *
 * @package Payever\CommonBundle\Tests\Unit\Impl\Services
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class TokenGeneratorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * We generate 10000 Hashes and find that only less than 0.01% collisions is acceptable.
     */
    public function testGenerateAccountActivationToken()
    {
        $knownHashes = array();

        $attempts = 0;

        while (count($knownHashes) < 10000) {
            $generator = new TokenGenerator();

            $newHash = $generator->generateAccountActivationToken();

            $this->assertEquals(2 * 32, strlen($newHash), 'We expect the new string to have a certain length');

            if (!array_key_exists($newHash, $knownHashes)) {
                $knownHashes[$newHash] = $attempts;
            }
            $attempts++;
        }

        $this->assertLessThan(10001, $attempts, 'We expect only 0.01% collisions in generating unique hashed');
    }

    /**
     * We generate 10000 Hashes and find that only less than 0.01% collisions is acceptable.
     */
    public function testGenerateResetPasswordToken()
    {
        $knownHashes = array();

        $attempts = 0;

        while (count($knownHashes) < 10000) {
            $generator = new TokenGenerator();

            $newHash = $generator->generateResetPasswordToken();

            $this->assertEquals(2 * 32, strlen($newHash), 'We expect the new string to have a certain length');

            if (!array_key_exists($newHash, $knownHashes)) {
                $knownHashes[$newHash] = $attempts;
            }
            $attempts++;
        }

        $this->assertLessThan(10001, $attempts, 'We expect only 0.01% collisions in generating unique hashed');
    }
}
