<?php
namespace Payever\CommonBundle\Tests\Unit\Reflection;

use Payever\CommonBundle\Impl\Reflection\Reflector;
use Payever\CommonBundle\Impl\Reflection\ReflectorException;

/**
 * Class ReflectorTest
 *
 * @package Payever\CommonBundle\Tests\Unit\Reflection
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class ReflectorTest extends \PHPUnit_Framework_TestCase
{
    //// TESTS FOR Ctor /////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function testCtorSucceedsWithSomeObject()
    {
        $obj = new \SplObjectStorage();
        $subject = new Reflector($obj);

        $this->assertSame($obj, $subject->getSubject(), 'The subject must be set correctly');
    }

    public function testStaticCreationWorks()
    {
        $obj = new \stdClass();
        $subject = Reflector::create($obj);

        $this->assertEquals($obj, $subject->getSubject(), 'Static creation method must work');
    }

    /**
     * @expectedException \Payever\CommonBundle\Impl\Reflection\ReflectorException
     */
    public function testCtorFailsOnNull()
    {
        $obj = null;
        new Reflector($obj);
    }

    /**
     * @expectedException \Payever\CommonBundle\Impl\Reflection\ReflectorException
     */
    public function testCtorFailsOnNoneObject()
    {
        $obj = array(1,2,3);
        /** @var $obj object fake type to suppress warning */
        new Reflector($obj);    // ignore warning, it is on purpose
    }

    //// TESTS for getValue /////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param string  $propertyName
     * @param string  $expectedValue
     * @param boolean $expectsException
     * @param string  $message
     *
     * @dataProvider testGetValueProvider
     */
    public function testGetValue($propertyName, $expectedValue, $expectsException, $message)
    {
        $reflector = new Reflector(new ReflectorClassWithProperties());

        if ($expectsException) {

            try {
                $reflector->getValue($propertyName);
                $this->fail($message);
            } catch (ReflectorException $e) {}

        } else {
            $value = $reflector->getValue($propertyName);

            $this->assertEquals($expectedValue, $value, $message);
        }
    }

    /**
     * DataProvider for testGetValue
     *
     * @return array
     */
    public static function testGetValueProvider()
    {
        return array(
            // test property access modifiers
            array('publicProperty',     'publicPropertyValue',  false,  'Getting a public property must work'),
            array('protectedProperty',  null,                   true,   'Getting a protected property must not be allowed'),
            array('privateProperty',    null,                   true,   'Getting a private property must not be allowed'),

            // test invalid properties
            array('publicStaticProperty',   null,               true,   'Getting a public static property must not be allowed'),

            // test getters
            array('publicStaticGetter', null,                   true,   'Getting value from a public static getter must not be allowed'),
            array('publicGetter',       'getPublicGetterValue', false,  'Getting value from a public getter must work'),
            array('protectedGetter',    null,                   true,   'Getting value from a protected getter must not be allowed'),
            array('privateGetter',      null,                   true,   'Getting value from a private getter must not be allowed'),

            // test hassers
            array('publicStaticHasser', null,                   true,   'Getting value from a public static hasser must not be allowed'),
            array('publicHasser',       'hasPublicHasserValue', false,  'Getting value from a public hasser must work'),
            array('protectedHasser',    null,                   true,   'Getting value from a protected hasser must not be allowed'),
            array('privateHasser',      null,                   true,   'Getting value from a private hasser must not be allowed'),

            // test issers
            array('publicStaticIsser',  null,                   true,   'Getting value from a public static isser must not be allowed'),
            array('publicIsser',        'isPublicIsserValue',   false,  'Getting value from a public isser must work'),
            array('protectedIsser',     null,                   true,   'Getting value from a protected isser must not be allowed'),
            array('privateIsser',       null,                   true,   'Getting value from a private isser must not be allowed'),
        );
    }

    //// TESTS for setValue /////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param $propertyName
     * @param $expectsException
     * @param $message
     *
     * @dataProvider testSetValueProvider
     */
    public function testSetValue($propertyName, $expectsException, $message)
    {
        $reflector = new Reflector(new ReflectorClassWithProperties());

        if ($expectsException) {

            try {
                $reflector->setValue($propertyName, 'SOME_VALUE');
                $this->fail($message);
            } catch (ReflectorException $e) {}

        } else {
            $reflector->setValue($propertyName, 'SOME_VALUE');
        }
    }

    /**
     * DataProvider for testSetValue
     *
     * @return array
     */
    public static function testSetValueProvider()
    {
        return array(
            // test property access modifiers
            array('publicProperty',     false,  'Setting a public property must work'),
            array('protectedProperty',  true,   'Setting a protected property must not be allowed'),
            array('privateProperty',    true,   'Setting a private property must not be allowed'),

            // test setters
            array('publicStatic',       true,   'Setting value through a public static setter must not be allowed'),
            array('public',             false,  'Setting value through a public setter must work'),
            array('protected',          true,   'Setting value through a protected setter must not be allowed'),
            array('private',            true,   'Setting value through a private setter must not be allowed'),
        );
    }
}

/**
 * Class ReflectorClassWithProperties
 *
 * @package Payever\CommonBundle\Tests\Unit\Reflection
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class ReflectorClassWithProperties
{
    /**
     * The code in the c'tor is just here to suppress warnings
     */
    public function __construct()
    {
        $this->privateProperty = null;
        $this->getPrivateGetter();
        $this->hasPrivateHasser();
        $this->isPrivateIsser();
        $this->setPrivate(null);
    }

    public static   $publicStaticProperty   = null;
    public          $publicProperty         = 'publicPropertyValue';
    protected       $protectedProperty      = null;
    private         $privateProperty        = null;

    public static function getPublicStaticGetter()  {}
    /**  */
    public        function getPublicGetter()        { return 'getPublicGetterValue'; }
    protected     function getProtectedGetter()     {}
    private       function getPrivateGetter()       {}

    public static function hasPublicStaticHasser()  {}
    /**  */
    public        function hasPublicHasser()        { return 'hasPublicHasserValue'; }
    protected     function hasProtectedHasser()     {}
    private       function hasPrivateHasser()       {}

    public static function isPublicStaticIsser()    {}
    /**  */
    public        function isPublicIsser()          { return 'isPublicIsserValue'; }
    protected     function isProtectedIsser()       {}
    private       function isPrivateIsser()         {}

    public static function setPublicSetter()        {}
    /**  */
    public        function setPublic($value)        { $this->publicProperty = $value; }
    /**  */
    protected     function setProtected($value)     {}
    /**  */
    private       function setPrivate($value)       {}
}
