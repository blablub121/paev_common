<?php
namespace Payever\CommonBundle\Tests\Unit\Mapping;

use Payever\CommonBundle\Impl\Mapping\BasicDataMapper;

/**
 * Class BasicDataMapperTest
 *
 * @package Payever\CommonBundle\Tests\Unit\Mapping
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class BasicDataMapperTest extends \PHPUnit_Framework_TestCase
{
    const VAL_PUBLIC = 'public';
    const VAL_PRIVATE = 'private';

    public function testMapToTargetSucceedsWithoutIgnoringMissingFields()
    {
        $source = new BasicDataMapperTestObject();
        $source->publicProperty = self::VAL_PUBLIC;
        $source->setPrivateProperty(self::VAL_PRIVATE);

        $target = new BasicDataMapperTestObject();

        $mapper = new BasicDataMapper($target);
        $mapper->mapToTarget($source, array('publicProperty', 'privateProperty'), false);

        $this->assertEquals(self::VAL_PUBLIC, $target->publicProperty, 'The public property must be set correctly');
        $this->assertEquals(self::VAL_PRIVATE, $target->getPrivateProperty(), 'The private property must be set correctly by using a setter');
    }

    /**
     * @expectedException \Payever\CommonBundle\Impl\Mapping\MappingException
     */
    public function testMapToTargetFailsWithoutIgnoringMissingFields()
    {
        $source = new BasicDataMapperTestObject();
        $target = new \stdClass();

        $mapper = new BasicDataMapper($target);

        $mapper->mapToTarget($source, array('NOT_EXISTING'), false);
    }

    public function testMapToTargetSucceedsOnMappingToStdClass()
    {
        $source = new BasicDataMapperTestObject();
        $source->publicProperty = self::VAL_PUBLIC;
        $source->setPrivateProperty(self::VAL_PRIVATE);

        $target = new \stdClass();

        $mapper = new BasicDataMapper($target);
        $mapper->mapToTarget($source, array('publicProperty', 'privateProperty'), false);

        $this->assertEquals(self::VAL_PUBLIC, $target->publicProperty, 'The public property must be set correctly');
        $this->assertEquals(self::VAL_PRIVATE, $target->privateProperty, 'The private property must be set correctly');
    }

    public function testMapToTargetSucceedsOnMappingFromStdClass()
    {
        $source = new \stdClass();
        $source->publicProperty = self::VAL_PUBLIC;
        $source->privateProperty = self::VAL_PRIVATE;

        $target = new BasicDataMapperTestObject();

        $mapper = new BasicDataMapper($target);
        $mapper->mapToTarget($source, array('publicProperty', 'privateProperty'), false);

        $this->assertEquals(self::VAL_PUBLIC, $target->publicProperty, 'The public property must be set correctly');
        $this->assertEquals(self::VAL_PRIVATE, $target->getPrivateProperty(), 'The private property must be set correctly');
    }
}

/**
 * Class BasicDataMapperTestObject
 *
 * @package Payever\CommonBundle\Tests\Unit\Mapping
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class BasicDataMapperTestObject
{
    /** @var string */
    public $publicProperty;
    /** @var string */
    private $privateProperty;

    /**
     * @return string
     */
    public function getPrivateProperty()
    {
        return $this->privateProperty;
    }

    /**
     * @param string $privateProperty
     */
    public function setPrivateProperty($privateProperty)
    {
        $this->privateProperty = $privateProperty;
    }
}
