<?php
namespace Payever\CommonBundle\Tests\Unit\Forms\MultiStep\Data;

use Payever\CommonBundle\Impl\Forms\MultiStep\Data\MultiStepFormStep;
use Payever\CommonBundle\Impl\Forms\MultiStep\Data\StepCollection;

/**
 * Class StepCollectionTest
 *
 * @package Payever\CommonBundle\Tests\Unit\Forms\MultiStep\Data
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class StepCollectionTest extends \PHPUnit_Framework_TestCase
{
    public function testAddAndAll()
    {
        $step1 = new MultiStepFormStep();
        $step2 = new MultiStepFormStep();

        $subject = new StepCollection();

        // test empty list
        $steps = $subject->all();

        $this->assertTrue(is_array($steps), 'An empty array must be returned');
        $this->assertCount(0, $steps, 'The array with returned steps must be empty');

        // add and test the first step
        $subject->add($step1);
        $steps = $subject->all();

        $this->assertCount(1, $steps, 'The number of items must match');
        $this->assertTrue(in_array($step1, $steps, true), 'Step 1 must be in the array');
        $this->assertFalse(in_array($step2, $steps, true), 'Step 2 must not yet be in the array');

        // add and test the second step
        $subject->add($step2);
        $steps = $subject->all();

        $this->assertCount(2, $steps, 'The number of items must match');
        $this->assertTrue(in_array($step1, $steps, true), 'Step 1 must be in the array');
        $this->assertTrue(in_array($step2, $steps, true), 'Step 2 must be in the array');
    }

    public function testHas()
    {
        $step1 = new MultiStepFormStep();
        $step2 = new MultiStepFormStep();
        $step3 = new MultiStepFormStep();

        $subject = new StepCollection();
        $subject->add($step1);
        $subject->add($step2);
        $subject->add($step3);

        $this->assertFalse($subject->has(new MultiStepFormStep()), 'The step must not be found since it is not in the collection');
        $this->assertTrue($subject->has($step1), 'The step must be found');
        $this->assertTrue($subject->has($step2), 'The step must be found');
        $this->assertTrue($subject->has($step3), 'The step must be found');
    }

    public function testHasWithUri()
    {
        $step1 = new MultiStepFormStep();
        $step1->setUri('uri-1');
        $step2 = new MultiStepFormStep();
        $step2->setUri('uri-2');

        $subject = new StepCollection();

        // test on item that is not yet in the list
        $this->assertFalse($subject->hasWithUri('uri-1'), 'Item must no be found since item is not yet in list');
        // test with step1 added
        $subject->add($step1);
        $this->assertTrue($subject->hasWithUri('uri-1'), 'Item must no be found');
        // test with step2 added
        $subject->add($step2);
        $this->assertTrue($subject->hasWithUri('uri-1'), 'Item must no be found');
        $this->assertTrue($subject->hasWithUri('uri-2'), 'Item must no be found');
        // test some random uri still is not found
        $this->assertFalse($subject->hasWithUri('RANDOM'), 'Item must not be found since it is not present');
    }

    public function testGetWithUri()
    {
        $step1 = new MultiStepFormStep();
        $step1->setUri('uri-1');
        $step2 = new MultiStepFormStep();
        $step2->setUri('uri-2');

        $subject = new StepCollection();

        // test on item that is not yet in the list
        $this->assertNull($subject->getWithUri('uri-1'), 'Item must no be found since item is not yet in list');
        // test with step1 added
        $subject->add($step1);
        $this->assertSame($step1, $subject->getWithUri('uri-1'), 'Item must be found');
        // test with step2 added
        $subject->add($step2);
        $this->assertSame($step1, $subject->getWithUri('uri-1'), 'Item must be found');
        $this->assertSame($step2, $subject->getWithUri('uri-2'), 'Item must be found');
        // test some random uri still is not found
        $this->assertNull($subject->getWithUri('RANDOM'), 'Item must not be found since it is not present');
    }

    public function testCount()
    {
        $step1 = new MultiStepFormStep();
        $step1->setUri('uri-1');
        $step2 = new MultiStepFormStep();
        $step2->setUri('uri-2');

        $subject = new StepCollection();

        // test on item that is not yet in the list
        $this->assertEquals(0, $subject->count(), 'The item count must match');
        // test with step1 added
        $subject->add($step1);
        $this->assertEquals(1, $subject->count(), 'The item count must match');
        // test with step2 added
        $subject->add($step2);
        $this->assertEquals(2, $subject->count(), 'The item count must match');
    }

    public function testGetLast()
    {
        $step1 = new MultiStepFormStep();
        $step1->setUri('uri-1');
        $step2 = new MultiStepFormStep();
        $step2->setUri('uri-2');

        $subject = new StepCollection();

        // test on item that is not yet in the list
        $this->assertNull($subject->getLast(), 'There must not be a last item since the list is empty');
        // test with step1 added
        $subject->add($step1);
        $this->assertSame($step1, $subject->getLast(), 'The last item must match');
        // test with step2 added
        $subject->add($step2);
        $this->assertSame($step2, $subject->getLast(), 'The last item must match');
    }

    public function testGetAllBefore()
    {
        $step1 = new MultiStepFormStep();
        $step2 = new MultiStepFormStep();
        $step3 = new MultiStepFormStep();

        $subject = new StepCollection();
        $subject->add($step1);
        $subject->add($step2);
        $subject->add($step3);

        // test with a step not in the collection
        $stepsBefore = $subject->allBefore(new MultiStepFormStep());
        $this->assertCount(0, $stepsBefore, 'The number of items must be 0 since the steps searched for is not in the collection');

        $stepsBefore = $subject->allBefore($step1);
        $this->assertCount(0, $stepsBefore, 'There must be 0 items before step1');

        $stepsBefore = $subject->allBefore($step2);
        $this->assertCount(1, $stepsBefore, 'There must be 1 items before step2');

        $stepsBefore = $subject->allBefore($step3);
        $this->assertCount(2, $stepsBefore, 'There must be 2 items before step3');
    }
}
