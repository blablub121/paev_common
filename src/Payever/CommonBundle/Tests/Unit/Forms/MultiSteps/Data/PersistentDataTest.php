<?php
namespace Payever\CommonBundle\Tests\Unit\Forms\MultiStep\Data;

use Payever\CommonBundle\Impl\Forms\MultiStep\Data\PersistentData;

/**
 * Class PersistentDataTest
 *
 * @package Payever\CommonBundle\Tests\Unit\Forms\MultiStep\Data
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class PersistentDataTest extends \PHPUnit_Framework_TestCase
{
    public function testGetStepData()
    {
        $subject = new PersistentData();

        $subject->setData(array(
            'step-1' => 'step-1-data',
            'step-2' => 'step-2-data'
        ));

        $this->assertNull($subject->getStepData('RANDOM'), 'There is no data with this key');
        $this->assertEquals('step-1-data', $subject->getStepData('step-1'), 'The data must be returned correctly');
        $this->assertEquals('step-2-data', $subject->getStepData('step-2'), 'The data must be returned correctly');
    }

    public function testMarkStepValid()
    {
        $subject = new PersistentData();

        // test empty list of valid uri
        $this->assertCount(0, $subject->getValidStepUris(), 'There must be no valid uris');

        // test adding a valid uri
        $subject->markStepValid('first');
        $validSteps = $subject->getValidStepUris();
        $this->assertCount(1, $validSteps, 'There must be 1 valid uri');
        $this->assertTrue(in_array('first', $validSteps, true), 'The step must be in the valid uris');

        // test adding another valid uri
        $subject->markStepValid('second');
        $validSteps = $subject->getValidStepUris();
        $this->assertCount(2, $validSteps, 'There must be 2 valid uri');
        $this->assertTrue(in_array('first', $validSteps, true), 'The step must be in the valid uris');
        $this->assertTrue(in_array('second', $validSteps, true), 'The step must be in the valid uris');

        // test removing a valid step
        $subject->markStepValid('first', false);
        $validSteps = $subject->getValidStepUris();
        $this->assertCount(1, $validSteps, 'There must be 1 valid uri');
        $this->assertFalse(in_array('first', $validSteps, true), 'The step must no longer be in the valid uris');
        $this->assertTrue(in_array('second', $validSteps, true), 'The step must be in the valid uris');

        // test removing the other valid step as well
        $subject->markStepValid('second', false);
        $validSteps = $subject->getValidStepUris();
        $this->assertCount(0, $validSteps, 'There must be no valid uri');
        $this->assertFalse(in_array('first', $validSteps, true), 'The step must no longer be in the valid uris');
        $this->assertFalse(in_array('second', $validSteps, true), 'The step must no longer be in the valid uris');
    }
}
