<?php
namespace Payever\CommonBundle\Tests\Unit\Forms;

use Payever\CommonBundle\Impl\Forms\FormHelper;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormView;

/**
 * Class FormHelperTest
 *
 * @package Payever\CommonBundle\Tests\Unit\Forms
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class FormHelperTest extends \PHPUnit_Framework_TestCase
{
    public function testHasErrorsWithNoErrors()
    {
        $mainView = new FormView();

        $mainView->children['child1'] = new FormView();
        $mainView->children['child2'] = new FormView();

        $mainView['child1']->children['sub1'] = new FormView();

        $helper = new FormHelper();
        $hasErrors = $helper->hasErrors($mainView);

        $this->assertFalse($hasErrors, 'There not be any errors');
    }

    public function  testHasErrorWithErrorInMainView()
    {
        $mainView = new FormView();
        $mainView->vars['errors'][] = new FormError('Some error');

        $mainView->children['child1'] = new FormView();
        $mainView->children['child2'] = new FormView();

        $mainView['child1']->children['sub1'] = new FormView();

        $helper = new FormHelper();
        $hasErrors = $helper->hasErrors($mainView);

        $this->assertTrue($hasErrors, 'There not be any errors');
    }

    public function testHasErrorsWithErrorInSubView()
    {
        $mainView = new FormView();

        $mainView->children['child1'] = new FormView();
        $mainView->children['child2'] = new FormView();

        $mainView['child1']->children['sub1'] = new FormView();
        $mainView['child1']->children['sub1']->vars['errors'][] = new FormError('Some error');

        $helper = new FormHelper();
        $hasErrors = $helper->hasErrors($mainView);

        $this->assertTrue($hasErrors, 'There not be any errors');
    }
}
