<?php
namespace Payever\CommonBundle\Tests\Unit\Forms\Constraints;

use Payever\CommonBundle\Impl\Forms\Constraints\Email;
use Payever\CommonBundle\Impl\Forms\Constraints\EmailValidator;

/**
 * Class EmailValidatorTest
 *
 * @package Payever\CommonBundle\Tests\Unit\MultiStep\Data
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class EmailValidatorTest extends \PHPUnit_Framework_TestCase
{
    public function testSlashInEmailFailsValidation()
    {
        $emailConstraint = new Email();

        /** @var \PHPUnit_Framework_MockObject_MockObject $contextMock */
        $contextMock = $this->getMockBuilder('\Symfony\Component\Validator\ExecutionContextInterface')
            ->disableOriginalConstructor()
            ->setMethods(array('addViolation'))
            ->getMockForAbstractClass();

        $contextMock->expects($this->once())
            ->method('addViolation')
            ->with($this->equalTo($emailConstraint->message));

        /** @var \Symfony\Component\Validator\ExecutionContextInterface $contextMock */
        $validator = new EmailValidator();
        $validator->initialize($contextMock);
        $validator->validate("slash/in@email.de", $emailConstraint);
    }

    public function testValidationSucceeds()
    {
        $emailConstraint = new Email();

        /** @var \PHPUnit_Framework_MockObject_MockObject $contextMock */
        $contextMock = $this->getMockBuilder('\Symfony\Component\Validator\ExecutionContextInterface')
            ->disableOriginalConstructor()
            ->setMethods(array('addViolation'))
            ->getMockForAbstractClass();

        $contextMock->expects($this->never())
            ->method('addViolation');

        /** @var \Symfony\Component\Validator\ExecutionContextInterface $contextMock */
        $validator = new EmailValidator();
        $validator->initialize($contextMock);
        $validator->validate("no-slash-in@email.de", $emailConstraint);
    }
}
