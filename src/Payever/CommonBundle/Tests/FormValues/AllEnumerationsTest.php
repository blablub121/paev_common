<?php
namespace Payever\CommenBundle\Tests\FormValues;

use Payever\CommonBundle\Impl\FormValues\Branch;
use Payever\CommonBundle\Impl\FormValues\CreditDueDate;
use Payever\CommonBundle\Impl\FormValues\CreditDuration;
use Payever\CommonBundle\Impl\FormValues\CreditReason;
use Payever\CommonBundle\Impl\FormValues\EmploymentContractType;
use Payever\CommonBundle\Impl\FormValues\EmploymentType;
use Payever\CommonBundle\Impl\FormValues\EnumerableConstants;
use Payever\CommonBundle\Impl\FormValues\MaritalStatus;
use Payever\CommonBundle\Impl\FormValues\PaymentMethod;
use Payever\CommonBundle\Impl\FormValues\ResidencePermit;
use Payever\CommonBundle\Impl\FormValues\ResidenceTerm;
use Payever\CommonBundle\Impl\FormValues\Salutation;
use Payever\CommonBundle\Impl\FormValues\WorkPermit;

/**
 * Class AllEnumerationsTest
 *
 * @package Payever\CommenBundle\Tests\FormValues
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class AllEnumerationsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @param EnumerableConstants $enum
     *
     * @dataProvider testEnumerationProvider
     */
    public function testEnumeration(EnumerableConstants $enum)
    {
        $ref = new \ReflectionClass($enum);

        $constants = $ref->getConstants();

        // calculate the translation prefix by camel case splitting the class name
        $ref = new \ReflectionClass($enum);
        $className = $ref->getShortName();
        $pass1 = preg_replace("/([a-z])([A-Z])/","\\1_\\2", $className);
        $prefix = preg_replace("/([A-Z])([A-Z][a-z])/","\\1_\\2",$pass1);
        $prefix = strtoupper($prefix) . '_';


        foreach ($constants as $name => $value) {
            $this->assertEquals($name, $value, 'The name and the value of the enumeration constant must match');

            $this->assertStringStartsWith($prefix, $name, 'All values for enum ' . $ref->getShortName() . ' must start with ' . $prefix);
        }
    }

    /**
     * @return array
     */
    public static function testEnumerationProvider()
    {
        return array(
            array(Branch::instance()),
            array(CreditDueDate::instance()),
            array(CreditDuration::instance()),
            array(CreditReason::instance()),
            array(EmploymentContractType::instance()),
            array(EmploymentType::instance()),
            array(MaritalStatus::instance()),
            array(PaymentMethod::instance()),
            array(ResidencePermit::instance()),
            array(ResidenceTerm::instance()),
            array(Salutation::instance()),
            array(WorkPermit::instance()),
        );
    }
}
