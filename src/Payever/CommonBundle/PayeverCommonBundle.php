<?php
namespace Payever\CommonBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class PayeverCommonBundle
 *
 * @package Payever\CommonBundle
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class PayeverCommonBundle extends Bundle
{
}
