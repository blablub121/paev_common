SET foreign_key_checks = 0;

DELETE FROM `user_account`;
DELETE FROM `user_account_activation_token`;
DELETE FROM `user_account_forgot_password_token`;

DELETE FROM `crm_data_merchant`;
DELETE FROM `merchant_address`;
DELETE FROM `merchant_bank_account`;
DELETE FROM `merchant_details`;
DELETE FROM `merchant_online_shop`;

SET foreign_key_checks = 1;