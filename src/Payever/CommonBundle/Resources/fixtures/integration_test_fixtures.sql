SET foreign_key_checks = 0;

DELETE FROM `user_account`;
DELETE FROM `user_account_activation_token`;
DELETE FROM `user_account_forgot_password_token`;

DELETE FROM `crm_data_merchant`;

DELETE FROM `merchant_address`;
DELETE FROM `merchant_bank_account`;
DELETE FROM `merchant_details`;
DELETE FROM `merchant_online_shop`;

INSERT INTO `user_account`
  (`id`, `salutation`, `first_name`, `middle_name`, `last_name`, `email`,               `password`,                                                 `birthday`, `type`,     `status`, `blocked`, `password_salt`,           `accepts_terms_and_conditions`, `accepts_privacy_policy`, `hash`)
  VALUES
-- password: aaabbbccc
  (1,   NULL,         NULL,          NULL,          NULL,       'merchant_01@bbb.ccc',  'CHRykwndXZkmbUQr+RLGTODI2O0zu5BUnPtQaTlcWLhwOknLgynO1g==', NULL,       'merchant', 1,        0,        '52335cc880e922.56745133',  0,                              0,                        'merch_01')
-- password: aaabbbccc
 ,(2,   NULL,         NULL,          NULL,          NULL,       'merchant_02@bbb.ccc',  'CHRykwndXZkmbUQr+RLGTODI2O0zu5BUnPtQaTlcWLhwOknLgynO1g==', NULL,       'merchant', 1,        0,        '52335cc880e922.56745133',  0,                              0,                        'merch_02')
-- password: aaabbbccc
 ,(3,   NULL,         NULL,          NULL,          NULL,       'merchant_03@bbb.ccc',  'CHRykwndXZkmbUQr+RLGTODI2O0zu5BUnPtQaTlcWLhwOknLgynO1g==', NULL,       'merchant', 1,        0,        '52335cc880e922.56745133',  1,                              1,                        'merch_03')
-- blocked user - password: aaabbbccc
 ,(4,   NULL,         NULL,          NULL,          NULL,       'blocked@payever.de',   'CHRykwndXZkmbUQr+RLGTODI2O0zu5BUnPtQaTlcWLhwOknLgynO1g==', NULL,       'merchant', 1,        1,        '52335cc880e922.56745133',  0,                              0,                        'blocked_01')
-- password: aaabbbccc
 ,(5,   NULL,         NULL,          NULL,          NULL,       'cust@payever.de',      'CHRykwndXZkmbUQr+RLGTODI2O0zu5BUnPtQaTlcWLhwOknLgynO1g==', NULL,       'customer', 1,        0,        '52335cc880e922.56745133',  0,                              0,                        'cust_01')
;

-- Merchant Details Object for user with id = 3
INSERT INTO `merchant_details`
  (`id`,  `user_account_id`,  `created_at`,           `company_name`, `legal_form`, `chief_title`,  `chief_first_name`, `chief_middle_name`,  `chief_last_name`, `commercial_register_number`, `trade_id`,  `sells_b2b`,  `sells_b2c`)
  VALUES
  (1,     1,                  '2013-10-26 21:08:33',  '',             '',           '',             '',                 '',                   '',               '',                           '',           0,            0)
 ,(2,     2,                  '2013-10-26 21:08:33',  '',             '',           '',             '',                 '',                   '',               '',                           '',           0,            0)
 ,(3,     3,                  '2013-10-26 21:08:33',  '',             '',           '',             '',                 '',                   '',               '',                           '',           0,            0)
 ,(4,     4,                  '2013-10-26 21:08:33',  '',             '',           '',             '',                 '',                   '',               '',                           '',           0,            0)

;

-- OnlineShop for user with with id = 3 and merchant details with id = 3
INSERT INTO `merchant_online_shop`
  (`id`,  `merchant_details_id`, `created_at`,           `logo_filename`,  `phone_number`, `url`,                `sector`,  `products`, `avg_order_value`, `annual_sales_value`, `accepts_installment_payment`,  `accepts_bill_payment`, `name`,      `pays_installment_payment_fees`,  `pays_bill_payment_fees`)
  VALUES
  (1,     1,                    '2013-10-26 21:08:33',  '',               '',             'http://merchant_01',  '',         '',         0,                 0,                    1,                              0,                     'Merch_01',  0,                                0)
 ,(2,     2,                    '2013-10-26 21:08:33',  '',               '',             'http://merchant_02',  '',         '',         0,                 0,                    0,                              1,                     'Merch_02',  0,                                0)
 ,(3,     3,                    '2013-10-26 21:08:33',  '',               '',             'http://merchant_03',  '',         '',         0,                 0,                    1,                              1,                     'Merch_03',  0,                                0)
 ,(4,     4,                    '2013-10-26 21:08:33',  '',               '',             'http://blocked',      '',         '',         0,                 0,                    0,                              0,                     'Blocked',   0,                                0)
;

INSERT INTO `merchant_bank_account`
  (`id`, `merchant_details_id`,  `created_at`,           `is_primary`,   `bank_name`,  `bank_code`,  `account_number`,   `iban`,    `owner`,    `bic`)
  VALUES
  (1,   1,                      '2013-12-14 14:18:19',  1,              'M1 Bank',    '87654321',   '12345678',         'M1 IBAN', 'M1 OWNER',  'M1 BIC'),
  (2,   2,                      '2013-12-14 14:18:29',  1,              'M2 Bank',    '87654321',   '12345678',         'M2 IBAN', 'M2 OWNER',  'M2 BIC'),
  (3,   3,                      '2013-12-14 14:39:23',  1,              'M3 Bank',    '87654321',   '12345678',         'M3 IBAN', 'M3 OWNER',  'M3 BIC'),
  (4,   4,                      '2013-12-14 14:42:41',  1,              '',           '',           '',                 '',       '',       '')
;

INSERT INTO `crm_data_merchant`
  (`id`,  `merchant_details_id`,  `created_at`,           `trustability`, `post_ident_approved`,  `trade_register_excerpt_approved`, `button_use_approved`)
  VALUES
  (1,     1,                      '2013-12-14 14:18:19',  0.50,           0,                      0,                                  1),
  (2,     2,                      '2013-12-14 14:18:29',  0.50,           0,                      0,                                  1),
  (3,     3,                      '2013-12-14 14:39:23',  0.50,           0,                      0,                                  1),
  (4,     4,                      '2013-12-14 14:42:41',  0.50,           0,                      0,                                  0)
;

SET foreign_key_checks = 1;
