<?php
namespace Payever\CommonBundle\Impl\Repository;

use Doctrine\ORM\EntityRepository;
use Payever\CommonBundle\Entity\UniqueIdentifier;

/**
 * Class UniqueIdentifierRepository
 *
 * @package Payever\CommonBundle\Impl\Repository
 */
class UniqueIdentifierRepository extends EntityRepository
{
    /**
     * @param string    $hash
     *
     * @return UniqueIdentifier|null
     */
    public function findOneByHash($hash)
    {
        return $this->findOneBy(array('hash' => $hash));
    }
}
