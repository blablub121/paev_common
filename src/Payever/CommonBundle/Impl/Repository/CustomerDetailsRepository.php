<?php
namespace Payever\CommonBundle\Impl\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr as Expr;
use Payever\CommonBundle\Entity\UserAccount;
use Payever\CommonBundle\Interfaces\Model\User\UserAccountInterface;
use Payever\CommonBundle\Interfaces\Model\Customer\CustomerDetailsInterface;
use Payever\CommonBundle\Entity\CustomerBankAccount;
use Payever\CommonBundle\Entity\CustomerDetails;

/**
 * Class CustomerDetailsRepository
 *
 * @package Payever\CommonBundle\Impl\Repository
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class CustomerDetailsRepository extends EntityRepository
{
    /**
     * @param $email
     * @return CustomerDetails|null
     */
    public function findOneByEmail($email)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->innerJoin(UserAccount::getClass(), 'u', Expr\Join::WITH, 'm.userAccount = u.id')
            ->andWhere('u.email = ?0')
            ->setMaxResults(1)
        ;

        $query = $qb->getQuery();
        $results = $query->execute(array($email));

        if (count($results) > 0) {
            return $results[0];
        }

        return null;
    }

    /**
     * @param $userId
     * @return CustomerDetails|null
     */
    public function findOneByUserId($userId)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->innerJoin(UserAccount::getClass(), 'u', Expr\Join::WITH, 'm.userAccount = u.id')
            ->andWhere('u.id = ?0')
            ->setMaxResults(1)
        ;

        $query = $qb->getQuery();
        $results = $query->execute(array($userId));

        if (count($results) > 0) {
            return $results[0];
        }

        return null;
    }


    /**
     * @param UserAccountInterface $userAccount
     * @return CustomerDetails
     */
    public function getOrCreateForUser(UserAccountInterface $userAccount)
    {
        /** @var CustomerDetails $customerDetails */
        $customerDetails = $this->findOneBy(array('userAccount' => $userAccount));

        $customerDetails = $this->createMinimalData($customerDetails, $userAccount);

        $this->getEntityManager()->persist($customerDetails);
        $this->getEntityManager()->flush($customerDetails);

        return $customerDetails;
    }

    /**
     * @param CustomerDetailsInterface  $customerDetails
     * @param UserAccountInterface      $userAccount
     *
     * @return CustomerDetails
     */
    private function createMinimalData($customerDetails, $userAccount)
    {
        if ($customerDetails === null) {
            $customerDetails = new CustomerDetails();
            $customerDetails->setUserAccount($userAccount);
        }

        if (count($customerDetails->getBankAccounts()) == 0) {
            $bankAccount = new CustomerBankAccount();
            $bankAccount->setIsPrimary(true);
            $bankAccount->setCustomerDetails($customerDetails);
            $customerDetails->addBankAccount($bankAccount);
        }

        return $customerDetails;
    }
}
