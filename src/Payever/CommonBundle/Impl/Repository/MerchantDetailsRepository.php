<?php
namespace Payever\CommonBundle\Impl\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr as Expr;
use Payever\CommonBundle\Entity\UserAccount;
use Payever\CommonBundle\Interfaces\Model\User\UserAccountInterface;
use Payever\CommonBundle\Entity\CrmDataMerchant;
use Payever\CommonBundle\Entity\MerchantAddress;
use Payever\CommonBundle\Entity\MerchantBankAccount;
use Payever\CommonBundle\Entity\MerchantDetails;
use Payever\CommonBundle\Entity\MerchantOnlineShop;

/**
 * Class MerchantDetailsRepository
 *
 * @package Payever\CommonBundle\Impl\Repository
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class MerchantDetailsRepository extends EntityRepository
{
    /**
     * @param $email
     * @return MerchantDetails|null
     */
    public function findOneByEmail($email)
    {
        $qb = $this->createQueryBuilder('m');
        $qb->innerJoin(UserAccount::getClass(), 'u', Expr\Join::WITH, 'm.userAccount = u.id')
            ->andWhere('u.email = ?0')
            ->setMaxResults(1)
        ;

        $query = $qb->getQuery();
        $results = $query->execute(array($email));

        if (count($results) > 0) {
            return $results[0];
        }

        return null;
    }

    /**
     * @param $name
     *
     * @return MerchantDetails[]
     */
    public function findMerchantsWithShop($name)
    {
        $searchTerm = "%$name%";

        $qb = $this->createQueryBuilder('m');
        $qb->join('m.onlineShops', 's')
            ->andWhere('s.name like :searchTerm')
            ->orWhere('s.url like :searchTerm')
            ;

        $qb->orderBy('s.name');

        $query = $qb->getQuery();

        return $query->execute(
            array(
                'searchTerm' => $searchTerm
            )
        );
    }


    /**
     * @param UserAccountInterface $userAccount
     * @return MerchantDetails
     */
    public function getOrCreateForUser(UserAccountInterface $userAccount)
    {
        /** @var MerchantDetails $merchantDetails */
        $merchantDetails = $this->findOneBy(array('userAccount' => $userAccount));

        if ($merchantDetails === null) {
            $merchantDetails = new MerchantDetails();
            $merchantDetails->setUserAccount($userAccount);
        }

        $merchantDetails = $this->createMinimalData($merchantDetails);

        $this->getEntityManager()->persist($merchantDetails);
        $this->getEntityManager()->flush($merchantDetails);

        return $merchantDetails;
    }

    /**
     * @param int   $id
     *
     * @return MerchantDetails
     */
    public function getOrCreateForId($id)
    {
        /** @var MerchantDetails $merchantDetails */
        $merchantDetails = $this->findOneBy(array('id' => $id));

        $merchantDetails = $this->createMinimalData($merchantDetails);

        $this->getEntityManager()->persist($merchantDetails);
        $this->getEntityManager()->flush($merchantDetails);

        return $merchantDetails;
    }

    /**
     * @param MerchantDetails   $merchantDetails
     *
     * @return MerchantDetails
     */
    private function createMinimalData($merchantDetails)
    {
        if ($merchantDetails->getCrmData() === null) {
            $crmData = new CrmDataMerchant();
            $crmData->setMerchantDetails($merchantDetails);
            $crmData->setMerchantDetails($merchantDetails);
            $merchantDetails->setCrmData($crmData);
        }

        if (count($merchantDetails->getAddresses()) == 0) {
            $address = new MerchantAddress();
            $address->setIsPrimary(true);
            $address->setMerchantDetails($merchantDetails);
            $merchantDetails->addAddress($address);
        }

        if (count($merchantDetails->getBankAccounts()) == 0) {
            $bankAccount = new MerchantBankAccount();
            $bankAccount->setIsPrimary(true);
            $bankAccount->setMerchantDetails($merchantDetails);
            $merchantDetails->addBankAccount($bankAccount);
        }

        if (count($merchantDetails->getOnlineShops()) == 0) {
            $onlineShop = new MerchantOnlineShop();
            $onlineShop->setMerchantDetails($merchantDetails);
            $merchantDetails->addOnlineShop($onlineShop);
        }

        return $merchantDetails;
    }
}
