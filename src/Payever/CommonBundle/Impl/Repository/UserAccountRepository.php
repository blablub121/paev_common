<?php
namespace Payever\CommonBundle\Impl\Repository;

use Doctrine\ORM;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Payever\CommonBundle\Entity\UserAccount;

/**
 * Class UserAccountRepository
 *
 * @package Payever\CommonBundle\Impl\Repository
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class UserAccountRepository extends EntityRepository
{
    /**
     * @param string    $email
     *
     * @return UserAccount|null
     */
    public function findOneByEmail($email)
    {
        return $this->findOneBy(array('email' => $email));
    }

    /**
     * @param string    $hash
     *
     * @return UserAccount|null
     */
    public function findOneByHash($hash)
    {
        return $this->findOneBy(array('hash' => $hash));
    }
}
