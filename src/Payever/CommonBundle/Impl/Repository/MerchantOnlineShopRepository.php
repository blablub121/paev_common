<?php
namespace Payever\CommonBundle\Impl\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr as Expr;
use Payever\CommonBundle\Entity\MerchantDetails;
use Payever\CommonBundle\Entity\MerchantOnlineShop;

/**
 * Class MerchantOnlineShopRepository
 *
 * @package Payever\CommonBundle\Impl\Repository
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class MerchantOnlineShopRepository extends EntityRepository
{
    /**
     * @param $id
     * @param MerchantDetails $merchant
     *
     * @return MerchantOnlineShop
     */
    public function findOneByIdAndMerchant($id, MerchantDetails $merchant)
    {
        return $this->findOneBy(array('id' => $id, 'merchantDetails' => $merchant));
    }
}
