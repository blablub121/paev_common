<?php
namespace Payever\CommonBundle\Impl\Repository;

use Doctrine\ORM\EntityRepository;
use Payever\CommonBundle\Entity\MerchantOnlineShop;
use Payever\CommonBundle\Entity\Payment\BasePayment;
use Payever\CommonBundle\Entity\UserAccount;
use Payever\CommonBundle\Interfaces\Model\Merchant\MerchantOnlineShopInterface;
use Payever\CommonBundle\Interfaces\Model\User\UserAccountInterface;

/**
 * Class PaymentRepository
 *
 * @package Payever\CommonBundle\Impl\Repository
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class PaymentRepository extends EntityRepository
{
    /**
     * @param $status
     *
     * @return BasePayment[]
     */
    public function findAllByStatus($status)
    {
        return $this->findBy(array('status' => $status));
    }

    /**
     * @param UserAccountInterface $userAccount
     *
     * @return BasePayment[]
     */
    public function findByUserAccount(UserAccountInterface $userAccount)
    {
        $qb = $this->createQueryBuilder('b')
            ->join('b.customerDetails', 'c')
            ->where('c.userAccount = :userAccount')
        ;

        $query = $qb->getQuery();

        $results = $query->execute(array('userAccount' => $userAccount));

        return $results;
    }

    /**
     * @param MerchantOnlineShopInterface $onlineShop
     *
     * @return BasePayment[]
     */
    public function findByOnlineShop(MerchantOnlineShopInterface $onlineShop)
    {
        return $this->findBy(array('onlineShop' => $onlineShop));
    }
}
