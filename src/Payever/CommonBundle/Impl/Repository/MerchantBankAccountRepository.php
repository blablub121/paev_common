<?php
namespace Payever\CommonBundle\Impl\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr as Expr;
use Payever\CommonBundle\Entity\MerchantBankAccount;
use Payever\CommonBundle\Entity\MerchantDetails;

/**
 * Class MerchantBankAccountRepository
 *
 * @package Payever\CommonBundle\Impl\Repository
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class MerchantBankAccountRepository extends EntityRepository
{
    /**
     * @param $id
     * @param MerchantDetails $merchant
     *
     * @return MerchantBankAccount
     */
    public function findOneByIdAndMerchant($id, MerchantDetails $merchant)
    {
        return $this->findOneBy(array('id' => $id, 'merchantDetails' => $merchant));
    }
}
