<?php
namespace Payever\CommonBundle\Impl\Repository;

use Doctrine\ORM;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Payever\CommonBundle\Entity\UserAccountPasswordToken;

/**
 * Class UserAccountPasswordTokenRepository
 *
 * @package Payever\CommonBundle\Impl\Repository
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class UserAccountPasswordTokenRepository extends EntityRepository
{
    /**
     * @param string    $token
     *
     * @return UserAccountPasswordToken|null
     */
    public function findOne($token)
    {
        return $this->findOneBy(
            array('token' => $token)
        );
    }

    /**
     * @param string    $email
     *
     * @return UserAccountPasswordToken[]
     */
    public function findByEmail($email)
    {
        return $this->findBy(
            array('email' => $email)
        );
    }
}
