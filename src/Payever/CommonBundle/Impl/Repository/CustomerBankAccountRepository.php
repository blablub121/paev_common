<?php
namespace Payever\CommonBundle\Impl\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr as Expr;
use Payever\CommonBundle\Entity\CrmDataMerchant;
use Payever\CommonBundle\Entity\CustomerDetails;
use Payever\CommonBundle\Entity\MerchantBankAccount;

/**
 * Class CustomerBankAccountRepository
 *
 * @package Payever\CommonBundle\Impl\Repository
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class CustomerBankAccountRepository extends EntityRepository
{
    /**
     * @param $id
     * @param CustomerDetails $customer
     *
     * @return MerchantBankAccount
     */
    public function findOneByIdAndCustomer($id, CustomerDetails $customer)
    {
        return $this->findOneBy(array('id' => $id, 'customerDetails' => $customer));
    }
}
