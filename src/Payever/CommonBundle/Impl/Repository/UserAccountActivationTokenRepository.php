<?php
namespace Payever\CommonBundle\Impl\Repository;

use Doctrine\ORM;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr;
use Payever\CommonBundle\Entity\UserAccountActivationToken;

/**
 * Class UserAccountActivationTokenRepository
 *
 * @package Payever\CommonBundle\Impl\Repository
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class UserAccountActivationTokenRepository extends EntityRepository
{
    /**
     * @param string $token
     *
     * @return UserAccountActivationToken
     */
    public function findOne($token)
    {
        return $this->findOneBy(
            array('token' => $token)
        );
    }

    /**
     * @param $email
     *
     * @return UserAccountActivationToken[]
     */
    public function findByEmail($email)
    {
        return $this->findBy(
            array('email' => $email)
        );
    }
}
