<?php
namespace Payever\CommonBundle\Impl\Serialization;

/**
 * Class ModelFactory
 *
 * @package Payever\CommonBundle\Impl\Serialization
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class ModelFactory
{
    /** @var array */
    private $typeMappings;

    /**
     * @param array     $typeMappings
     */
    public function __construct(array $typeMappings)
    {
        $this->typeMappings = $typeMappings;
    }

    /**
     * @param string    $input
     * @return mixed
     *
     * @throws \InvalidArgumentException
     */
    public function buildModel(&$input)
    {
        if ($input instanceof \stdClass && isset($input->__type)) {
            return $this->unserializeType($input);
        } elseif (is_array($input)) {
            return $this->unserializeArray($input);
        } elseif ($input instanceof \stdClass) {
            return $this->unserializeStdClass($input);
        }

        return $input;
    }

    /**
     * @param $input
     *
     * @return mixed
     * @throws \InvalidArgumentException
     */
    protected function unserializeType(&$input)
    {
        $type = $input->__type;
        if (! isset($this->typeMappings[$type])) {
            throw new \InvalidArgumentException("No mapping defined for type '$type'!");
        }

        $cls = $this->typeMappings[$type];
        $target = new $cls();
        $reflectionClass = new \ReflectionClass($target);

        foreach ($input as $property => &$value) {

            $unSerializedValue = $this->buildModel($value);

            $setter = 'set' . ucfirst($property);

            if (method_exists($target, $setter)) {
                $target->$setter($unSerializedValue);
            } elseif (property_exists($target, $property)) {

                $property = $reflectionClass->getProperty($property);

                if (!$property->isPublic()) {
                    $property->setAccessible(true);
                }

                $property->setValue($target, $value);

                if (!$property->isPublic()) {
                    $property->setAccessible(false);
                }
            }
        }

        return $target;
    }

    /**
     * @param \stdClass $input
     *
     * @return \stdClass
     */
    protected function unserializeStdClass(&$input)
    {
        $target = new \stdClass();

        foreach ($input as $property => &$value) {

            $unSerializedValue = $this->buildModel($value);
            $target->$property = $unSerializedValue;
        }

        return $target;
    }

    /**
     * @param array $input
     *
     * @return array
     */
    protected function unserializeArray(&$input)
    {
        $ret = array();

        foreach ($input as $k => $v) {
            $ret[$k] = $this->buildModel($v);
        }

        return $ret;
    }
}
