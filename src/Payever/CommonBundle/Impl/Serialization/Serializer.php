<?php
namespace Payever\CommonBundle\Impl\Serialization;

/**
 * Class Serializer
 *
 * @package Payever\CommonBundle\Impl\Serialization
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class Serializer
{
    /** @var \SplObjectStorage */
    private $knownObjects;

    /** @var \ArrayIterator */
    private $privatePropertiesIt;

    /**
     * C'tor
     */
    public function __construct()
    {
        $this->knownObjects = new \SplObjectStorage();
    }

    /**
     * @param $source
     *
     * @return array|int|object|\stdClass|string
     */
    public function serialize($source)
    {
        if (is_object($source)) {
            if ($this->knownObjects->contains($source)) {
                return 'RECURSION';
            }
            $this->knownObjects->offsetSet($source);
        }

        if ($source instanceof AbstractSerializableObject) {
            return $this->serializeEntity($source);
        } elseif (is_array($source) || $source instanceof \Traversable) {
            return $this->serializeTraversable($source);
        } elseif ($source instanceof \DateTime) {
            return $source->getTimestamp();
        } else {
            return $source;
        }
    }

    /**
     * @param AbstractSerializableObject $entity
     *
     * @return \stdClass
     */
    private function serializeEntity(AbstractSerializableObject $entity)
    {
        $privatePropertiesIt = $this->getPrivatePropertiesIt($entity);

        $ret = new \stdClass();

        for ($privatePropertiesIt->rewind(); $privatePropertiesIt->valid(); $privatePropertiesIt->next()) {

            $propertyName = $privatePropertiesIt->current()->name;
            $getterName = 'get' . ucfirst($propertyName);
            $value = $entity->$getterName();

            $ret->{$propertyName} = $this->serialize($value);
        }

        $ret->__type = $entity->getClass();

        return $ret;
    }

    /**
     * @param \Traversable $traversable
     * @return array
     */
    private function serializeTraversable($traversable)
    {
        $ret = array();

        foreach ($traversable as $k => $v) {
            $ret[$k] = $this->serialize($v);
        }

        return $ret;
    }

    /**
     * @param AbstractSerializableObject $entity
     *
     * @return \ArrayIterator
     */
    private function getPrivatePropertiesIt(AbstractSerializableObject $entity)
    {
        $cls = $entity->getClass();

        if (! isset($this->privatePropertiesIt[$cls])) {
            $reflectionClass = new \ReflectionClass($cls);
            $privateProperties = $reflectionClass->getProperties(\ReflectionProperty::IS_PRIVATE);
            $this->privatePropertiesIt[$cls] = new \ArrayIterator($privateProperties);
        }
        return $this->privatePropertiesIt[$cls];
    }
}
