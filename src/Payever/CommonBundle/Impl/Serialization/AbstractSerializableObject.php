<?php
namespace Payever\CommonBundle\Impl\Serialization;

use Payever\CommonBundle\Interfaces\Reflector\ReflectiveClassInterface;
use Payever\CommonBundle\Interfaces\Serialization\SerializableObjectInterface;

/**
 * Class AbstractSerializableObject
 *
 * @package Payever\CommonBundle\Impl\Serialization
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
abstract class AbstractSerializableObject implements SerializableObjectInterface, ReflectiveClassInterface
{
    /**
     * @return \stdClass
     */
    public function toStdClass()
    {
        $serializer = new Serializer();

        return $serializer->serialize($this);
    }
}
