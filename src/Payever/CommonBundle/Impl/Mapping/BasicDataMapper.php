<?php
namespace Payever\CommonBundle\Impl\Mapping;

use Payever\CommonBundle\Impl\Reflection\Reflector;
use Payever\CommonBundle\Interfaces\Mapping\DataMapperInterface;

/**
 * Class BasicDataMapper
 *
 * @package Payever\CommonBundle\Impl\Mapping
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class BasicDataMapper implements DataMapperInterface
{
    /** @var mixed */
    private $target;

    /**
     * @param mixed $target
     */
    public function __construct(&$target)
    {
        $this->target = $target;
    }

    /**
     * {@inheritdoc}
     */
    public function mapToTarget($source, array $fieldNames, $ignoreMissingFields = false)
    {
        if ($this->target == null) {
            return;
        }

        $sourceReflect = Reflector::create($source);
        $targetReflect = Reflector::create($this->target);

        foreach ($fieldNames as $fieldName)
        {
            $canRead    = $sourceReflect->hasPublicReadAccessor($fieldName);
            $canWrite   = $targetReflect->hasPublicWriteAccessor($fieldName);

            if (! $ignoreMissingFields) {
                if (! $canRead) {
                    throw new MappingException('Cannot read property "' . $fieldName . '" from source');
                }
                if (! $canWrite) {
                    throw new MappingException('Cannot write property "' . $fieldName . '" to target');
                }
            }

            if ($canRead && $canWrite) {
                $data = $sourceReflect->getValue($fieldName);
                $targetReflect->setValue($fieldName, $data);
            }
        }
    }
}
