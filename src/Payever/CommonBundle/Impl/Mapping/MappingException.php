<?php
namespace Payever\CommonBundle\Impl\Mapping;

/**
 * Class MappingException
 *
 * @package Payever\CommonBundle\Impl\Mapping
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class MappingException extends \Exception
{
}
