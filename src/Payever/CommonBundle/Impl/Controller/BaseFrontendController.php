<?php
namespace Payever\CommonBundle\Impl\Controller;

use Psr\Log\LoggerInterface;
use \Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpKernel\Exception\GoneHttpException;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class BaseFrontendController
 *
 * @package Payever\CommonBundle\Impl\Controller
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class BaseFrontendController extends Controller
{
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // SHORTCUT - METHODS
    /////

    /**
     * Translates the given message.
     *
     * @param string $id         The message id
     * @param array  $parameters An array of parameters for the message
     * @param string $domain     The domain for the message - DEFAULT 'messages' see Translator::trans
     * @param string $locale     The locale
     *
     * @return string The translated string
     *
     * @see TranslatorInterface::trans
     * @see Symfony\Component\Translation\Translator::trans - for $domain = 'message' - Default-value
     */
    protected function __($id, array $parameters = array(), $domain = 'messages', $locale = null) {

        return $this->getTranslator()->trans($id, $parameters, $domain, $locale);
    }

    /**
     * @param string $message
     *
     * @return GoneHttpException
     */
    public function createGoneHttpException($message = null)
    {
        return new GoneHttpException($message);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // PRETTY LITTLE HELPERS
    /////

    /**
     * @param string    $notice
     */
    protected function addFlashNotice($notice)
    {
        $this->getSession()->getFlashBag()->add('notice', $notice);
    }

    /**
     * Disables the profiler if the profiler service is available
     */
    protected function disableProfiler()
    {
        if ($this->has('profiler')) {
            $this->get('profiler')->disable();
        }
    }

    /**
     * {@inheritdoc}
     */
    final public function createForm($type, $data = null, array $options = array())
    {
        return $this->getFormFactory()->create($type, $data, $options);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // GETTING SERVICES WITH A TYPE HINT
    /////

    /**
     * @return FormFactoryInterface
     */
    protected function getFormFactory()
    {
        return $this->get('payever.common.form.factory');
    }

    /**
     * @return null|\Symfony\Component\HttpFoundation\Session\Session
     */
    protected function getSession() {
        return $this->getRequest()->getSession();
    }

    /**
     * @return \Symfony\Component\Translation\TranslatorInterface
     */
    protected function getTranslator() {
        return $this->get('translator');
    }

    /**
     * @return LoggerInterface
     */
    protected function getLogger()
    {
        return $this->get('logger');
    }

    /**
     * @return RouterInterface
     */
    protected function getRouter()
    {
        return $this->get('router');
    }
}
