<?php
namespace Payever\CommonBundle\Impl\Forms\Transforms;

use Symfony\Component\Form\DataTransformerInterface;

/**
 * Class FloatTransformer
 *
 * @package Payever\CommonBundle\Impl\Forms\Transforms
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class FloatTransformer implements DataTransformerInterface
{
    /**
     * @param mixed $value
     * @return string
     */
    public function transform($value)
    {
        return (string) floatval($value);
    }

    /**
     * @param mixed $value
     * @return float
     */
    public function reverseTransform($value)
    {
        return floatval($value);
    }
}
