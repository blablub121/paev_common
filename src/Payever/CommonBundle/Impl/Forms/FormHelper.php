<?php
namespace Payever\CommonBundle\Impl\Forms;

use Symfony\Component\Form\FormView;

/**
 * Class FormHelper
 *
 * @package Payever\CommonBundle\Impl\Forms
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class FormHelper
{
    /**
     * @param FormView $view
     *
     * @return bool
     */
    public function hasErrors(FormView $view = null)
    {
        if ($view === null) {
            return false;
        }

        if (isset($view->vars['errors']) && count($view->vars['errors']) > 0) {
            return true;
        }

        foreach ($view->getIterator() as $child) {

            if ($child instanceof FormView) {
                if ($this->hasErrors($child)) {
                    return true;
                }
            }
        }

        return false;
    }
}
