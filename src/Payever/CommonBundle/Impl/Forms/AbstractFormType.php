<?php
namespace Payever\CommonBundle\Impl\Forms;

use Payever\CommonBundle\Interfaces\Awareness\TranslatorAwareInterface;
use \Symfony\Component\Form\AbstractType;
use \Symfony\Component\Translation\TranslatorInterface;

/**
 * Class AbstractFormType
 *
 * @package Payever\CommonBundle\Impl\Forms
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
abstract class AbstractFormType extends AbstractType implements TranslatorAwareInterface
{
    /** @var TranslatorInterface */
    private $translator;

    /**
     * @return string
     */
    public static function getClass()
    {
        return get_called_class();
    }

    /**
     * @param TranslatorInterface $translator
     */
    public function setTranslator(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @return TranslatorInterface
     */
    public function getTranslator()
    {
        return $this->translator;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // SHORTCUT - METHODS
    /////

    /**
     * Translates the given message.
     *
     * @param string $id         The message id
     * @param array  $parameters An array of parameters for the message
     * @param string $domain     The domain for the message - DEFAULT 'messages' see Translator::trans
     * @param string $locale     The locale
     *
     * @return string The translated string
     *
     * @see TranslatorInterface::trans
     * @see Symfony\Component\Translation\Translator::trans - for $domain = 'message' - Default-value
     */
    protected function __($id, array $parameters = array(), $domain = 'messages', $locale = null) {

        return $this->getTranslator()->trans($id, $parameters, $domain, $locale);
    }
}
