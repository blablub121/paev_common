<?php
namespace Payever\CommonBundle\Impl\Forms\MultiStep\Data;

use Payever\CommonBundle\Impl\Forms\MultiStep\Data\PersistentData;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormInterface;

/**
 * Class ProcessingResult
 *
 * @package Payever\CommonBundle\Impl\Forms\MultiStep\Data
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class ProcessingResult
{
    /** @var FormInterface */
    private $form;
    /** @var PersistentData */
    private $data;
    /** @var boolean */
    private $stepOk;
    /** @var boolean */
    private $isCompleted;

    /**
     * @param FormInterface     $form
     * @param PersistentData    $data
     * @param boolean           $stepOk
     * @param boolean           $isCompleted
     */
    public function __construct(FormInterface $form, PersistentData $data, $stepOk, $isCompleted)
    {
        $this->form         = $form;
        $this->data         = $data;
        $this->stepOk       = $stepOk;
        $this->isCompleted  = $isCompleted;
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param int       $step
     * @return array
     */
    public function getStepData($step)
    {
        return $this->getData()->getStepData($step);
    }

    /**
     * @return PersistentData
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return boolean
     */
    public function isStepOk()
    {
        return $this->stepOk;
    }

    /**
     * @return boolean
     */
    public function isCompleted()
    {
        return $this->isCompleted;
    }
}
