<?php
namespace Payever\CommonBundle\Impl\Forms\MultiStep\Data;

/**
 * Class StepCollection
 *
 * @package Payever\CommonBundle\Impl\Forms\MultiStep\Data
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class StepCollection
{
    /** @var MultiStepFormStep[] */
    private $steps = array();

    /**
     * @param MultiStepFormStep $step
     */
    public function add(MultiStepFormStep $step)
    {
        $this->steps[] = $step;
    }

    /**
     * @param MultiStepFormStep $step
     *
     * @return bool
     */
    public function has(MultiStepFormStep $step)
    {
        foreach ($this->steps as $stepInList) {
            if ($step === $stepInList) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param int $idx
     *
     * @throws \OutOfRangeException
     * @return MultiStepFormStep
     */
    public function getAt($idx)
    {
        if ($idx < 0 || $idx >= count($this->steps)) {
            throw new \OutOfRangeException('No step at index ' . $idx);
        }

        return $this->steps[$idx];
    }

    /**
     * @param string    $uri
     *
     * @return bool
     */
    public function hasWithUri($uri)
    {
        return $this->getWithUri($uri) !== null;
    }

    /**
     * @param string    $uri
     *
     * @return MultiStepFormStep|null
     */
    public function getWithUri($uri)
    {
        foreach ($this->steps as $step) {
            if ($step->getUri() === $uri) {
                return $step;
            }
        }

        return null;
    }

    /**
     * @return MultiStepFormStep[]
     */
    public function all()
    {
        return $this->steps;
    }

    /**
     * @param MultiStepFormStep $beforeStep
     *
     * @return MultiStepFormStep[]
     */
    public function allBefore(MultiStepFormStep $beforeStep)
    {
        if (!$this->has($beforeStep)) {
            return array();
        }

        $result = array();

        foreach ($this->steps as $step) {
            if ($step === $beforeStep) {
                break;
            }

            $result[] = $step;
        }

        return $result;
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->steps);
    }

    /**
     * @return MultiStepFormStep
     */
    public function getLast()
    {
        if ($this->count() == 0) {
            return null;
        }

        return $this->steps[$this->count() - 1];
    }
}
