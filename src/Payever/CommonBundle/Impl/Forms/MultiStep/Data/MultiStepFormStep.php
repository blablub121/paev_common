<?php
namespace Payever\CommonBundle\Impl\Forms\MultiStep\Data;

use Payever\CommonBundle\Interfaces\Mapping\DataMapperInterface;
use Symfony\Component\Form\FormTypeInterface;

/**
 * Class MultiStepFormStep
 *
 * @package Payever\CommonBundle\Impl\Forms\MultiStep\Data
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class MultiStepFormStep
{
    /** @var string */
    private $name;
    /** @var string */
    private $uri;
    /** @var FormTypeInterface */
    private $formType;
    /** @var null|mixed */
    private $data;
    /** @var array */
    private $options = array();
    /** @var DataMapperInterface */
    private $mapper;
    /** @var mixed */
    private $defaultData;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return \Symfony\Component\Form\FormTypeInterface
     */
    public function getFormType()
    {
        return $this->formType;
    }

    /**
     * @param \Symfony\Component\Form\FormTypeInterface $formType
     *
     * @return $this
     */
    public function setFormType($formType)
    {
        $this->formType = $formType;

        return $this;
    }

    /**
     * @param null|mixed    $data
     *
     * @return $this
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * @return mixed|null
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param array $options
     *
     * @return $this
     */
    public function setOptions(array $options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return DataMapperInterface
     */
    public function getMapper()
    {
        return $this->mapper;
    }

    /**
     * @param DataMapperInterface $mapper
     *
     * @return $this
     */
    public function setMapper($mapper)
    {
        $this->mapper = $mapper;

        return $this;
    }

    /**
     * @param mixed $data
     */
    public function setDefaultData($data)
    {
        $this->defaultData = $data;
    }

    /**
     * @return mixed
     */
    public function getDefaultData()
    {
        return $this->defaultData;
    }
}
