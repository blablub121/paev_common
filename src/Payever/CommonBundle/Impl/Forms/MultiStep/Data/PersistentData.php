<?php
namespace Payever\CommonBundle\Impl\Forms\MultiStep\Data;

/**
 * Class PersistentData
 *
 * @package Payever\CommonBundle\Impl\Forms\MultiStep
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class PersistentData {

    /** @var array */
    private $data = array();
    /** @var string[] */
    private $validSteps = array();

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @param string    $stepUri
     *
     * @return array|null
     */
    public function getStepData($stepUri)
    {
        return array_key_exists($stepUri, $this->data) ? $this->data[$stepUri] : null;
    }

    /**
     * @param string    $stepUri
     *
     * @return bool
     */
    public function hasStepData($stepUri)
    {
        return array_key_exists($stepUri, $this->data);
    }

    /**
     * @param string    $stepUri
     * @param mixed     $data
     */
    public function setStepData($stepUri, $data)
    {
        $this->data[$stepUri] = $data;
    }

    /**
     * @param string    $stepUri
     * @param bool      $valid
     */
    public function markStepValid($stepUri, $valid = true)
    {
        if ($valid) {
            $this->validSteps[$stepUri] = true;
        } else {
            unset($this->validSteps[$stepUri]);
        }
    }

    /**
     * @return string[]
     */
    public function getValidStepUris()
    {
        return array_keys($this->validSteps);
    }
}
