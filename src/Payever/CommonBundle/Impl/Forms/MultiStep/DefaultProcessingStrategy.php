<?php
namespace Payever\CommonBundle\Impl\Forms\MultiStep;

use Payever\CommonBundle\Impl\Forms\MultiStep\Data\MultiStepFormStep;
use Payever\CommonBundle\Impl\Forms\MultiStep\Data\ProcessingResult;
use Payever\CommonBundle\Impl\Forms\MultiStep\Data\PersistentData;
use Payever\CommonBundle\Impl\Forms\MultiStep\Data\StepCollection;
use Payever\CommonBundle\Impl\Request\Uri;
use Payever\CommonBundle\Interfaces\Forms\MultiStep\ProcessingStrategyInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class DefaultProcessingStrategy
 *
 * @package Payever\CommonBundle\Impl\Forms\MultiStep
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class DefaultProcessingStrategy implements ProcessingStrategyInterface
{
    const SESSION_PREFIX            = 'msf_';
    const VALIDATION_GROUP_PREFIX   = 'step';

    /** @var StepCollection  */
    private $steps;
    /** @var Uri */
    private $currentUri;
    /** @var Uri */
    private $targetUri;
    /** @var Request */
    private $request;
    /** @var FormFactoryInterface */
    private $formFactory;
    /** @var SessionInterface */
    private $session;

    /** @var string */
    private $persistenceId;

    /**
     * @param string               $processName
     * @param StepCollection       $steps
     * @param Uri                  $currentUri
     * @param Uri                  $targetUri
     * @param Request              $request
     * @param FormFactoryInterface $formFactory
     * @param SessionInterface     $session
     */
    public function __construct(
        $processName,
        StepCollection $steps,
        $currentUri,
        $targetUri,
        Request $request,
        FormFactoryInterface
        $formFactory,
        SessionInterface $session
    ) {
        $this->steps        = $steps;
        $this->currentUri   = $currentUri;
        $this->targetUri    = $targetUri;
        $this->request      = $request;
        $this->formFactory  = $formFactory;
        $this->session      = $session;

        $this->persistenceId = self::SESSION_PREFIX . $processName;
    }

    /**
     * {@inheritdoc}
     */
    public function cleanUp()
    {
        $this->session->clear($this->persistenceId);
    }

    /**
     * {@inheritdoc}
     */
    public function processStep($formName)
    {
        // get data from session and create the form
        $multiStepFormData = $this->loadData();

        $step       = $this->steps->getWithUri($this->currentUri->get());
        $formType   = $step->getFormType();
        $options    = array(
            'validation_groups' => $this->getValidationGroups($formType)
        );

        if ($multiStepFormData->hasStepData($this->currentUri->get())) {
            $stepData  = $multiStepFormData->getStepData($this->currentUri->get());
        } else {
            $stepData = $step->getDefaultData();
        }

        $form = $this->formFactory->create($formType, $stepData, $options);

        $stepOk = false;

        if ($this->request->isMethod('POST')) {
            $form->handleRequest($this->request);

            if ($form->isValid()) {
                $multiStepFormData->setStepData($this->currentUri->get(), $form->getData());

                $stepOk = true;
            }

            // mark the current steps validity
            $multiStepFormData->markStepValid($this->currentUri->get(), $stepOk);
        }

        // write back to session
        $this->saveData($multiStepFormData);

        return new ProcessingResult($form, $multiStepFormData, $stepOk, $stepOk && $this->isCompleted());
    }

    /**
     * @return MultiStepFormStep
     * @throws MultiStepFormException
     */
    public function getNextStep()
    {
        // do we explicitly know where to go next?
        if (! $this->targetUri->isEmpty()) {
            if ($this->steps->hasWithUri($this->targetUri->get())) {
                return $this->steps->getWithUri($this->targetUri->get());
            }

            throw new MultiStepFormException("Unknown target step '{$this->targetUri->get()}'");
        }

        // no explicitly target step is set so we try to go to the next step
        $returnNext = false;
        foreach ($this->steps->all() as $step) {
            if ($returnNext) {
                return $step;
            }

            if ($step->getUri() == $this->currentUri->get()) {
                $returnNext = true;
            }
        }

        throw new MultiStepFormException("There is no step after '{$this->currentUri->get()}'");
    }

    /**
     * @param ProcessingResult      $data
     * @param MultiStepFormStep     $stepToTest
     *
     * @return boolean
     */
    public function isStepReachable(ProcessingResult $data, MultiStepFormStep $stepToTest)
    {
        // A step is reachable if all the preceding steps and step it self are valid.
        $validStepUris = $data->getData()->getValidStepUris();

        foreach ($this->steps->allBefore($stepToTest) as $step)
        {
            if (!in_array($step->getUri(), $validStepUris)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param FormTypeInterface $formType
     *
     * @return array
     */
    protected function getValidationGroups(FormTypeInterface $formType)
    {
        if ($formType instanceof MultiStepFormTypeInterface) {
            return array($formType, 'getValidationGroups');
        }

        return array('Default');
    }

    /**
     * @return boolean
     */
    protected function isCompleted()
    {
        // we are done when the current step is the last step and when there is no explicit target step
        return
            $this->steps->getLast()->getUri() === $this->currentUri->get()
            &&
            $this->targetUri->isEmpty()
        ;
    }

    /**
     * @return PersistentData
     */
    protected function loadData()
    {
        return $this->session->get($this->persistenceId, new PersistentData());
    }

    /**
     * @param PersistentData $data
     */
    protected function saveData(PersistentData $data)
    {
        $this->session->set($this->persistenceId, $data);
    }
}
