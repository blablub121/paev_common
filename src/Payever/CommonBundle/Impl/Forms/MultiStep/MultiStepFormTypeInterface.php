<?php
namespace Payever\CommonBundle\Impl\Forms\MultiStep;

/**
 * Class MultiStepFormTypeInterface
 *
 * @package Payever\CommonBundle\Impl\Forms\MultiStep
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
interface MultiStepFormTypeInterface
{
    /**
     * @return array
     */
    public function getValidationGroups();
}
