<?php
namespace Payever\CommonBundle\Impl\Forms\MultiStep;

/**
 * Class MultiStepFormException
 *
 * @package Payever\CommonBundle\Impl\Forms\MultiStep
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class MultiStepFormException extends \Exception
{
}
