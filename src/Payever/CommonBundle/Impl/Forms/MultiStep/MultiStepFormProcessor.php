<?php
namespace Payever\CommonBundle\Impl\Forms\MultiStep;

use Payever\CommonBundle\Impl\Forms\MultiStep\Data\MultiStepFormStep;
use Payever\CommonBundle\Impl\Forms\MultiStep\Data\ProcessingResult;
use Payever\CommonBundle\Impl\Forms\MultiStep\Data\StepCollection;
use Payever\CommonBundle\Impl\Request\Uri;
use Payever\CommonBundle\Interfaces\Forms\MultiStep\ProcessingStrategyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class MultiStepFormProcessor
 *
 * @package Payever\CommonBundle\Impl\Forms\MultiStep
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class MultiStepFormProcessor
{
    /** @var string */
    protected $processName;
    /** @var Request  */
    protected $request;
    /** @var SessionInterface  */
    protected $session;
    /** @var FormFactoryInterface  */
    protected $formFactory;

    /** @var Uri */
    protected $currentStepUri;
    /** @var Uri */
    protected $targetStepUri;

    /** @var ProcessingStrategyInterface */
    protected $processing;

    /** @var StepCollection */
    protected $steps;

    /** @var ProcessingResult */
    protected $result = false;

    /**
     * @return string
     */
    public static function getClass()
    {
        return get_class();
    }

    //// INITIALIZATION ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param string                $processName
     * @param Request               $request
     * @param SessionInterface      $session
     * @param FormFactoryInterface  $formFactory
     */
    public function __construct($processName, Request $request, SessionInterface $session, FormFactoryInterface $formFactory)
    {
        $this->processName  = $processName;
        $this->request      = $request;
        $this->session      = $session;
        $this->formFactory  = $formFactory;

        $this->steps            = new StepCollection();

        // get current step and target step uris
        $this->targetStepUri    = new Uri($this->request->request->get('__target_step', null));
        $this->currentStepUri   = new Uri($this->request->attributes->get('step', null));

        // create processing strategy
        $this->processing = $this->createProcessingStrategy();
    }

    /**
     *  @return ProcessingStrategyInterface
     */
    protected function createProcessingStrategy()
    {
        return new DefaultProcessingStrategy(
            $this->processName,
            $this->steps,
            $this->currentStepUri,
            $this->targetStepUri,
            $this->request,
            $this->formFactory,
            $this->session
        );
    }

    /**
     * @param string       $name
     * @param string       $uri
     * @param AbstractType $formType
     *
     * @throws MultiStepFormException
     *
     * @return MultiStepFormStep
     */
    public function addStep($name, $uri, AbstractType $formType)
    {
        // validate
        if ($this->steps->hasWithUri($uri)) {
            throw new MultiStepFormException("You cannot add a step with the uri '$uri' a second time");
        }

        if (empty($uri)) {
            throw new MultiStepFormException("The steps uri must not be empty");
        }

        // create the step
        $step = new MultiStepFormStep();
        $step->setName($name);
        $step->setUri($uri);
        $step->setFormType($formType);

        $this->steps->add($step);

        // when we do not current step uri, we automatically choose the first step
        if ($this->currentStepUri->isEmpty()) {
            $this->currentStepUri->set($step->getUri());
        }

        return $step;
    }

    //// PROCESSING ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Processes the current step.
     *
     * Validates the request data for the current step.
     *
     * @return boolean False if there current uri is not defined or not reachable
     */
    public function process()
    {
        // 1. ensure that the requested uri exists in the list of steps
        $currentStep = $this->steps->getWithUri($this->currentStepUri->get());

        if ($currentStep === null) {
            return false;
        }

        // 2. do the processing
        $this->result = $this->processing->processStep($this->processName);

        // 3. decide if the current step is reachable (or if the user tried to skip some steps)
        return $this->processing->isStepReachable($this->result, $currentStep);
    }

    /**
     * @throws MultiStepFormException
     *
     * @return FormView
     */
    public function createFormView()
    {
        if ($this->result === null) {
            throw new MultiStepFormException('You must call process() before you can call createFormView()');
        }

        return $this->result->getForm()->createView();
    }

    /**
     * Map the results according to the mappers set for each step
     *
     * @throws MultiStepFormException
     */
    public function mapResult()
    {
        if ($this->result === null) {
            throw new MultiStepFormException('You must call process() before you can call mapResult()');
        }

        // go through all steps
        foreach ($this->steps->all() as $step) {
            // does the step has an individual mapper?
            $mapper = $step->getMapper();
            if ($mapper !== null) {
                $stepForm = $this->formFactory->create($step->getFormType(), null, $step->getOptions());

                $fieldNames = array();
                foreach ($stepForm->all() as $formField) {
                    $fieldNames[] = $formField->getName();
                }

                $mapper->mapToTarget($this->result->getStepData($step->getUri()), $fieldNames, true);
            }
        }
    }

    //// NAVIGATION ////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Is the whole process finished?
     *
     * @throws MultiStepFormException
     *
     * @return bool
     */
    public function isCompleted()
    {
        if ($this->result === null) {
            throw new MultiStepFormException('You must call process() before you can call isCompleted()');
        }

        return $this->result->isCompleted();
    }

    /**
     * Was the form for the current step filled out without errors?
     *
     * @throws MultiStepFormException
     *
     * @return bool
     */
    public function isStepOk()
    {
        if ($this->result === null) {
            throw new MultiStepFormException('You must call process() before you can call isStepOk()');
        }

        return $this->result->isStepOk();
    }

    /**
     * @throws MultiStepFormException
     *
     * @return MultiStepFormStep
     */
    public function getNextStep()
    {
        if ($this->result === null) {
            throw new MultiStepFormException('You must call process() before you can call getNextStepUri()');
        }

        return $this->processing->getNextStep();
    }

    /**
     * @param MultiStepFormStep $step
     *
     * @return boolean
     */
    public function isStepReachable(MultiStepFormStep $step)
    {
        return $this->processing->isStepReachable($this->result, $step);
    }

    /**
     * @return MultiStepFormStep[]
     */
    public function getSteps()
    {
        return $this->steps->all();
    }

    /**
     * @param int $idx
     *
     * @throws \OutOfRangeException
     * @return MultiStepFormStep
     */
    public function getStep($idx)
    {
        return $this->steps->getAt($idx);
    }

    /**
     * @return string
     */
    public function getCurrentStepUri()
    {
        return $this->currentStepUri;
    }

    /**
     * @return int|null
     */
    public function getCurrentStepNum()
    {
        $count = 1;
        foreach ($this->steps->all() as $step) {
            if ($step->getUri() == $this->currentStepUri->get()) {
                return $count;
            }
            $count++;
        }

        return null;
    }

    /**
     * Clean up for example if something was written to the session
     */
    public function cleanUp()
    {
        $this->processing->cleanUp();
    }
}
