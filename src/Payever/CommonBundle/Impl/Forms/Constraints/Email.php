<?php
namespace Payever\CommonBundle\Impl\Forms\Constraints;

use Symfony\Component\Validator\Constraints\Email as BaseEmail;

/**
 * Class Email
 *
 * @package Payever\CommonBundle\Impl\Forms\Constraints
 *
 * @Annotation
 */
class Email extends BaseEmail
{

}
