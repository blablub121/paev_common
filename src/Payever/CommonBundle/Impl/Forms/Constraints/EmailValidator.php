<?php
namespace Payever\CommonBundle\Impl\Forms\Constraints;

use Symfony\Component\Validator\Constraints\EmailValidator as BaseEmailValidator;
use Symfony\Component\Validator\Constraint;

/**
 * Class EmailValidator
 *
 * @package Payever\CommonBundle\Impl\Forms\Constraints
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class EmailValidator extends BaseEmailValidator
{
    /**
     * {@inheritDoc}
     *
     * @param Email $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        parent::validate($value, $constraint);

        if (strpos($value, '/') !== false) {
            $this->context->addViolation($constraint->message, array('{{ value }}' => $value));
        }
    }
}
