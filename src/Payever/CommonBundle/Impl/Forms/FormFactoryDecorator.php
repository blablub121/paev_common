<?php
namespace Payever\CommonBundle\Impl\Forms;

use Payever\CommonBundle\Interfaces\Awareness\TranslatorAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class FormFactoryDecorator
 *
 * @package Payever\CommonBundle\Impl\Forms
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class FormFactoryDecorator extends FormFactory
{
    /** @var FormFactoryInterface */
    private $nativeFormFactory;
    /** @var TranslatorInterface */
    private $translator;

    /**
     * @param FormFactoryInterface  $nativeFormFactory
     * @param TranslatorInterface   $translator
     */
    public function __construct(FormFactoryInterface $nativeFormFactory, TranslatorInterface $translator)
    {
        $this->nativeFormFactory    = $nativeFormFactory;
        $this->translator           = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function create($type = 'form', $data = null, array $options = array())
    {
        if ($type instanceof TranslatorAwareInterface) {
            $type->setTranslator($this->translator);
        }

        return $this->nativeFormFactory->create($type, $data, $options);
    }
}
