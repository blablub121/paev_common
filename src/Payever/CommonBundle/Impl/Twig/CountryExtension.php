<?php
namespace Payever\CommonBundle\Impl\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Intl\Intl;

/**
 * Class CountryExtension
 *
 * @package Payever\CommonBundle\Impl\Twig
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class CountryExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('country', array($this, 'country')),
        );
    }

    /**
     * @param string $countryCode
     * @param string $locale
     *
     * @return mixed
     */
    public function country($countryCode, $locale = 'en')
    {
        $regionBundle = Intl::getRegionBundle();// getDisplayCountries($locale);

        return $regionBundle->getCountryName($countryCode, $locale);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'payever_country_extension';
    }
}
