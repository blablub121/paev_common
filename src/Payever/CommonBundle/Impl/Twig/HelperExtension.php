<?php
namespace Payever\CommonBundle\Impl\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Intl\Intl;

/**
 * Class HelperExtension
 *
 * @package Payever\CommonBundle\Impl\Twig
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class HelperExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('toArray', array($this, 'toArrayFilter')),
            new \Twig_SimpleFilter('toString', array($this, 'toStringFilter')),
        );
    }

    /**
     * @param mixed $val
     *
     * @return mixed
     */
    public function toArrayFilter($val)
    {
        if (is_array($val)) {
            return $val;
        }
        if (is_object($val)) {
            return get_object_vars($val);
        }

        return array($val);
    }

    /**
     * @param $val
     *
     * @return string
     */
    public function toStringFilter($val)
    {
        if ($val instanceof \DateTime) {
            return $val->format('c');
        }

        if (is_object($val)) {

            if (method_exists($val, '__toString')) {
                return $val->__toString();
            }
        } else {
            try {
                return (string) $val;
            } catch (\Exception $e) {
                return '';
            }
        }

        return '';
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'payever_helper_extension';
    }
}
