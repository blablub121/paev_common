<?php
namespace Payever\CommonBundle\Impl\Reflection;

/**
 * Class Reflector
 *
 * @package Payever\CommonBundle\Impl\Reflection
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class Reflector
{
    /** @var object */
    private $subject;
    /** @var \ReflectionClass */
    private $reflect;

    /**
     * @return string
     */
    public static function getClass()
    {
        return get_class();
    }

    /**
     * @param object    $subject
     *
     * @return Reflector
     */
    public static function create($subject)
    {
        return new Reflector($subject);
    }

    /**
     * @param object    $subject
     *
     * @throws ReflectorException When the given subject is not an object
     */
    public function __construct($subject)
    {
        if (!is_object($subject)) {
            throw new ReflectorException('The subject of a reflection must be an object');
        }

        $this->subject = $subject;
        $this->reflect = new \ReflectionClass($subject);
    }

    /**
     * @return mixed
     */
    public final function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return \ReflectionProperty[]
     */
    public function getPublicProperties()
    {
        return $this->reflect->getProperties(\ReflectionProperty::IS_PUBLIC);
    }

    /**
     * @return string[]
     */
    public function getPublicPropertyNames()
    {
        $props = $this->getPublicProperties();

        $ret = array();
        foreach ($props as $prop) {
            $ret[] = $prop->getName();
        }

        return $ret;
    }


    /**
     * Try to find a reading accessor for the given property name.
     *
     * The method tries the following the in the following order:
     *
     * 1. Find a public property with the given $propertyName
     * 2. Find a public getter for the given $propertyName
     * 3. Find a public hasser for the given $propertyName
     * 4. Find a public isser for the given $propertyName
     *
     * @param string    $propertyName
     *
     * @return bool When an accessor was found
     */
    public function hasPublicReadAccessor($propertyName)
    {
        return $this->hasStdClassProperty($propertyName)
            || $this->findPublicNonStaticProperty($propertyName) !== null
            || $this->findPublicNonStaticGetter($propertyName) !== null
            || $this->findPublicNonStaticHasser($propertyName) !== null
            || $this->findPublicNonStaticIsser($propertyName)
        ;
    }

    /**
     * Try to get a value identified by the property name.
     *
     * The method tries the following the in the following order:
     *
     * 1. Find a public property with the given $propertyName
     * 2. Find a public getter for the given $propertyName
     * 3. Find a public hasser for the given $propertyName
     * 4. Find a public isser for the given $propertyName
     *
     * If non the above is successful an exception is thrown.
     *
     * @param string    $propertyName
     *
     * @return mixed    The acquired value
     *
     * @throws ReflectorException
     */
    public function getValue($propertyName)
    {
        if ($this->hasStdClassProperty($propertyName)) {
            return $this->subject->$propertyName;
        }

        if ($property = $this->findPublicNonStaticProperty($propertyName)) {
            return $property->getValue($this->subject);
        }

        if ($getter = $this->findPublicNonStaticGetter($propertyName)) {
            return $getter->invoke($this->subject);
        }

        if ($hasser = $this->findPublicNonStaticHasser($propertyName)) {
            return $hasser->invoke($this->subject);
        }

        if ($isser = $this->findPublicNonStaticIsser($propertyName)) {
            return $isser->invoke($this->subject);
        }

        throw new ReflectorException(
            "Could not find public property, getter, hasser or isser for '$propertyName' on object '{$this->reflect->getName()}'"
        );
    }

    /**
    /**
     * Try to find a writing accessor for the given property name.
     *
     * The method tries the following things in the following order:
     *
     * 1. Find a public property with the given $propertyName
     * 2. Find a public setter with the given $propertyName
     *
     * @param $propertyName
     *
     * @return bool
     */
    public function hasPublicWriteAccessor($propertyName)
    {
        return $this->subject instanceof \stdClass
            || $this->findPublicNonStaticProperty($propertyName) !== null
            || $this->findPublicNonStaticSetter($propertyName);
    }

    /**
     * Try to set the given value to property identified by the property name.
     *
     * The method tries the following things in the following order:
     *
     * 1. Find a public property with the given $propertyName
     * 2. Find a public setter with the given $propertyName
     * 3. If the subject ($this->subject) is a \stdClass the property is set by name
     *
     * @param string    $propertyName
     * @param mixed     $data
     *
     * @throws ReflectorException
     */
    public function setValue($propertyName, $data)
    {
        // special handling for non-sealed \stdClass objects
        if ($this->subject instanceof \stdClass) {
            $this->subject->$propertyName = $data;
            return;
        }

        if ($property = $this->findPublicNonStaticProperty($propertyName)) {
            $property->setValue($this->subject, $data);
            return;
        }

        if ($setter = $this->findPublicNonStaticSetter($propertyName))
        {
            $setter->invoke($this->subject, $data);
            return;
        }

        throw new ReflectorException(
            "Subject is no \\stdClass and could not find public property or setter for '$propertyName' on object '{$this->reflect->getName()}'"
        );
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    private function hasStdClassProperty($name)
    {
        return ($this->subject instanceof \stdClass && isset($this->subject->$name));
    }

    /**
     * @param string    $name
     *
     * @return null|\ReflectionProperty
     */
    private function findPublicNonStaticProperty($name)
    {
        try {
            $property = $this->reflect->getProperty($name);
            return $property->isPublic() && !$property->isStatic() ? $property : null;
        } catch (\ReflectionException $e) {
            return null;
        }
    }

    /**
     * @param string    $name
     *
     * @return null|\ReflectionMethod
     */
    private function findPublicNonStaticGetter($name)
    {
        try {
            $method = $this->reflect->getMethod('get' . ucfirst($name));
            return $method->isPublic() && !$method->isStatic() ? $method : null;
        } catch (\ReflectionException $e) {
            return null;
        }
    }

    /**
     * @param string    $name
     *
     * @return null|\ReflectionMethod
     */
    private function findPublicNonStaticHasser($name)
    {
        try {
            $method = $this->reflect->getMethod('has' . ucfirst($name));
            return $method->isPublic() && !$method->isStatic() ? $method : null;
        } catch (\ReflectionException $e) {
            return null;
        }
    }

    /**
     * @param string    $name
     *
     * @return null|\ReflectionMethod
     */
    private function findPublicNonStaticIsser($name)
    {
        try {
            $method = $this->reflect->getMethod('is' . ucfirst($name));
            return $method->isPublic() && !$method->isStatic() ? $method : null;
        } catch (\ReflectionException $e) {
            return null;
        }
    }

    /**
     * @param string    $name
     *
     * @return null|\ReflectionMethod
     */
    private function findPublicNonStaticSetter($name)
    {
        try {
            $method = $this->reflect->getMethod('set' . ucfirst($name));
            return $method->isPublic() && !$method->isStatic() ? $method : null;
        } catch (\ReflectionException $e) {
            return null;
        }
    }
}
