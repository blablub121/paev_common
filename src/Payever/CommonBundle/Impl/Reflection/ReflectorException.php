<?php
namespace Payever\CommonBundle\Impl\Reflection;

/**
 * Class ReflectorException
 *
 * @package Payever\CommonBundle\Impl\Reflection
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class ReflectorException extends \ReflectionException
{
}
