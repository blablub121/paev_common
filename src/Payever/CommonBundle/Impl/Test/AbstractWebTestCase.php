<?php
namespace Payever\CommonBundle\Impl\Test;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as WebTestCaseBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AbstractWebTestCase
 *
 * @package Payever\CommonBundle\Impl\Test
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
abstract class AbstractWebTestCase extends WebTestCaseBase
{
    /** @var \Symfony\Bundle\FrameworkBundle\Client  */
    protected $client;
    /** @var \Symfony\Bundle\FrameworkBundle\Routing\Router */
    protected $router;

    /**
     * set up
     */
    public function setUp()
    {
        $this->client = static::createClient();
        $this->router = $this->client->getContainer()->get('router');
    }

    /**
     * Verbose output on failed tests.
     *
     * In most cases the functional test fails, because something went wrong within a webservice call.
     * The method the checks whether the test where run with the --verbose option, and if so the last http-request
     * response is dumped.
     */
    public function tearDown()
    {
        global $argv;
        if ($this->hasFailed()) {
            if (in_array('--verbose', $argv) || in_array('--verbose', $_GET)) {
                if ($this->getResponse()) {
                    print_r($this->getResponse()->getContent());
                }
            }
        }
    }

    /**
     * @param string    $method
     * @param string    $route
     * @param array     $params
     * @param string    $content
     *
     * @return \Symfony\Component\DomCrawler\Crawler
     */
    protected function requestRoute($method, $route, $params = array(), $content = null)
    {
        return $this->client->request(
            $method,
            $this->router->generate($route, $params),
            array(),
            array(),
            array(),
            $content
        );
    }

    /**
     * @return null|\Symfony\Component\HttpFoundation\Response
     */
    protected function getResponse()
    {
        return $this->client->getResponse();
    }
}
