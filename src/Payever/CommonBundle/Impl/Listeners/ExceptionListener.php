<?php
namespace Payever\CommonBundle\Impl\Listeners;

use Psr\Log\LoggerInterface;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

/**
 * Class ExceptionListener
 *
 * @package Payever\CommonBundle\Impl\Listeners
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class ExceptionListener
{
    /**
     * @var LoggerInterface
     */
    private $logger = null;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger = null)
    {
        $this->logger = $logger;
    }

    /**
     * @param \Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event
     */
    public function onException(GetResponseForExceptionEvent $event)
    {
        if ($this->logger === null) {
            return;
        }

        // todo: put the code into a special trace logger service and use it

        $exception = $event->getException();
        $flattenException = FlattenException::create($exception);

        $output  = "[EXCEPTION] " . $exception->getMessage() . "\n";
        $output .= "Stack trace\n";

        foreach ($flattenException->getTrace() as $trace) {
            $file = isset($trace['file']) ? $trace['file'] : 'unknown';
            $line = isset($trace['line']) ? $trace['line'] : 'unknown';

            $output .= sprintf("at %s line %s\n", $file, $line);
        }

        $this->logger->error($output);
    }
}
