<?php
namespace Payever\CommonBundle\Impl\Request;

/**
 * Class Uri
 *
 * @package Payever\CommonBundle\Impl\Request
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class Uri
{
    /** @var string */
    private $uri;

    /**
     * @param string $uri
     */
    public function __construct($uri)
    {
        $this->set($uri);
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return $this->uri === null;
    }

    /**
     * @return string
     */
    public function get()
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     */
    public function set($uri)
    {
        $this->uri = $uri;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->get();
    }
}
