<?php
namespace Payever\CommonBundle\Impl;

use Payever\CommonBundle\Impl\FormValues\Branch;
use Payever\CommonBundle\Impl\FormValues\CreditDueDate;
use Payever\CommonBundle\Impl\FormValues\CreditDuration;
use Payever\CommonBundle\Impl\FormValues\CreditReason;
use Payever\CommonBundle\Impl\FormValues\EmploymentContractType;
use Payever\CommonBundle\Impl\FormValues\EmploymentType;
use Payever\CommonBundle\Impl\FormValues\EnumerableConstants;
use Payever\CommonBundle\Impl\FormValues\MaritalStatus;
use Payever\CommonBundle\Impl\FormValues\ResidencePermit;
use Payever\CommonBundle\Impl\FormValues\ResidenceTerm;
use Payever\CommonBundle\Impl\FormValues\Salutation;
use Payever\CommonBundle\Impl\FormValues\WorkPermit;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class FormValuesService
 *
 * @package Payever\CommonBundle\Impl
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class FormValuesService
{
    /** @var TranslatorInterface  */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @return array
     */
    public function getBranches()
    {
        return $this->translateEnum(Branch::instance());
    }

    /**
     * @return array
     */
    public function getBirthYearsRange()
    {
        $now = new \DateTime('NOW');

        return range($now->format('Y') - 18, $now->format('Y') - 100);
    }

    /**
     * @return array
     */
    public function getCreditDueDates()
    {
        return $this->translateEnum(CreditDueDate::instance());
    }

    /**
     * @param bool $provides12Months
     *
     * @return array
     */
    public function getCreditDurations($provides12Months)
    {
        $values = $this->translateEnum(CreditDuration::instance());

        if (! $provides12Months) {
            unset($values[CreditDuration::CREDIT_DURATION_MONTHS_12]);
        }

        return $values;
    }

    /**
     * @return array
     */
    public function getCreditReasons()
    {
        return $this->translateEnum(CreditReason::instance());
    }

    /**
     * @return array
     */
    public function getEmploymentContractTypes()
    {
        return $this->translateEnum(EmploymentContractType::instance());
    }

    /**
     * @return array
     */
    public function getEmploymentTypes()
    {
        return $this->translateEnum(EmploymentType::instance());
    }

    /**
     * @return array
     */
    public function getMaritalStates()
    {
        return $this->translateEnum(MaritalStatus::instance());
    }

    /**
     * @return array
     */
    public function getResidencePermitOptions()
    {
        return $this->translateEnum(ResidencePermit::instance());
    }

    /**
     * @return array
     */
    public function getResidenceTerms()
    {
        return $this->translateEnum(ResidenceTerm::instance());
    }

    /**
     * @return array
     */
    public function getSalutations()
    {
        return $this->translateEnum(Salutation::instance());
    }

    /**
     * @return array
     */
    public function getWorkPermitOptions()
    {
        return $this->translateEnum(WorkPermit::instance());
    }

    /**
     * @param string    $creditReason
     *
     * @return bool
     */
    public function mustProvideCreditReasonDetails($creditReason)
    {
        return $creditReason == CreditReason::CREDIT_REASON_MISC;
    }

    /**
     * @param $nationality
     *
     * @return bool
     */
    public function mustProvideInfoOnWorkPermit($nationality)
    {
        return strtolower($nationality) !== 'de';
    }

    /**
     * @param $contractType
     *
     * @return bool
     */
    public function mustProvideEmploymentContractEndDate($contractType)
    {
        return $contractType == EmploymentContractType::EMPLOYMENT_CONTRACT_TYPE_TEMPORARY;
    }

    /**
     * @param $employmentType
     *
     * @return bool
     */
    public function mustProvideEmploymentDetails($employmentType)
    {
        return $employmentType !== EmploymentType::EMPLOYMENT_TYPE_HOUSEWIFE
            && $employmentType !== EmploymentType::EMPLOYMENT_TYPE_PENSIONER
            && $employmentType !== EmploymentType::EMPLOYMENT_TYPE_STUDENT
            && $employmentType !== EmploymentType::EMPLOYMENT_TYPE_UNEMPLOYED
        ;
    }

    /**
     * @param EnumerableConstants $enumClass
     *
     * @return array
     */
    private function translateEnum(EnumerableConstants $enumClass)
    {
        $ret = array();

        $enum = $enumClass->enum();

        foreach ($enum as $k) {

            $ret[$k] = $this->translator->trans($k, array(), 'formValues');
        }

        return $ret;
    }
}
