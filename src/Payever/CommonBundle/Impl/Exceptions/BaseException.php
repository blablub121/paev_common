<?php
namespace Payever\CommonBundle\Impl\Exceptions;

/**
 * Class BaseException
 *
 * @package Payever\CommonBundle\Impl\Exceptions
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
abstract class BaseException extends \Exception
{

}
