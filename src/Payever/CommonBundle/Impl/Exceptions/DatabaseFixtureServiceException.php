<?php
namespace Payever\CommonBundle\Impl\Exceptions;

use Payever\CommonBundle\Impl\Exceptions\ServiceException;

/**
 * Class DatabaseFixtureServiceException
 *
 * @package Payever\CommonBundle\Impl\Exception
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class DatabaseFixtureServiceException extends ServiceException
{
}
