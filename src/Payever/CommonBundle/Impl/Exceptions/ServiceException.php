<?php
namespace Payever\CommonBundle\Impl\Exceptions;

/**
 * Class ServiceException
 *
 * @package Payever\CommonBundle\Impl\Exceptions
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class ServiceException extends BaseException
{
}
