<?php
namespace Payever\CommonBundle\Impl\WebServices;

use Buzz\Client\ClientInterface;
use Buzz\Client\Curl;

/**
 * Class CurlClient
 *
 * @package Payever\CommonBundle\Impl\WebServices
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class CurlClient extends Curl implements ClientInterface
{
    /** @var array */
    private $originalOptions;

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->originalOptions;
    }

    /**
     * Set Curl options, this is mainly if we access resources through proxies or similar.
     *
     * @param array $options
     */
    public function setOptions(array $options = null)
    {
        $this->originalOptions = $options;

        if ($options == null) {
            return;
        }

        foreach ($options as $k => $v) {

            if (strpos($k, 'CURL') === 0) {
                $this->setOption(constant($k), $v);
            } else {
                $this->setOption($k, $v);
            }
        }
    }
}
