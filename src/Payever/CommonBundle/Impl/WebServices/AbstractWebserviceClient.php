<?php
namespace Payever\CommonBundle\Impl\WebServices;

use Buzz\Browser;
use Buzz\Client\ClientInterface;
use Buzz\Message\Response;
use Payever\CommonBundle\Impl\Serialization\ModelFactory;

/**
 * Class AbstractWebserviceClient
 *
 * @package Payever\CommonBundle\Impl\Webservice
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class AbstractWebserviceClient
{
    /** @var \Buzz\Client\ClientInterface */
    private $buzz;
    /** @var ModelFactory */
    protected $modelFactory;
    /** @var string */
    protected $endpoint;

    /**
     * @param ClientInterface   $buzz
     * @param ModelFactory      $modelFactory
     * @param string            $endpoint
     */
    public function __construct(ClientInterface $buzz, ModelFactory $modelFactory, $endpoint)
    {
        $this->buzz = $buzz;
        $this->modelFactory = $modelFactory;
        $this->endpoint = $endpoint;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @return Browser
     */
    protected function getBrowser()
    {
        static $obj = null;
        return $obj !== null ? $obj : $obj = new Browser($this->buzz);
    }

    /**
     * @param Response  $response
     * @return mixed
     */
    protected function createModelFromResponse(Response $response)
    {
        $content = json_decode($response->getContent());
        return $this->modelFactory->buildModel($content);
    }

}
