<?php
namespace Payever\CommonBundle\Impl\Services;

use \Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use Payever\CommonBundle\Impl\Exceptions\DatabaseFixtureServiceException;
use Payever\CommonBundle\Interfaces\Services\DatabaseFixturesServiceInterface;
use Payever\CommonBundle\Interfaces\Services\LocatorServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DatabaseFixturesService
 *
 * @package Payever\CommonBundle\Impl\Service
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class DatabaseFixturesService implements DatabaseFixturesServiceInterface
{
    /** @var LocatorServiceInterface */
    private $locator;
    /** @var Registry */
    private $doctrine;
    /** @var array */
    private $databaseFixtureResources = array();

    /**
     * @param LocatorServiceInterface $locator
     * @param ContainerInterface      $container
     * @param array                   $fixtures
     *
     * @throws \LogicException
     */
    public function __construct(LocatorServiceInterface $locator, ContainerInterface $container, array $fixtures)
    {
        if (! $container->has('doctrine')) {
            throw new \LogicException('Service "doctrine" must be available in order to the DoctrineUidProvider');
        }

        $this->locator = $locator;
        $this->doctrine = $container->get('doctrine');
        $this->databaseFixtureResources = $fixtures;
    }

    /**
     * @param string $fixtureName
     *
     * @throws \Payever\CommonBundle\Impl\Exceptions\DatabaseFixtureServiceException
     */
    public function applyFixture($fixtureName)
    {
        if (!$this->canUpdateDatabase()) {
            throw new DatabaseFixtureServiceException(
                "Database seems to be a LIVE database",
                "Sorry, I will not reset the database '{$this->getDatabaseName()}' because it does not seem to be a TEST or DEV database. " .
                "To avoid serious mistakes the database name must either end with '_test' or '_dev'. Thanks."
            );
        }

        $filePath = $this->getFixtureFilePath($fixtureName);

        $this->applyFixtureFromFile($filePath);
    }

    /**
     * @param string $fixtureName
     *
     * @throws DatabaseFixtureServiceException
     *
     * @return string
     */
    public function getFixtureFilePath($fixtureName)
    {
        if (! array_key_exists($fixtureName, $this->databaseFixtureResources)) {
            throw new DatabaseFixtureServiceException(
                "Unknown database fixture '$fixtureName'. Available fixtures are [" .
                implode(', ', array_keys($this->databaseFixtureResources)) . "]"
            );
        }

        $filePath = $this->locator->locateResource($this->databaseFixtureResources[$fixtureName]);
        if ($filePath === null) {
            throw new DatabaseFixtureServiceException(
                "Could not find the fixture file '$filePath'"
            );
        }

        return $filePath;
    }

    /**
     * @return string
     */
    public function getDatabaseName()
    {
        /** @var $connection \Doctrine\DBAL\Connection */
        $connection = $this->doctrine->getConnection();
        return $connection->getDatabase();
    }

    /**
     * @return bool
     */
    private function canUpdateDatabase()
    {
        $databaseName = $this->getDatabaseName();

        if (preg_match('/^.*\_test$/', $databaseName) == 0 &&
            preg_match('/^.*\_dev$/', $databaseName) == 0) {

            return false;
        }

        return true;
    }

    /**
     * @param string    $sqlFilePath
     *
     * @throws DatabaseFixtureServiceException
     */
    private function applyFixtureFromFile($sqlFilePath)
    {
        $content = file_get_contents($sqlFilePath);

        $connection = $this->getEntityManager()->getConnection();

        try {
            $connection->executeQuery($content);
        } catch (DBALException $e) {
            throw new DatabaseFixtureServiceException(
                'Error executing fixtures sql',
                0,
                $e
            );
        }
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->doctrine->getManager();
    }
}
