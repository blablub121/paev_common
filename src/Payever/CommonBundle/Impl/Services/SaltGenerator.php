<?php
namespace Payever\CommonBundle\Impl\Services;

use Payever\CommonBundle\Interfaces\Services\SaltGeneratorInterface;

/**
 * Class SaltGenerator
 *
 * @package Payever\CommonBundle\Impl\Services
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class SaltGenerator implements SaltGeneratorInterface
{

    /**
     * @return string
     */
    public function generateSalt()
    {
        $salt = substr(base64_encode(hash('sha512', microtime(). mt_rand() . uniqid(mt_rand(), true), true)), 0, 63);
        return $salt;
    }
}
