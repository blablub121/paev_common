<?php
namespace Payever\CommonBundle\Impl\Services;

use Payever\CommonBundle\Interfaces\Services\TokenGeneratorInterface;

/**
 * Class TokenGenerator
 *
 * @package Payever\CommonBundle\Impl\Services
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class TokenGenerator implements TokenGeneratorInterface
{
    /**
     * C'tor
     */
    public function __construct()
    {
        srand(floatval(microtime(true)));
    }

    /**
     * {@inheritdoc}
     */
    public function generateAccountActivationToken()
    {
        $longHash = md5(rand() . uniqid('', true)) . md5(rand()) . md5(rand());
        return substr($longHash, 10, 2*32);
    }

    /**
     * {@inheritdoc}
     */
    public function generateResetPasswordToken()
    {
        $longHash = md5(rand() . uniqid('', true)) . md5(rand()) . md5(rand());
        return substr($longHash, 14, 2*32);
    }
}
