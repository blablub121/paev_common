<?php
namespace Payever\CommonBundle\Impl\Services;

use \Doctrine\Bundle\DoctrineBundle\Registry;
use Payever\CommonBundle\Entity\UniqueIdentifier;
use Payever\CommonBundle\Impl\Repository\UniqueIdentifierRepository;
use Payever\CommonBundle\Interfaces\Services\UniqueIdentifierProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DoctrineUidProvider
 *
 * @package Payever\CommonBundle\Impl\Service
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class DoctrineUidProvider implements UniqueIdentifierProviderInterface
{
    /** @var Registry */
    private $doctrine;

    /**
     * @param ContainerInterface $container
     *
     * @throws \LogicException
     */
    public function __construct(ContainerInterface $container = null)
    {
        if (! $container->has('doctrine')) {
            throw new \LogicException('Service "doctrine" must be available in order to the DoctrineUidProvider');
        }

        $this->doctrine = $container->get('doctrine');
    }

    /**
     * @return string   A system wide unique hash
     */
    public function getNextHash()
    {
        do {
            $newHash = $this->createHash();
            $allReadyExists = $this->getRepository()->findOneByHash($newHash) !== null;
        } while ($allReadyExists);

        return $newHash;
    }

    /**
     * @return string
     */
    public function createHash()
    {
        $seed = floatval(microtime(true));
        srand($seed);
        return md5(rand() . uniqid('', true) . rand() . uniqid('', true) . rand());
    }

    /**
     * @return UniqueIdentifierRepository
     */
    private function getRepository()
    {
        return $this->doctrine->getRepository(UniqueIdentifier::getClass());
    }
}
