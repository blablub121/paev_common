<?php
namespace Payever\CommonBundle\Impl\Services;

use Payever\CommonBundle\Interfaces\Services\LocatorServiceInterface;
use Symfony\Component\HttpKernel\Kernel;

/**
 * Class LocatorService
 *
 * @package Payever\CommonBundle\Impl\Service
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class LocatorService implements LocatorServiceInterface
{
    /** @var Kernel */
    private $kernel;

    /**
     * @param Kernel $kernel
     */
    public function __construct($kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * {@inheritdoc}
     */
    public function locateResource($resourceName)
    {
        return $this->kernel->locateResource('@' . $resourceName, true);
    }
}
