<?php
namespace Payever\CommonBundle\Impl\Services;

use Payever\CommonBundle\Interfaces\Services\AuthenticationServiceInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\SecurityContextInterface;

/**
 * Class AuthenticationService
 *
 * @package Payever\CommonBundle\Impl\Services
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class AuthenticationService implements AuthenticationServiceInterface
{
    /** @var AuthenticationManagerInterface */
    private $authentication;
    /** @var SecurityContextInterface */
    private $security;
    /** @var SessionInterface */
    private $session;

    /**
     * @param AuthenticationManagerInterface $authentication
     * @param SecurityContextInterface       $security
     * @param SessionInterface               $session
     */
    public function __construct(
        AuthenticationManagerInterface $authentication,
        SecurityContextInterface $security,
        SessionInterface $session
    ) {
        $this->authentication   = $authentication;
        $this->security         = $security;
        $this->session          = $session;
    }

    /**
     * {@inheritdoc}
     */
    public function loginUser($username, $password, $firewall = 'main')
    {
        try {
            $token = new UsernamePasswordToken($username, $password, $firewall);
            $token = $this->authentication->authenticate($token);

            $this->security->setToken($token);
            $this->session->set('_security_' . $firewall, $token->serialize());

            return true;

        } catch (\Exception $e) {
            return false;
        }
    }
}
