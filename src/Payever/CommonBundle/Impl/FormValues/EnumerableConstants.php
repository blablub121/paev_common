<?php
namespace Payever\CommonBundle\Impl\FormValues;

/**
 * Class EnumerableConstants
 *
 * @package Payever\CommonBundle\Impl\FormValues
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
abstract class EnumerableConstants
{
    /**
     * @return EnumerableConstants
     */
    public static function instance()
    {
        return new static;
    }

    /**
     * @return array
     */
    public static function enum()
    {
        $obj = new static();

        $ref = new \ReflectionClass($obj);
        $constants = $ref->getConstants();

        return $constants;
    }
}
