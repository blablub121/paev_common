<?php
namespace Payever\CommonBundle\Impl\FormValues;

/**
 * Class CreditDueDate
 *
 * @package Payever\CommonBundle\Impl\FormValues
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
final class CreditDueDate extends EnumerableConstants
{
    const CREDIT_DUE_DATE_DAY_OF_MONTH_1    = 'CREDIT_DUE_DATE_DAY_OF_MONTH_1';     // 1. des Monats
    const CREDIT_DUE_DATE_DAY_OF_MONTH_15   = 'CREDIT_DUE_DATE_DAY_OF_MONTH_15';    // 15. des Monats
}
