<?php
namespace Payever\CommonBundle\Impl\FormValues;

/**
 * Class WorkPermit
 *
 * @package Payever\CommonBundle\Impl\FormValues
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
final class WorkPermit extends EnumerableConstants
{
    const WORK_PERMIT_NOT_NEEDED    = 'WORK_PERMIT_NOT_NEEDED'; // Keine Arbeitserlaubnis benötigt
    const WORK_PERMIT_TEMPORARY     = 'WORK_PERMIT_TEMPORARY';  // Befristete Arbeitserlaubnis
    const WORK_PERMIT_UNLIMITED     = 'WORK_PERMIT_UNLIMITED';  // Unbefristete Arbeitserlaubnis
    const WORK_PERMIT_NONE          = 'WORK_PERMIT_NONE';       // Keine Arbeitserlaubnis
}
