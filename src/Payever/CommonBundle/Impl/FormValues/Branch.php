<?php
namespace Payever\CommonBundle\Impl\FormValues;

/**
 * Class Branch
 *
 * @package Payever\CommonBundle\Impl\FormValues
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
final class Branch extends EnumerableConstants
{
    const BRANCH_WASTE_MANAGEMENT                  = 'BRANCH_WASTE_MANAGEMENT';           // Abwasser-/Abfallentsorgung
    const BRANCH_CONSTRUCTION_INDUSTRY             = 'BRANCH_CONSTRUCTION_INDUSTRY';      // Bauindustrie
    const BRANCH_MINING                            = 'BRANCH_MINING';                     // Bergbau
    const BRANCH_MILITARY                          = 'BRANCH_MILITARY';                   // Bundeswehr
    const BRANCH_CHEMICAL_INDUSTRY                 = 'BRANCH_CHEMICAL_INDUSTRY';          // Chemieindustrie
    const BRANCH_DATA_PROCESSING                   = 'BRANCH_DATA_PROCESSING';            // Datenverarbeitung
    const BRANCH_ENERGY_SUPPLY                     = 'BRANCH_ENERGY_SUPPLY';              // Energiegewinnung/-versorgung
    const BRANCH_COLOR_PRINT_PAPER                 = 'BRANCH_COLOR_PRINT_PAPER';          // Farb-, Druck- und Papierindustrie
    const BRANCH_FISHERY                           = 'BRANCH_FISHERY';                    // Fischerei
    const BRANCH_CATERING                          = 'BRANCH_CATERING';                   // Gatronomie
    const BRANCH_WHOLESALE_RETAIL                  = 'BRANCH_WHOLESALE_RETAIL';           // Groß- und Einzelhandel
    const BRANCH_REAL_ESTATE                       = 'BRANCH_REAL_ESTATE';                // Grundstücks-/Wohnungswesen
    const BRANCH_CRAFTSMANSHIP                     = 'BRANCH_CRAFTSMANSHIP';              // Handwerk
    const BRANCH_HEALTH_CARE                       = 'BRANCH_HEALTH_CARE';                // Heilberufe/Gesundheitswesen
    const BRANCH_LUMBER_INDUSTRY                   = 'BRANCH_LUMBER_INDUSTRY';            // Holzindustrie
    const BRANCH_HOTEL_INDUSTRY                    = 'BRANCH_HOTEL_INDUSTRY';             // Hotelgewerbe
    const BRANCH_IRON_STEEL_INDUSTRY               = 'BRANCH_IRON_STEEL_INDUSTRY';        // Hütten- und Stahlindustrie
    const BRANCH_JOURNALISM                        = 'BRANCH_JOURNALISM';                 // Journalismus
    const BRANCH_AUTOMOTIVE                        = 'BRANCH_AUTOMOTIVE';                 // KFZ-Hersteller
    const BRANCH_SOCIAL_SERVICE_PROVIDER           = 'BRANCH_SOCIAL_SERVICE_PROVIDER';    // Kirchliche und soziale Einrichtungen
    const BRANCH_HOSPITAL                          = 'BRANCH_HOSPITAL';                   // Kranken- und Pflegeanstalten
    const BRANCH_FINANCIAL_SERVICE_PROVIDER        = 'BRANCH_FINANCIAL_SERVICE_PROVIDER'; // Kreditinstitute/Finanzdienstleister
    const BRANCH_AGRICULTURE_FORESTRY              = 'BRANCH_AGRICULTURE_FORESTRY';       // Land- und Forstwirtschaft
    const BRANCH_MEDIA                             = 'BRANCH_MEDIA';                      // Medien
    const BRANCH_MEASUREMENT_CYBERNETICS_OPTICS    = 'BRANCH_MEASUREMENT_CYBERNETICS_OPTICS';    // Mess-, Steuer-, Regelungstechnik und Optik
    const BRANCH_METAL_INDUSTRY                    = 'BRANCH_METAL_INDUSTRY';             // Metallindustrie
    const BRANCH_FOOD_LUXURY_FOOD_INDUSTRY         = 'BRANCH_FOOD_LUXURY_FOOD_INDUSTRY';  // Nahrungs- und Genussmittelindustrie
    const BRANCH_CIVIL_SERVICE                     = 'BRANCH_CIVIL_SERVICE';              // Öffentlicher Dienst
    const BRANCH_PUBLIC_RELATIONS                  = 'BRANCH_PUBLIC_RELATIONS';           // Öffentlichkeitsarbeit und PR
    const BRANCH_LAWYER_NOTARY                     = 'BRANCH_LAWYER_NOTARY';              // Rechtsanwälte und Notare
    const BRANCH_TRAVEL_INDUSTRY                   = 'BRANCH_TRAVEL_INDUSTRY';            // Reisewirtschaft
    const BRANCH_BROADCASTING                      = 'BRANCH_BROADCASTING';               // Rundfunk- und Fernsehanstalten
    const BRANCH_SEAFARING                         = 'BRANCH_SEAFARING';                  // Schifffahrt
    const BRANCH_FORWARDING_AGENCY                 = 'BRANCH_FORWARDING_AGENCY';          // Speditionen
    const BRANCH_TAX_MANAGEMENT_CONSULTING         = 'BRANCH_TAX_MANAGEMENT_CONSULTING';  // Steuer- und Unternehmensberatung
    const BRANCH_TEXTILE_INDUSTRY                  = 'BRANCH_TEXTILE_INDUSTRY';           // Textilindustrie
    const BRANCH_PUBLISHING                        = 'BRANCH_PUBLISHING';                 // Verlags- und Zeitungswesen
    const BRANCH_INSURANCE                         = 'BRANCH_INSURANCE';                  // Versicherungen
    const BRANCH_STEEL_MILL                        = 'BRANCH_STEEL_MILL';                 // Walzwerke
    const BRANCH_MISC                              = 'BRANCH_MISC';                       // Sonstige
}
