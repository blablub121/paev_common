<?php
namespace Payever\CommonBundle\Impl\FormValues;

/**
 * Class ResidencePermit
 *
 * @package Payever\CommonBundle\Impl\FormValues
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
final class ResidencePermit extends EnumerableConstants
{
    const RESIDENCE_PERMIT_NOT_NEEDED    = 'RESIDENCE_PERMIT_NOT_NEEDED';    // Keine Aufenthaltserlaubnis benötigt
    const RESIDENCE_PERMIT_TEMPORARY     = 'RESIDENCE_PERMIT_TEMPORARY';     // Befristete Aufenthaltserlaubnis
    const RESIDENCE_PERMIT_UNLIMITED     = 'RESIDENCE_PERMIT_UNLIMITED';     // Unbefristete Aufenthaltserlaubnis
    const RESIDENCE_PERMIT_NONE          = 'RESIDENCE_PERMIT_NONE';          // Keine Aufenthaltserlaubnis
}
