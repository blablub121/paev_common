<?php
namespace Payever\CommonBundle\Impl\FormValues;

/**
 * Class Salutation
 *
 * @package Payever\CommonBundle\Impl\FormValues
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
final class Salutation extends EnumerableConstants
{
    const SALUTATION_MR        = 'SALUTATION_MR';      // Herr
    const SALUTATION_MRS       = 'SALUTATION_MRS';     // Frau
}
