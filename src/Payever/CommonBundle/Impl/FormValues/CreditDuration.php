<?php
namespace Payever\CommonBundle\Impl\FormValues;

/**
 * Class CreditDuration
 *
 * @package Payever\CommonBundle\Impl\FormValues
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
final class CreditDuration extends EnumerableConstants
{
    const CREDIT_DURATION_MONTHS_12     = 'CREDIT_DURATION_MONTHS_12';  // 12 Monate
    const CREDIT_DURATION_MONTHS_24     = 'CREDIT_DURATION_MONTHS_24';  // 24 Monate
    const CREDIT_DURATION_MONTHS_36     = 'CREDIT_DURATION_MONTHS_36';  // 36 Monate
    const CREDIT_DURATION_MONTHS_48     = 'CREDIT_DURATION_MONTHS_48';  // 48 Monate
    const CREDIT_DURATION_MONTHS_60     = 'CREDIT_DURATION_MONTHS_60';  // 60 Monate
    const CREDIT_DURATION_MONTHS_72     = 'CREDIT_DURATION_MONTHS_72';  // 72 Monate
    const CREDIT_DURATION_MONTHS_84     = 'CREDIT_DURATION_MONTHS_84';  // 84 Monate
}
