<?php
namespace Payever\CommonBundle\Impl\FormValues;

/**
 * Class ResidenceTerm
 *
 * @package Payever\CommonBundle\Impl\FormValues
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
final class ResidenceTerm extends EnumerableConstants
{
    const RESIDENCE_TERM_TENANT                = 'RESIDENCE_TERM_TENANT';                 // Mieter
    const RESIDENCE_TERM_OWNER                 = 'RESIDENCE_TERM_OWNER';                  // Eigentümer
    const RESIDENCE_TERM_LIVING_WITH_PARENTS   = 'RESIDENCE_TERM_LIVING_WITH_PARENTS';    // bei den Eltern lebend
    const RESIDENCE_TERM_MISC                  = 'RESIDENCE_TERM_MISC';                   // Sonstige
}
