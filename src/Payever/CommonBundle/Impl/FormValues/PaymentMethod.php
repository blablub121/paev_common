<?php
namespace Payever\CommonBundle\Impl\FormValues;

/**
 * Class PaymentMethod
 *
 * @package Payever\CommonBundle\Impl\FormValues
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
final class PaymentMethod extends EnumerableConstants
{
    const PAYMENT_METHOD_BILL_PAYMENT   = 'PAYMENT_METHOD_BILL_PAYMENT';               // Rechnungszahlung
    const PAYMENT_METHOD_INSTALLMENTS   = 'PAYMENT_METHOD_INSTALLMENTS';               // Ratenzahlung
}
