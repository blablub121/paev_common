<?php
namespace Payever\CommonBundle\Impl\FormValues;

/**
 * Class EmploymentType
 *
 * @package Payever\CommonBundle\Impl\FormValues
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
final class EmploymentType extends EnumerableConstants
{
    const EMPLOYMENT_TYPE_EMPLOYEE          = 'EMPLOYMENT_TYPE_EMPLOYEE';           // Angestellter
    const EMPLOYMENT_TYPE_WORKER            = 'EMPLOYMENT_TYPE_WORKER';             // Arbeiter
    const EMPLOYMENT_TYPE_CIVIL_SERVANT     = 'EMPLOYMENT_TYPE_CIVIL_SERVANT';      // Beamter
    const EMPLOYMENT_TYPE_SELF_EMPLOYED     = 'EMPLOYMENT_TYPE_SELF_EMPLOYED';      // Selbständig
    const EMPLOYMENT_TYPE_FREELANCER        = 'EMPLOYMENT_TYPE_FREELANCER';         // Freiberufler
    const EMPLOYMENT_TYPE_PENSIONER         = 'EMPLOYMENT_TYPE_PENSIONER';          // Rentner
    const EMPLOYMENT_TYPE_HOUSEWIFE         = 'EMPLOYMENT_TYPE_HOUSEWIFE';          // Hausfrau
    const EMPLOYMENT_TYPE_STUDENT           = 'EMPLOYMENT_TYPE_STUDENT';            // Student
    const EMPLOYMENT_TYPE_APPRENTICE        = 'EMPLOYMENT_TYPE_APPRENTICE';         // Auszubildender
    const EMPLOYMENT_TYPE_CONSCRIPTION      = 'EMPLOYMENT_TYPE_CONSCRIPTION';       // Zivil- / Wehrdienstleistender
    const EMPLOYMENT_TYPE_REGULAR_SOLDIER   = 'EMPLOYMENT_TYPE_REGULAR_SOLDIER';    // Zeitsoldat
    const EMPLOYMENT_TYPE_UNEMPLOYED        = 'EMPLOYMENT_TYPE_UNEMPLOYED';         // Arbeitslos
    const EMPLOYMENT_TYPE_MISC              = 'EMPLOYMENT_TYPE_MISC';               // Sonstige
}
