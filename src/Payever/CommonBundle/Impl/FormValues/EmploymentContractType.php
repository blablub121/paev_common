<?php
namespace Payever\CommonBundle\Impl\FormValues;

/**
 * Class EmploymentContractType
 *
 * @package Payever\CommonBundle\Impl\FormValues
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
final class EmploymentContractType extends EnumerableConstants
{
    const EMPLOYMENT_CONTRACT_TYPE_TEMPORARY     = 'EMPLOYMENT_CONTRACT_TYPE_TEMPORARY';     // befristet
    const EMPLOYMENT_CONTRACT_TYPE_UNLIMITED     = 'EMPLOYMENT_CONTRACT_TYPE_UNLIMITED';     // unbefristet
}
