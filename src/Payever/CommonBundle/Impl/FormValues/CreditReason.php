<?php
namespace Payever\CommonBundle\Impl\FormValues;

/**
 * Class CreditReason
 *
 * @package Payever\CommonBundle\Impl\FormValues
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
final class CreditReason extends EnumerableConstants
{
    const CREDIT_REASON_HOUSEHOLD_ELECTRONICS     = 'CREDIT_REASON_HOUSEHOLD_ELECTRONICS';    // Haushaltselektronik
    const CREDIT_REASON_RENOVATION                = 'CREDIT_REASON_RENOVATION';               // Renovierung
    const CREDIT_REASON_AUTOMOBILE                = 'CREDIT_REASON_AUTOMOBILE';               // PKW
    const CREDIT_REASON_TRAVEL                    = 'CREDIT_REASON_TRAVEL';                   // Reise
    const CREDIT_REASON_MISC                      = 'CREDIT_REASON_MISC';                     // Sonstige
}
