<?php
namespace Payever\CommonBundle\Impl\FormValues;

/**
 * Class MaritalStatus
 *
 * @package Payever\CommonBundle\Impl\FormValues
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
final class MaritalStatus extends EnumerableConstants
{
    const MARITAL_STATUS_SINGLE                    = 'MARITAL_STATUS_SINGLE';                  // ledig - allein lebend
    const MARITAL_STATUS_MARRIED                   = 'MARITAL_STATUS_MARRIED';                 // verheirated
    const MARITAL_STATUS_DIVORCED                  = 'MARITAL_STATUS_DIVORCED';                // geschieden
    const MARITAL_STATUS_WIDOWED                   = 'MARITAL_STATUS_WIDOWED';                 // Witwe / Witwer
    const MARITAL_STATUS_COHABITING                = 'MARITAL_STATUS_COHABITING';              // zusammen lebend
    const MARITAL_STATUS_SEPARATED                 = 'MARITAL_STATUS_SEPARATED';               // getrennt lebend
    const MARITAL_STATUS_REGISTERED_PARTNERSHIP    = 'MARITAL_STATUS_REGISTERED_PARTNERSHIP';  // registrierte Partnerschaft
}
