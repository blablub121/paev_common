<?php
namespace Payever\CommonBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;

/**
 * Class DatabaseResetCommand
 *
 * @package Payever\CommonBundle\Command
 *
 * @author  Karsten J. Gerber <kontakt@karsten-gerber.de>
 */
class DatabaseResetCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('payever:database:reset')
            ->setDefinition(array(
                new InputArgument('fixture', InputArgument::REQUIRED, 'The fixture to use'),
            ))
            ->setDescription('Executes a database fixtures sql')
            ->setHelp(<<<EOT
The <info>%command.name%</info> command resets the database

<info>php %command.full_name% web</info>

EOT
            )
        ;

        parent::configure();
    }

    /**
     * {@inheritdoc}
     *
     * @throws \InvalidArgumentException When the target directory does not exist or symlink cannot be used
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fixtureName = $input->getArgument('fixture');

        $service = $this->getContainer()->get('payever.common.service.database.fixtures');

        $service->applyFixture($fixtureName);

        $fixtureFile = $service->getFixtureFilePath($fixtureName);
        $databaseName = $service->getDatabaseName();

        $output->writeln("Loaded fixtures from file '{$fixtureFile}' into database '$databaseName', hope that was okay.");
    }
}
